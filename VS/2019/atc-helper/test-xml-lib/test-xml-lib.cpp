// test-xml-lib.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>
#include <fstream>
#include <vector>
#include <map>
#include <errno.h>
#include <string.h>
#include <unordered_map>
#include <string_view>
#include "pugixml.hpp"

typedef struct _atc_task
{
  int seq{ 0 }; // we will use the sequence as inumerator
  std::map <int, std::string> mapName; // example: clearence, taxi, tower. Task name in Category element
  std::map <int, std::string> mapText;  // all the text in the task
} atc_tasks_strct;

static std::map <std::string, std::string> mapDynamicNameAndUserEnteredValue; // stores user prefered dynamic string. key represent the dynamic name, value: represent user entered value

typedef struct _atc_category
{
  int seq_category_i{ 0 };
  std::string category_name{ "" };
  atc_tasks_strct category_tasks;

  // option strings
  std::vector <std::string> vecDynamicOptions; // split the attribute dynamic_options
  std::vector <const char*> vecDynamicOptions_char; // pointer to string for imgui::choice widget

} atc_category_strct;

//std::map <int, atc_category_strct> map_atc_categories; // will hold only the data from specific flight type and region


std::string replaceStringWithOtherString(std::string inStringToModify, std::string inStringToReplace, std::string inNewString, bool flag_forAllOccurances)
{
  bool flag_continueSearch = true;

  auto pos = inStringToModify.find(inStringToReplace);
  while (pos != std::string::npos)
  {
    inStringToModify.replace(pos, inStringToReplace.length(), inNewString);

    if (flag_forAllOccurances)
    {
      auto offset = pos + inNewString.length();
      std::cout << "Pos: " << pos << ", new search pos: " << pos + inNewString.length();
      pos = inStringToModify.find(inStringToReplace, pos + inNewString.length());
      std::cout << ". Find in Pos: " << pos << "\n";

    }
    else
    {
      pos = std::string::npos;
      break;
    }
  
  } // end while loop

  return inStringToModify;
}

void drill_node(pugi::xml_node& parent, int level)
{
  if (parent.empty())
    return;

  level++;
  const auto tabs = [&level]{ 
    std::string outTabs = "";
    for (int i = 0; i < level; ++i) outTabs.append(" ");

    return outTabs;
  };


  auto nodes = parent.children();
  for (auto node : nodes)
  {
    auto sibling = node.next_sibling();
    std::cout << tabs() << node.name() << " [ " << sibling.name() << " ]\n";

    drill_node(node, level);

  }
}

void drill_node_v2(pugi::xml_node& parent, int level)
{
  if (parent.empty())
    return;

  level++;
  const auto tabs = [&level]{ 
    std::string outTabs = "";
    for (int i = 0; i < level; ++i) outTabs.append(" ");

    return outTabs;
  };


  auto nodes = parent.children();
  for (auto node : nodes)
  {
    const std::string name = node.name();
    const std::string text = node.text().get();
    if (!text.empty() && name.empty() )
      std::cout << node.text().as_string();

    drill_node_v2(node, level);

  }

}

void test_drill_read(pugi::xml_document &doc)
{
  auto nodes = doc.children();
  int level = 1;
  for (auto node : nodes)
  {
    auto sibling = node.next_sibling();
    //node.print(std::cout);
    //std::cout << node.name() << "\n[ " << node.value() << " ]\n";
    std::cout << node.name() << " [ " << sibling.name() << " ]\n";
    drill_node_v2(node, level);

  }
}

std::vector <std::string> splitString(const std::string inString, char delimeter, bool b_remove_spaces, bool b_skipEmptyString = false)
{
  std::vector <std::string> tokens;
  std::string token{ "" };
  for (auto& c : inString)
  {
    if (c == delimeter)
    {
      if (token.empty() && b_skipEmptyString)
        continue;
      else
      {
        tokens.emplace_back(token);
        token.clear();
      }
    }
    else if (c == ' ' && b_remove_spaces)
      continue;
    else
    {
      token.push_back(c);
    }

  } // end loop over all characters

  return tokens;
}



const std::vector <std::string> read_category_dynamic_options(pugi::xml_node nodeCategory)
{
  auto options_attrib = nodeCategory.attribute("options");
  if (options_attrib)
  {
    return splitString(options_attrib.value(), ',', true);
  }

  std::vector <std::string> empty_vecOptions;
  return empty_vecOptions;
}



const std::map <int, atc_category_strct> read_atc_text_based_on_region_and_flight_type(const pugi::xml_document& doc, const std::string_view inPathQuery)
{
  std::map <int, atc_category_strct> map_atc_categories;

  pugi::xpath_node xpath_node = doc.select_node(inPathQuery.data());
  auto nodes = xpath_node.node().children();

  std::string prevKey{ "" };

  map_atc_categories.clear();

  // we could have a recursive function but since we only read all text of the second level of sub nodes we could write a simple loop inside a loop
  // <category node>
  //   <internal task> => clearence, taxi, tower
  int category_node_seq_i = 0;
  for (auto nodeCategory : nodes)
  {
    if (nodeCategory.empty() || nodeCategory.name() == "")
      continue;

    atc_category_strct category; // create category struct
    // intitialize category struct
    category.seq_category_i = category_node_seq_i; // store sequence, will use it also in the category map.
    category.category_name = nodeCategory.name(); // store name of category node

    //auto subTaskNodes = nodeCategory.children();
    for (auto taskNode : nodeCategory.children())
    {

      auto pcnodes = taskNode.children(); // loop over all PCDATA and CDATA nodes
      std::string nodeText_s{ "" };

      for (auto pcn : pcnodes)
      {
        std::string name = pcn.name();
        std::string text = pcn.text().get();
        if (name.empty() && !text.empty())
          nodeText_s.append(pcn.text().as_string());
      }

      // store the task information in the category struct
      category.category_tasks.mapName[category.category_tasks.seq] = taskNode.name(); // store task node name
      category.category_tasks.mapText[category.category_tasks.seq] = nodeText_s; // store the task cumulative text
      category.category_tasks.seq++; // increment internal task sequence. Start from 0..N-1

    } // end loop over all category sub tasks


    category.vecDynamicOptions = read_category_dynamic_options(nodeCategory);

    // Store the category with all subtasks data
    map_atc_categories[category_node_seq_i] = category;
    category_node_seq_i++; // increment category node
  } // end loop over all categories

  return map_atc_categories;
}

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
// read all comm_standard elements. Mainly: <ATC><IFR><US>. Will store the first and second sub siblings. The first will be the key, and the second will be the vector.
const std::map <std::string, std::vector <std::string>> get_flightType_and_commStandards_from_xml_template(pugi::xml_document& doc)
{
  std::map <std::string, std::vector <std::string>> mapFlightTypes_and_CommStandards_defined_in_xml_template; // key = IFR, value vector of sub node sibling names.
  std::vector <std::string> vecTags; // will hold all of the node names

  auto atc_node = doc.child("ATC");
  if (!atc_node.empty())
  {
    auto node_range_sub1 = atc_node.children();
    for (auto node = node_range_sub1.begin(); node != node_range_sub1.end(); ++node )
    {
      std::string key = node->name(); // name of ATC children, example: IFR/VFR
      if (key.compare("IFR") != 0 && key.compare("VFR") != 0)
        continue; // skip non IFR/VFR elements
      auto node_range_sub2 = node->children(); // sub 2 childrens
      for (auto node_comm = node_range_sub2.begin(); node_comm != node_range_sub2.end(); ++node_comm)
      {
        vecTags.emplace_back(node_comm->name());
      }

      // decide if to store
      if (!vecTags.empty())
        mapFlightTypes_and_CommStandards_defined_in_xml_template[key] = vecTags;

      vecTags.clear();

    }
  }

  return mapFlightTypes_and_CommStandards_defined_in_xml_template;
}




//---------------------------------------------------------------------------------

std::string ltrim_text_on_all_lines(const std::string inText)
{
  bool b_found_none_space_char = false;
  int lineIndex = 0;
  std::string s; s.clear();

  // loop over all characters
  for (auto c : inText)
  {
    if (c == ' ' && !b_found_none_space_char)
    {
      continue;
    }
    else if (c == '\n') // new line
    {
      if (lineIndex == 0 && !b_found_none_space_char) // skip all leading \n from first row until we reach the first line with characters.
        continue;

      ++lineIndex;
      b_found_none_space_char = false; // reset bool on new line
      s += c;
      continue;
    }
    else if (c == '.' && !b_found_none_space_char) // represent the begining of logical line
    {
      // first dot (".") in line represent "read from here all chars until "\n", include leading spaces
      b_found_none_space_char = true;

      continue;
    }


    s += c;
    b_found_none_space_char = true;

  }

  return s;
}

//---------------------------------------------------------------------------------

std::map <std::string, std::string> get_parsed_doc8643(std::filesystem::path &filepath )
{
  char buff[256];
  std::map <std::string, std::string> mapDoc8643;
  std::ifstream ifs(filepath, std::ios::binary|std::ios::ate);

  if (!ifs)
  {
#ifndef RELEASE
    strerror_s(buff, errno);
#endif // !RELEASE
    return mapDoc8643; // throw std::runtime_error(filepath.string() + ": " + buff);
  }
    

  auto end = ifs.tellg();
  ifs.seekg(0, std::ios::beg);

  auto size = std::size_t(end - ifs.tellg());

  if (size == 0) // avoid undefined behavior 
    return mapDoc8643;

  std::vector<std::byte> buffer(size);

  if (!ifs.read((char*)buffer.data(), buffer.size()))
  {
#ifndef RELEASE
    strerror_s(buff, errno);
#endif // !RELEASE

    return mapDoc8643; // throw std::runtime_error(filepath.string() + ": " + buff + "\n");

  }

  // split vector in "=" symbol
  std::string key{ "" }, value{ "" };
  bool isKey = true;
  for (const auto& c : buffer)
  {
    if ((char)c == '\n')
    {
      (!isKey) ? mapDoc8643[key] = value : "";
      isKey = true;
      key.clear();
      value.clear();

      continue;
    }
    else if ((char)c == '=')
    {
      isKey = false;
      continue;
    }
    else if ((int)c > 31)
      (isKey) ? key.push_back((char)c) : value.push_back((char)c);
    else
      continue;
    
  }


  return mapDoc8643;
}

//---------------------------------------------------------------------------------
std::vector<std::string> ones{ "","one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
std::vector<std::string> teens{ "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen","sixteen", "seventeen", "eighteen", "nineteen" };
std::vector<std::string> tens{ "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

std::string nameForNumber(long number) {
  if (number < 10) {
    return ones[number];
  }
  else if (number < 20) {
    return teens[number - 10];
  }
  else if (number < 100) {
    return tens[number / 10] + ((number % 10 != 0) ? " " + nameForNumber(number % 10) : "");
  }
  else if (number < 1000) {
    return nameForNumber(number / 100) + " hundred" + ((number % 100 != 0) ? " " + nameForNumber(number % 100) : "");
  }
  else if (number < 1000000) {
    return nameForNumber(number / 1000) + " thousand" + ((number % 1000 != 0) ? " " + nameForNumber(number % 1000) : "");
  }
  else if (number < 1000000000) {
    return nameForNumber(number / 1000000) + " million" + ((number % 1000000 != 0) ? " " + nameForNumber(number % 1000000) : "");
  }
  else if (number < 1000000000000) {
    return nameForNumber(number / 1000000000) + " billion" + ((number % 1000000000 != 0) ? " " + nameForNumber(number % 1000000000) : "");
  }
  return "error";
}


//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

int main()
{

  std::cout << nameForNumber(2500) << "\n";

  return 0;

  std::filesystem::path filepath("d:/xp11clean/Resources/plugins/atc-helper/Doc8643.txt");
  std::map <std::string, std::string> mapDoc8643 = get_parsed_doc8643(filepath);
  
//  std::string text = R"(  
//
//    .  Hello World   
//
//      I told you so - you can      \r\n    
//.      sdfa                            asdf
//Wow
//)";
//  std::string text_ltrim = ltrim_text_on_all_lines(text);
//  std::cout << "Text Before: \n[" << text << "]\n\nText After:\n[" << text_ltrim << "]\n";
//
//  return 0;


    std::cout << "Testing PugiXML!\n";

    pugi::xml_document doc;

    //pugi::xml_parse_result result = doc.load_file("d:/programming/GIT/atc-helper/libs/pugixml/docs/samples/tree.xml");
    pugi::xml_parse_result result = doc.load_file("d:/xp11clean/Resources/plugins/atc-helper/atc-template.xml");
    //pugi::xml_parse_result result = doc.load_file("d:/programming/GIT/atc-helper/atc02.xml");
    std::cout << "Load result: " << result.description() << "\n";
    if (!result)
    {
      std::cout << "Error Offset: " << result.offset << "\n";
      return -1;
    }

    //test_drill_read(doc); // test 1: simple function to read all text element types in lower most nodes, like <clearence>, <taxi> etc...

    auto mapStandards = get_flightType_and_commStandards_from_xml_template(doc);

    ///// Read XML using XPATH
    std::string flightType{ "IFR" };
    std::string region{ "US" };
    std::string xpathQuery = std::string("/ATC/") + flightType + "/" + region;

    auto map_atc_categories = read_atc_text_based_on_region_and_flight_type(doc, xpathQuery);

    std::map <std::string, std::string> mapReplaceKeywords = { {"%callsign%", "C172"}, {"%plane_type%", "Cessna 172"}, {"%dep_airport%", "KSEA"}, {"%dest_airport%", "YBTL"}, {"%atis_info%","Alfa"}, {"%stand%", "GA10" } };
    for (auto &cat : map_atc_categories) // loop over all categories in region
    {
      for (auto& task : cat.second.category_tasks.mapText) // loop over all tasks text for that category
      {
        for (auto &r : mapReplaceKeywords) // replace each keyword with the value from mapReplaceKeywords
        {
          task.second = replaceStringWithOtherString(task.second, r.first, r.second, true); // task text will be replacing r.first string with r.second string.
        }
        std::cout << "Replacing task text: " << cat.second.category_tasks.mapName[task.first] << ", result: \n" << task.second;

      }
    }
}

