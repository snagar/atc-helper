#include "atc.h"

#include "fa-solid-900.inc"
#include "IconsFontAwesome5.h" // inside libs/imgui4xp

namespace atchelper {

  // Menu
  XPLMMenuID      ATC::atcMenuEntry;
  ATC::_atc_menu  ATC::atc_menu;


  ImgAtcWindowSPtrTy ATC::uiImGuiAtcWindow;
}


namespace atchelper
{

  void configureImgWindow()
  {
#ifdef LIN
    ImgWindow::sFontAtlas = std::make_shared<ImgFontAtlas>();
#else
    ImgWindow::sFontAtlas = std::make_unique<ImgFontAtlas>();
#endif

    ImgWindow::sFontAtlas->AddFontFromFileTTF(atchelper::DEFAULT_FONT_LOCATION.c_str(), atchelper::DEFAULT_FONT_SIZE); // original was: DejaVuSansMono.ttf
    ImFontConfig config;
    config.MergeMode = true;

    // We only read very selectively the individual glyphs we are actually using
    // to save on texture space
    static ImVector<ImWchar> icon_ranges;
    ImFontGlyphRangesBuilder builder;
    // Add all icons that are actually used (they concatenate into one string)
    builder.AddText(ICON_FA_SAVE ICON_FA_SEARCH
      ICON_FA_FILE_CODE ICON_FA_TRASH_ALT
      ICON_FA_WINDOW_MAXIMIZE ICON_FA_WINDOW_MINIMIZE
      ICON_FA_WINDOW_RESTORE ICON_FA_WINDOW_CLOSE 
      ICON_FA_COG ICON_FA_PLUS ICON_FA_MINUS); // 
    builder.BuildRanges(&icon_ranges);

    // Merge the icon font with the text font
    ImgWindow::sFontAtlas->AddFontFromMemoryCompressedTTF(fa_solid_900_compressed_data,
      fa_solid_900_compressed_size,
      atchelper::DEFAULT_FONT_SIZE,
      &config,
      icon_ranges.Data);
  }






// ----------------------------
// ----------------------------
// ----------------------------
// ----------------------------
// ----------------------------
// ----------------------------


  ATC::ATC()
  {
    init();
  }

  ATC::~ATC()
  {
  }

  void ATC::init()
  {

    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_read_config_file);
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_congifImgWindow_fontAtlas);
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_reload_xml_atc_template_file);

    //[[maybe_unused]]
    //std::string_view filepath_to_doc8643 = Utils::getPluginDirectoryWithSep(atchelper::PLUGIN_NAME, true) + atchelper::DOC8643_FILENAME;
    //std::filesystem::path filepath ( filepath_to_doc8643 );
    //atc_data::mapDoc8643 = atc_data::get_parsed_doc8643(filepath);
  }

  // ----------------------------

  void ATC::read_config_file()
  {
    // construct default config file from the uiWindow
    [[maybe_unused]]
    std::string default_config_file_text = 
R"(########################################
# This file is part of ATC-Helper plugin
# you should not modify it unless you know what you are doing.
# 
########################################

# window
maxWidth=)" + Utils::formatNumber<int>(atchelper::WinAtcHelper::MAX_WIDTH) +
"\nmaxHeight=" + Utils::formatNumber<int>(atchelper::WinAtcHelper::MAX_HEIGHT) +
"\nwidth=" + Utils::formatNumber<int>(atchelper::WinAtcHelper::layer_config.conf_width) +
"\nheight=" + Utils::formatNumber<int>(atchelper::WinAtcHelper::layer_config.conf_height) +
"\n\n;Flight Preference:" +
"\nflight_type=" +
"\ncomm_standard=" +
"\n"
;

    ad.read_config_file(default_config_file_text);
  }

  // ----------------------------

  void ATC::save_config_file()
  {
    int t, b, l, r;
    ATC::uiImGuiAtcWindow->GetWindowGeometry(l, t, r, b);
    int width = r - l;
    int height = t - b;

    const std::string config_file_path = atc_data::PLUGIN_PATH + atchelper::CONFIG_FILENAME;

    // construct default config file from the uiWindow
    [[maybe_unused]]
    std::string config_file_text =
      R"(########################################
# This file is part of ATC-Helper plugin
# you should not modify it unless you know what you are doing.
# 
########################################

# window
maxWidth=)" + Utils::formatNumber<int>(atchelper::WinAtcHelper::MAX_WIDTH) +
"\nmaxHeight=" + Utils::formatNumber<int>(atchelper::WinAtcHelper::MAX_HEIGHT) +
"\nwidth=" + Utils::formatNumber<int>(width) +
"\nheight=" + Utils::formatNumber<int>(height) +
"\n\n;Flight Config:" +
"\nflight_type=" + ATC::uiImGuiAtcWindow->layer_config.flightTypesAndStandard_ui_strct.mapFlightTypes_seq_key_ui [ATC::uiImGuiAtcWindow->layer_config.user_picked_flight_type_i] +
"\ncomm_standard=" + ATC::uiImGuiAtcWindow->layer_config.flightTypesAndStandard_ui_strct.vecCommStandards_char.at(WinAtcHelper::layer_config.user_picked_comm_standard_i) +
"\n" + atchelper::CONFIG_CALLSIGN               + "=" + ATC::uiImGuiAtcWindow->layer_config.callsign +
"\n" + atchelper::CONFIG_CALLSIGN_AS_PHONETIC   + "=" + mxUtils::formatNumber<bool>( ATC::uiImGuiAtcWindow->layer_config.b_useCallsignPhonetic ) +
"\n" + atchelper::CONFIG_PLANE_TYPE             + "=" + ATC::uiImGuiAtcWindow->layer_config.plane_type +
"\n" + atchelper::CONFIG_PLANE_TYPE_AS_PHONETIC + "=" + mxUtils::formatNumber<bool>( ATC::uiImGuiAtcWindow->layer_config.b_usePlaneTypePhonetic ) +
"\n" + atchelper::CONFIG_PLANE_MANUFACTURER     + "=" + ATC::uiImGuiAtcWindow->layer_config.plane_manufacturer;
"\n"
;

  // write to atc-helper.ini
  std::ofstream outfsConfFile;

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  outfsConfFile.open(config_file_path.c_str(), std::ios::out);
  if (outfsConfFile.is_open())
  {
    outfsConfFile << config_file_text << "\n";
    outfsConfFile.flush();
    Log::logMsg("Saved configuration file.\n");
  }
  else
    Log::logMsg("ERR: Failed saving config file.\n");

  outfsConfFile.close();


  }


  // ----------------------------


  void ATC::flc()
  {

    ////// Handle Logs
    int countMsg = 0;
    while (!atchelper::writeLogThread::qLogMessages_mainThread.empty() && (25 > countMsg))
    {
      std::string msg = atchelper::writeLogThread::qLogMessages_mainThread.front();
      XPLMDebugString(msg.c_str());
      atchelper::writeLogThread::qLogMessages_mainThread.pop();

      ++countMsg;
    }

    atchelper::Log::flc();
    ////// End Logs Handling

    // Call UI flc() function
    if (ATC::uiImGuiAtcWindow) ATC::uiImGuiAtcWindow->flc();

    // Call PRE tasks
    flc_TASKS();

    // call long running tasks
    flc_serial_dispatcher();



    // Handle postQueue
    while (!atc_data::postFlcActions.empty())
    {
      atc_data::atc_flc_pre_command c = atc_data::postFlcActions.front();
      atc_data::postFlcActions.pop();
      atchelper::atc_data::queFlcActions.push(c); // place action in the "pre task" Queue for next iteration.
    } // end loop over all postFlcActions
  }


  // ----------------------------
  void ATC::toggleMainWindow()
  {
    if (ATC::uiImGuiAtcWindow != nullptr)
      ATC::uiImGuiAtcWindow->execAction(atchelper::mx_window_actions::ACTION_TOGGLE_WINDOW);
    else // create the widnow
    {
      int left, top, right, bottom;
      Utils::getWinCoords(left, top, right, bottom); // 410, 200, 75, 30,

      // Set default values based on the CONFIG file
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_MAX_WIDTH))
        atchelper::WinAtcHelper::MAX_WIDTH = Utils::stringToNumber<int>(atc_data::mapConfig[atchelper::CONFIG_MAX_WIDTH]);
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_MAX_HEIGHT))
        atchelper::WinAtcHelper::MAX_HEIGHT = Utils::stringToNumber<int>(atc_data::mapConfig[atchelper::CONFIG_MAX_HEIGHT]);
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_WIDTH))
        atchelper::WinAtcHelper::layer_config.conf_width = Utils::stringToNumber<int>(atc_data::mapConfig[atchelper::CONFIG_WIDTH]);
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_HEIGHT))
        atchelper::WinAtcHelper::layer_config.conf_height = Utils::stringToNumber<int>(atc_data::mapConfig[atchelper::CONFIG_HEIGHT]);

      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_CALLSIGN))
        atchelper::WinAtcHelper::layer_config.setCallsign ( atc_data::mapConfig[atchelper::CONFIG_CALLSIGN] ); // will set the callsign parameter and the buff_callsign[7]
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_CALLSIGN_AS_PHONETIC)) // v0.3.0
      {
        bool b_result = false;
        if (Utils::isStringBool(atc_data::mapConfig[atchelper::CONFIG_CALLSIGN_AS_PHONETIC], b_result))
          atchelper::WinAtcHelper::layer_config.b_useCallsignPhonetic = b_result;
      }

      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_PLANE_TYPE))
        atchelper::WinAtcHelper::layer_config.setPlaneType(atc_data::mapConfig[atchelper::CONFIG_PLANE_TYPE]); // will set the plane type and the buff_plane_type[10]
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_PLANE_TYPE_AS_PHONETIC)) // v0.3.0
      {
        bool b_result = false;
        if (Utils::isStringBool(atc_data::mapConfig[atchelper::CONFIG_PLANE_TYPE_AS_PHONETIC], b_result))
          atchelper::WinAtcHelper::layer_config.b_usePlaneTypePhonetic = b_result;
      }


      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_PLANE_MANUFACTURER))
        atchelper::WinAtcHelper::layer_config.setManufacturer ( atc_data::mapConfig[atchelper::CONFIG_PLANE_MANUFACTURER] );// will set the manufacturer
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_FLIGHT_TYPE))
        atchelper::WinAtcHelper::layer_config.flight_type = atc_data::mapConfig[atchelper::CONFIG_FLIGHT_TYPE];
      if (mxUtils::isElementExists(atc_data::mapConfig, atchelper::CONFIG_COMM_STANDARD))
        atchelper::WinAtcHelper::layer_config.comm_standard = atc_data::mapConfig[atchelper::CONFIG_COMM_STANDARD];



      // Position widget relative to windows borders
      // left & right
      left = right - atchelper::WinAtcHelper::layer_config.conf_width - 20;
      right = left + atchelper::WinAtcHelper::layer_config.conf_width;
      // top & bottom
      top = top - 100;
      bottom = top - atchelper::WinAtcHelper::layer_config.conf_height;
//#ifdef LIN
//      Mission::uiImGuiOptions = std::make_shared<WinImguiOptions>(left, top, right, bottom, xplm_WindowDecorationSelfDecoratedResizable, xplm_WindowLayerFloatingWindows); // decoration and layer will use default values
//#else
//      //ATC::uiImGuiAtcWindow = std::make_unique<WinAtcHelper>(left, top, right, bottom, xplm_WindowDecorationSelfDecoratedResizable, xplm_WindowLayerFloatingWindows); // decoration and layer will use default values
//      ATC::uiImGuiAtcWindow = std::make_unique<WinAtcHelper>(left, top, right, bottom, xplm_WindowDecorationRoundRectangle, xplm_WindowLayerFloatingWindows); // decoration and layer will use default values
//#endif

      ATC::uiImGuiAtcWindow = std::make_unique<WinAtcHelper>(left, top, right, bottom, xplm_WindowDecorationRoundRectangle, xplm_WindowLayerFloatingWindows); // decoration and layer will use default values
      assert(ATC::uiImGuiAtcWindow && "Failed to create UI Window");

      //// Initialize setup/config screen
      atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_read_atc_config_data_from_loaded_xml_and_set_atc_conf_layer); 


      //ATC::uiImGuiAtcWindow->layer_config.set_mapFlightTypes_and_CommStandards_defined_in_xml_template(atc_data::get_flightType_and_commStandards_from_xml_template(atc_data::xml_main_doc_template_ptr));
      //ATC::uiImGuiAtcWindow->setLayer(atchelper::uiLayer_enum::setup_layer);

      //atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::push_loaded_xpath_data_to_window); // moved to the UI window // upload new XML information to uiImGuiAtcWindow Window

    }
  }
  // ----------------------------

  void ATC::flc_TASKS()
  {
    while (!atc_data::queFlcActions.empty())
    {
      atchelper::atc_data::atc_flc_pre_command c = atc_data::queFlcActions.front();
      atc_data::queFlcActions.pop();

      switch (c)
      {
        case atchelper::atc_data::atc_flc_pre_command::toggle_atc_helper_window:
        {
          toggleMainWindow();          
        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::task_read_config_file:
        {
          read_config_file();
        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::save_atc_setup:
        {
          save_config_file();
        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::task_congifImgWindow_fontAtlas:
        {
          configureImgWindow(); 
        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::task_reload_xml_atc_template_file:
        {
          pugi::xml_parse_result result;
          const std::string xml_template_file = atc_data::PLUGIN_PATH + atchelper::ATC_TEMPLATE_FILENAME;

          atc_data::xml_main_doc_template_ptr.reset();
          if (atc_data::xml_main_doc_template_ptr.children().begin() == atc_data::xml_main_doc_template_ptr.children().end())
          {
            result = atc_data::xml_main_doc_template_ptr.load_file(xml_template_file.c_str());

            if (result)
            {
              atchelper::atc_data::load_xml_err_s.clear(); 
              Log::logMsg("Successfully loaded: " + xml_template_file + "\n");
            }
            else 
            {
              atchelper::atc_data::load_xml_err_s = "Error Offset: " + Utils::formatNumber<long long>(result.offset) + "\n";
              Log::logMsg(atchelper::atc_data::load_xml_err_s);
              atc_data::xml_main_doc_template_ptr.reset();
              return;
            }

          }
        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::task_read_atc_config_data_from_loaded_xml_and_set_atc_conf_layer:
        {
          // Initialize setup/config screen
          ATC::uiImGuiAtcWindow->layer_config.set_mapFlightTypes_and_CommStandards_defined_in_xml_template(atc_data::get_flightType_and_commStandards_from_xml_template(atc_data::xml_main_doc_template_ptr));
          ATC::uiImGuiAtcWindow->setLayer(atchelper::uiLayer_enum::setup_layer);
          ATC::uiImGuiAtcWindow->layer_config.flag_first_time = true; // force the draw_setup_layer() to run the first time initialization logic.
          ATC::uiImGuiAtcWindow->layer_main.flag_first_time = true; // force the draw_setup_layer() to run the first time initialization logic.
        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::task_load_xpath_data:
        {
          const std::string xpath_search_string = (ATC::uiImGuiAtcWindow)? ATC::uiImGuiAtcWindow->getSearchXMLPath() : "";

#ifndef RELEASE
          Log::logMsg("Using xpath: " + xpath_search_string + "\n");
#endif

          atc_data::xml_search_xpath_and_load_data_to_mapAtcCategories(atc_data::xml_main_doc_template_ptr, xpath_search_string);
          atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::push_loaded_xpath_data_to_window); // upload new XML information to uiImGuiAtcWindow Window

          //ATC::uiImGuiAtcWindow->setLayer(atchelper::uiLayer_enum::atc_layer);

        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::push_loaded_xpath_data_to_window:
        {
          if (atc_data::mapAtcCategories.size() > (size_t)0 && ATC::uiImGuiAtcWindow)
          {
            ATC::uiImGuiAtcWindow->setNewCategories(atc_data::mapAtcCategories, ATC::uiImGuiAtcWindow->layer_config.b_clear_option_values);
          }

        }
        break;
        case atchelper::atc_data::atc_flc_pre_command::task_reapply_user_option_on_text_from_main_thread:
        {
          if (ATC::uiImGuiAtcWindow )
          {
            ATC::uiImGuiAtcWindow->execAction(atchelper::mx_window_actions::ACTION_APPLY_USER_ATC_OPTION);
          }

        }
        break;

        default:
          break;
      }
    } // end while loop


  } // flc_TASKS

// ----------------------------

  void ATC::flc_serial_dispatcher()
  {
    //atc_data::atc_dispatch_task disp = atc_data::atc_dispatch_task::idle;
    //if (!atc_data::dqDispatcher.empty())
    //  disp = atc_data::dqDispatcher.front(); // we will pop out the action later in this function ONLY if the action needs to be executed
    //
    //if (atc_data::current_executing == atc_data::atc_dispatch_task::idle)
    //{
    //  if (atc_data::dqDispatcher.size() > 0)
    //  {
    //    atc_data::dqDispatcher.pop_front();
    //    switch (disp)
    //    {
    //    //case atc_data::atc_dispatch_task::toggle_atc_helper_window:
    //    //{
    //    //  atc_data::current_executing = disp;
    //    //  if (ATC::uiImGuiAtcWindow != nullptr)
    //    //  {
    //    //    ATC::uiImGuiAtcWindow->execAction(atchelper::mx_window_actions::ACTION_TOGGLE_WINDOW);
    //    //    atc_data::current_executing = atc_data::atc_dispatch_task::idle; // make sure to set the executing to idle so next action will be dealt. 
    //    //  }
    //    //}
    //    //break;
    //    default:
    //      break;
    //
    //    } // end switch
    //  } // end pop_pu dqDispatcher
    //} // end atc_data::current_executing == atc_data::idle
    //else // what to do if current executiion is not idle
    //{
    //  if (atc_data::current_executing == disp) // we will have to manually test the state of dispatched job and if it is back to idle or finished, we should set the execution parameter to idle.
    //  {
    //
    //  }
    //  //else
    //  //  Log::logDebugBO("[flc disp] wait for action: " + Utils::formatNumber<int>(atc_data::current_executing) + " to finish."); // show only in debug mode - to which action we wait for before doing next action
    //
    //}
  } // end flc_serial_dispatcher

// ----------------------------


  void ATC::stop_plugin()
  {
    atc_data::xml_main_doc_template_ptr.reset();
  }


// ----------------------------
// ----------------------------
// ----------------------------
// ----------------------------




}
