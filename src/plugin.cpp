#include "plugin.h"
//#include "core/base_xp_include.h"
//#include "core/xx_atc_helper_const.hpp"
//#include "core/Timer.hpp"
//#include "core/Utils.h"
//#include "core/atc_data.h"
#include "atc.h"
using namespace atchelper;

namespace atchelper
{
  atchelper::ATC atc;

  float pluginCallback(float inElapsedSinceLastCall, float inElapsedTimeSinceLastFlightLoop, int inCounter, void* inRefcon);
  int  drawCallback(XPLMDrawingPhase inPhase, int inIsBefore, void* inRefcon); // draw dlc = draw loop call back

  void pluginMenuHandler(void * inMenuRef, void * inItemRef);
  /* ******** Commands ************************** */
  XPLMCommandRef dummyRef = NULL; // 
  std::map <std::string, XPLMCommandRef> mapCommands;

  // command members
  int  cmd_toggleAtcHelperWindow(XPLMCommandRef inCommand, XPLMCommandPhase inPhase, void* inRefcon);

  
}

// ---------------------------------------------------------
// Mandatory function. START
PLUGIN_API int XPluginStart(char * outName, char * outSig, char * outDesc)
{
  XPLMEnableFeature("XPLM_USE_NATIVE_PATHS", 1);
 
  atchelper::Log::set_logFile(std::string( atchelper::atc_data::PLUGIN_PATH) + "atchelper.log") ; // set our plugin log file path

 
  std::memset(LOG_BUFF, '\0', LOG_BUFF_SIZE); // First time initialization of LOG_BUFF so it will have a concrete set of memory to work on.

  // Register Plugin
  std::string plugin_name = "ATC Helper " + std::string(PLUGIN_VER_MAJOR) + "." + std::string(PLUGIN_VER_MINOR) + "." + std::string(PLUGIN_REVISION);
  std::string plugin_sig  = atchelper::PLUGIN_NAME + "-snagar.dev";
  std::string plugin_desc = "Online ATC Helper, Like Cue Cards to help communicate with live ATC like: Vatsim";

#if defined IBM
  strncpy_s(outName, 255, plugin_name.c_str(), sizeof(plugin_name) * sizeof(std::string));
  strncpy_s(outSig , 255, plugin_sig.c_str() , sizeof( plugin_sig) * sizeof(std::string));
  strncpy_s(outDesc, 255, plugin_desc.c_str(), sizeof(plugin_desc) * sizeof(std::string));
//#elif defined LIN
#else
  std::strncpy(outName, plugin_name.c_str() , 255);
  std::strncpy(outSig , plugin_sig.c_str()  , 255 );
  std::strncpy(outDesc, plugin_desc.c_str() , 255);
#endif
  // END Register Plugin

  // register callbacks
  XPLMRegisterFlightLoopCallback(pluginCallback, -1, NULL);


  // MENU
  int plugin_menu = XPLMAppendMenuItem(XPLMFindPluginsMenu(), outName, (void *)atchelper::ATC::plugin_menuIdRefs::ATC_HELPER_MENU_ENTRY, 1);
  atchelper::ATC::atcMenuEntry = XPLMCreateMenu(outName, XPLMFindPluginsMenu(), plugin_menu, &pluginMenuHandler, NULL);

  // optimized sub menu
  atchelper::ATC::atc_menu.toggle_draw_state_menu = XPLMAppendMenuItem(atchelper::ATC::atcMenuEntry, "Toggle ATC Helper", (void*)atchelper::ATC::TOGGLE_ATC_HELPER_WINDOW, 1); // toggle drawing state
  XPLMAppendMenuSeparator(atchelper::ATC::atcMenuEntry);


  // COMMANDS
// Create and Register our custom command
// 1. in command string name - ad hock 2. function handler.   3. Receive input before plugin windows., 4. inRefcon <- I think when using C++ function we should do something like the following: "MXCKeySniffer * me = reinterpret_cast<MXCKeySniffer *>(inRefCon);"      
  mapCommands["ToggleATCHelper"] = XPLMCreateCommand("atc-helper/toggleAtcWindow", "Toggle ATC Helper Window");
  XPLMRegisterCommandHandler(mapCommands["ToggleATCHelper"], cmd_toggleAtcHelperWindow, 1, (void*)0);


  //Log::logXPLMDebugString(">>>>>>>>>>>> END Loading ATC-Helper <<<<<<<<<<<<\n");

  return 1;
}

// ---------------------------------------------------------
// Mandatory Function STOP
PLUGIN_API void XPluginStop(void)
{
  atc_data::flag_drawcallback_is_active = false;
  atc_data::flag_can_draw = false;

  atc.stop_plugin();

  // abort Log writeMessage
  Log::stop_plugin(); 

  if (atchelper::ATC::uiImGuiAtcWindow)
  {
    atchelper::ATC::uiImGuiAtcWindow->ImgWindow::sFontAtlas.reset();
    atchelper::ATC::uiImGuiAtcWindow = nullptr;
  }

   XPLMDebugString("Plug-in stopped");
}

// ---------------------------------------------------------
// Mandatory Function ENABLE
PLUGIN_API int XPluginEnable(void)
{
  // register callbacks
  XPLMRegisterFlightLoopCallback(pluginCallback, -1, NULL);
  atc_data::flag_drawcallback_is_active = false;
  atc_data::flag_can_draw = false;


  return 1; // important so callback will continue
}

// ---------------------------------------------------------
// Mandatory Function DISABLE
PLUGIN_API void XPluginDisable(void)
{
  //debug
  Log::logMsg("Plug-in Disabling");
  // unregister callbacks
  XPLMUnregisterFlightLoopCallback(pluginCallback, NULL);
  XPLMUnregisterDrawCallback(drawCallback, xplm_Phase_Objects, 0, 0);

  atc_data::flag_drawcallback_is_active = false;
  atc_data::flag_can_draw = false;

  // abort Log writeMessage
  Log::stop_plugin(); // v3.0.217.8

  //debug
  Log::logMsg("Plug-in Disabled");
}

// ---------------------------------------------------------
PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, intptr_t inMsg, void * inParam)
{
#ifdef DEBUG

 // Log::logMsg ("[PluginMsg]Message Sent: " + Utils:: );
#endif

  switch (inMsg)
  {
  case XPLM_MSG_ENTERED_VR:
  {
    atc_data::flag_in_vr = true; 
    atc_data::flag_prev_in_vr = false; 
  }
  break;
  case XPLM_MSG_EXITING_VR:
  {
    atc_data::flag_in_vr = false;
    atc_data::flag_prev_in_vr = true;
  }
    break;
  default:
    break;
  } // switch
}

// ---------------------------------------------------------

namespace atchelper
{

  void pluginMenuHandler(void * inMenuRef, void * inItemRef)
  {
    // Main Menu Dispatcher that calls the function that creates each Widget

    switch (((intptr_t)inItemRef))
    {
    case ATC::TOGGLE_ATC_HELPER_WINDOW:
    {
     Log::logDebugBO("[plugin] Toggle ATC Helper Screen");
     atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::toggle_atc_helper_window);
    }
    break;
    default:
      break;

    } // end switch


  } // end pluginMenuHandler

  // ---------------------------------------------------------

  int cmd_toggleAtcHelperWindow(XPLMCommandRef inCommand, XPLMCommandPhase inPhase, void* inRefcon)
  {
    if (inPhase == xplm_CommandBegin)
    {
      atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::toggle_atc_helper_window);
    }

    return 0;
  }

  int drawCallback(XPLMDrawingPhase inPhase, int inIsBefore, void* inRefcon)
  {
    //atchelper::rwm.draw(inPhase, inIsBefore, inRefcon);

    return 1;
  }


  // ---------------------------------------------------------
  
  // ---------------------------------------------------------
  float pluginCallback(float inElapsedSinceLastCall, float inElapsedTimeSinceLastFlightLoop, int inCounter, void * inRefcon)
  {

    atc.flc();

    return 1.0f;
  }


  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // ---------------------------------------------------------

}
