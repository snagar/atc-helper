#pragma once
#ifndef ATCHELPERIMGUIWINDOW_H_
#define ATCHELPERIMGUIWINDOW_H_

#include <functional>
#include "../core/base_c_includes.h"
#include "../core/xx_atc_helper_const.hpp"
#include "../core/atc_data.h"

#include "IconsFontAwesome5.h" // inside libs/imgui4xp
#include "ImgWindow/ImgWindow.h" // inside libs/imgui4xp


namespace atchelper
{
  class WinAtcHelper : public ImgWindow
  {
  private:
    ImVec2 uv0 = { 0.0f, 0.0f };
    ImVec2 uv1 = { 1.0f, 1.0f };
    const int BTN_PADDING{ 2 };

    const float WINDOW_BORDER_PADDING = 20.0f; // for scrollbar, for example
    const std::string WIN_TITLE = atchelper::PLUGIN_NAME; // + " v" + atchelper::FULL_VERSION;
  public:
    // Counter for the number of windows opened
    static int      num_win;
    // I am window number...
    const int       myWinNum;
    // Note to myself that a change of window mode is requested
    XPLMWindowPositioningMode nextWinPosMode = -1;
    // Our flight loop callback in case we need one
    XPLMFlightLoopID flId = nullptr;

    WinAtcHelper(int left, int top, int right, int bot,
      XPLMWindowDecoration decoration = xplm_WindowDecorationRoundRectangle, // xplm_WindowDecorationSelfDecoratedResizable
      XPLMWindowLayer layer = xplm_WindowLayerFloatingWindows);
    ~WinAtcHelper() override;

    XPLMWindowID mWindow;
    static int MAX_WIDTH;
    static int MAX_HEIGHT;
    static int LINE_HEIGHT;
    static int OPTION_BOTTOM_PADDING;
 
    int win_pad{ 75 };      ///< distance from left and top border
    const int win_coll_pad{ 30 };      ///< offset of collated windows

    //// MEMBERS
    void flc() override;
    void execAction(atchelper::mx_window_actions actionCommand); // special function to handle specific requests from outside of the window

    void setNewCategories(std::map <int, atchelper::atc_category_strct>& inMapAtcCategories, bool b_inResetOptionValues = true);
    std::string getSearchXMLPath();
    static std::string get_phonetic_or_string_by_user_preference(bool& inUserPick_b, const std::string& inText, const bool inTranslateNumerics_b = false);

    ////////////////////////////
    // Draw function
    void setLayer(atchelper::uiLayer_enum inLayer);


    ///// STRUCTS / LAYERS /////////////////
    typedef struct _option_values_strct
    {
      enm_option_type option_type{ atchelper::enm_option_type::option_byValue };

      bool b_apply_as_value{ true }; // true = apply as option_val_s, false = apply as option_val_phonetic_s
      bool b_apply_as_elv_phonetic{ false }; // b_apply_as_elv_phonetic with b_apply_as_value=false will use elevation phonetic

      std::string option_val_s{ "" };
      std::string option_val_phonetic_s{ "" };

      atchelper::enm_option_type getOptionType() {
        return (b_apply_as_value) ? atchelper::enm_option_type::option_byValue : (b_apply_as_elv_phonetic) ? atchelper::enm_option_type::option_by_phonetic_elev : atchelper::enm_option_type::option_by_phonetic;
      }

      void operator=(const _option_values_strct other)
      {        
        b_apply_as_value = other.b_apply_as_value;
        b_apply_as_elv_phonetic = other.b_apply_as_elv_phonetic;
        option_val_s = other.option_val_s;
        option_val_phonetic_s = other.option_val_phonetic_s;
      }

    } option_values_strct;




    /// <summary>
    /// when user pick plane ICAO line from filter list, we need to split it using delimeter ":". The first 2 values is what we need to assign.
    /// </summary>
    /// <param name="inSelected"></param>
    typedef struct _config_strct
    {
      static bool flag_first_time;
      bool b_clear_option_values{ false };

      const int NOT_FOUND_I = -1;

      int conf_width{ 550 }; // from configuration file but we set defaults to be on the safe side
      int conf_height{ 250 };
      std::string callsign{ "" };
      std::string plane_type{ "" };
      std::string plane_manufacturer{ "" };
      std::string flight_type{ "" }; // IFR / VFR or the first one in the list if empty
      std::string comm_standard{ "" }; // communication preference. US/UK, we can add more in the XML
      std::string atis_code{ "[no atis info]" }; // atis code %atis_code%
      std::string ramp_start{ "%stand%" }; // ramp start
      std::string dep_airport{ "" }; // departure airport
      std::string dest_airport{ "" }; // destination airport


      std::map<std::string, std::vector<std::string>> mapFlightTypes_and_CommStandards_defined_in_xml_template;

      typedef struct _flightType_and_commStandards_strct {
        std::map <int, std::string> mapFlightTypes_seq_key_ui;
        std::map <std::string, int> mapFlightTypes_key_seq_translation; // this map is only a translation for mapFlightTypes_seq_key_ui
        std::map <int, std::vector<std::string>> mapCommStandards;
        std::vector <const char*> vecCommStandards_char; // holds the result from: getCommStandards_basedOnFlightType_asConstChar() to display in the Combo widget 
        void reset()
        {
          mapFlightTypes_key_seq_translation.clear();
          mapFlightTypes_seq_key_ui.clear();

          vecCommStandards_char.clear();
          mapCommStandards.clear();
        }
      } flightTypeAndStandard_strct;

      flightTypeAndStandard_strct flightTypesAndStandard_ui_strct;

      int user_picked_flight_type_i{ NOT_FOUND_I };
      int user_picked_comm_standard_i{ 0 };
      int user_picked_atis_code_i{ 0 };
      char buff_callsign[15];
      bool b_useCallsignPhonetic{ true };
      std::string callsign_phonetic{ "" };

      char buff_plane_type[10];
      bool b_usePlaneTypePhonetic{ true };
      std::string plane_type_phonetic{ "" };

      char buff_plane_manufacturer[20];


      char buff_ramp_start[10];
      std::string ramp_start_translated{ "" };

      char buff_dep_airport[25];
      char buff_destination_airport[25];

      void reset()
      {
        flightTypesAndStandard_ui_strct.reset();
      }

      ///// Members related to user pick //////
      void setPlaneType(const std::string inPlaneType)
      {
        plane_type = inPlaneType;
        std::memcpy(buff_plane_type, plane_type.c_str(), sizeof(buff_plane_type));
        plane_type_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_alphabet(plane_type, true);
      }

      void setManufacturer(const std::string inVal)
      {
        plane_manufacturer = inVal;
        std::memcpy(buff_plane_manufacturer, plane_manufacturer.c_str(), sizeof(buff_plane_manufacturer));
      }

      //std::string get_phonetic_callsign_by_user_preference()
      //{
      //  if (b_useCallsignPhonetic)
      //    return atchelper::atc_data::get_translated_string_to_phonetic_alphabet(callsign, true); 
      //  else
      //    return layer_config.callsign;
      //}

      void setCallsign(const std::string inCallsign)
      {
        callsign = inCallsign;
        std::memcpy(buff_callsign, callsign.c_str(), sizeof(buff_callsign));
        
        layer_config.callsign_phonetic = get_phonetic_or_string_by_user_preference(b_useCallsignPhonetic, callsign, false);
      }

      int getFlightTypeSeq(const std::string inValueToSearch)
      {

        for (auto& v : flightTypesAndStandard_ui_strct.mapFlightTypes_key_seq_translation)
        {
          if (inValueToSearch.compare(v.first) == 0)
            return v.second; // return the translated sequence number
        }

        return NOT_FOUND_I; // -1 means not found
      }


      void set_mapFlightTypes_and_CommStandards_defined_in_xml_template(std::map<std::string, std::vector<std::string>> inMap)
      {
        mapFlightTypes_and_CommStandards_defined_in_xml_template.clear();
        mapFlightTypes_and_CommStandards_defined_in_xml_template = inMap;
        // fill mapFlightTypes_radio_ui
        // mapFlightTypes_seq_key_ui - holds <seq, node name>
        // mapFlightTypes_key_seq_translation holds <node name, seq> for easy search. Yes, I know I could write a search function but it is a small map and neglegiable.
        // mapFlightTypes_key_seq_translation holds <seq, node name>. Standard node name: US, UK...
        int seq = 0;
        for (auto& v : mapFlightTypes_and_CommStandards_defined_in_xml_template)
        {
          flightTypesAndStandard_ui_strct.mapFlightTypes_seq_key_ui[seq] = v.first;
          flightTypesAndStandard_ui_strct.mapFlightTypes_key_seq_translation[v.first] = seq; // this way we can easily search the flightType seq unique number by name
          flightTypesAndStandard_ui_strct.mapCommStandards[seq] = v.second;

          ++seq;
        }
      } // set_mapFlightTypes_and_CommStandards_defined_in_xml_template


      std::vector <const char*> getCommStandards_basedOnFlightType_asConstChar(int in_picked_key_i) // this is like drill down, from parent to child
      {
        std::vector <const char*> vecCommStandards_char_result;
        // prepare the ui pointer for the combo
        if (mxUtils::isElementExists(flightTypesAndStandard_ui_strct.mapCommStandards, in_picked_key_i))
        {
          for (auto& v : flightTypesAndStandard_ui_strct.mapCommStandards[in_picked_key_i])
          {
            vecCommStandards_char_result.emplace_back(v.c_str());
          }
        }
        else
          vecCommStandards_char_result.clear();


        return vecCommStandards_char_result;
      } // getCommStandards_basedOnFlightType_asConstChar



    } atc_config_strct;
    static atc_config_strct layer_config; // hold configuration from file



    // Layer Main Struct
    typedef struct _layer_main
    {
      static bool flag_first_time;
      bool b_translate_numbers_as_phonetic{ true };
      bool b_display_helper_tab{ false }; // display the alphabetic helper image
      //bool b_helperCollapseHeader_01_1{ false };
      bool b_display_pattern_full{ false };
      ImVec2 vec2Pattern;

      int item_category_current{ 0 };
      int item_sub_category_current{ 0 };
      std::string_view markup_text_ptr{ "" };
      std::vector <std::string> vecCategoryChoice;
      std::vector <const char *> vecCategoryChoice_char; // pointer to vecCategoryChoice strings. If we clear the first we should clear the second.
      std::vector <std::string> vecSubCategoryChoice;
      std::vector <const char*> vecSubCategoryChoice_char;
      std::vector <std::string> vecSubCategoryText;    
      std::vector <std::string> vecSubCategoryText_copy; // save a copy of vecSubCategoryText in order to use with the user options replacement values

      ///// category options
      int item_option_current_i{ 0 };
      char buff_option_value[15];
      std::string option_string_value{ "" };
      std::string option_string_value_phonetic{ "" };
      std::map <std::string, option_values_strct> mapCategoryOptionsValues; // stores user prefered dynamic string. key represent the dynamic name, value: represent user entered value. We do not clear key until next x-plane  restart
      std::vector <std::string> vecCategoryOptions; // unique
      std::vector <const char*> vecCategoryOptions_chars; // points to vecCategoryOptions, used in ImGui::Choice widget

      _layer_main()
      {
        reset();
      }

      void reset()
      {
        item_category_current = 0;
        item_sub_category_current = 0;

        vecCategoryChoice_char.clear();
        vecSubCategoryChoice_char.clear();

        vecCategoryChoice.clear();
        vecSubCategoryChoice.clear();

        vecSubCategoryText.clear();

        markup_text_ptr = "";

        item_option_current_i = 0;
        //option_string_value.clear();
        //option_string_value_phonetic.clear();
        //vecCategoryOptions.clear();
        //vecCategoryOptions_chars.clear();
        //mapCategoryOptionsValues.clear();
      }

      bool initCategories(std::map <int, atchelper::atc_category_strct>& inMapAtcCategories)
      {
        vecCategoryChoice.clear();
        vecCategoryChoice_char.clear();
        for (auto& c : inMapAtcCategories)
        {
          vecCategoryChoice.emplace_back(c.second.category_name);    
        }
        // create the pointers for imgui::Choice
        for (auto& cat : vecCategoryChoice)
        {
          vecCategoryChoice_char.emplace_back(cat.c_str());
        }

        return vecCategoryChoice_char.size() > (size_t)0;
      } // initCategories


      void initSubCategories(std::map <int, atchelper::atc_category_strct>& inMapAtcCategories, std::string pickedCategory, bool resetCategories=true)
      {
        vecSubCategoryChoice.clear();
        vecSubCategoryChoice_char.clear(); // we must clear the pointers too
        vecSubCategoryText.clear();
        item_sub_category_current = (resetCategories) ? 0 : item_sub_category_current; // if user modified option value we want the screen to stay stationary and not reset to first subcategory
        markup_text_ptr = "";

        for (auto& c : inMapAtcCategories) // loop over category struct
        {
          if (c.second.category_name.compare(pickedCategory) == 0) // init only the picked category
          {
            auto task = c.second.category_tasks;
            for (auto t : c.second.category_tasks.mapName)
            {
              auto subCategoryName = t.second;
              const auto text = task.mapText[t.first];

              const auto modifiedText = WinAtcHelper::replaceSpecialCharsWithUserDefinedOnes(text); // replace %callsign%, %plane_type%" etc.. with user defined strings or defaults 

              vecSubCategoryChoice.emplace_back(subCategoryName); // this will place the name of the sub category
              vecSubCategoryText.emplace_back(modifiedText); // store the corresponding modified text using the sub category name sequence number.
            }

            // create the pointers so imgui::Choice will be able to read the vector data
            for (auto& sub : vecSubCategoryChoice)
            {
              vecSubCategoryChoice_char.emplace_back(sub.c_str());
            }

            // set text
            if (vecSubCategoryText.size() > item_sub_category_current)
              markup_text_ptr = vecSubCategoryText[item_sub_category_current];

            break; // exit the loop since we found the relevant category information
          }
        }

      } // end initSubCategories



    } atc_layer_main_strct;
    static atc_layer_main_strct layer_main;

  protected:
    std::map <int, atchelper::atc_category_strct> mapAtcCategories;

    void reset_atc_window();
    static std::string replaceSpecialCharsWithUserDefinedOnes(const std::string inText); // will be used to replace special strings with user data, like %collsign%

    // Main function: creates the window's UI
    void buildInterface() override;
    void draw_atc_layer();
    //void draw_bottom_atc_toolbar();
    void draw_setup_layer();


    //// Keep track of Layer change
    uiLayer_enum currentLayer{ atchelper::uiLayer_enum::setup_layer };
    uiLayer_enum prevLayer{ currentLayer };

  private:
    void (atchelper::WinAtcHelper::* drawFuncPtr)() {nullptr}; // declare a pointer to a function

    void get_and_set_plane_info_based_on_selected_string(const std::string inSelected);


#ifdef USE_IMGUI_MARKDOWN
    ImGui::MarkdownConfig mdConfig;
#endif

  };

}


#endif // !ATCHELPERIMGUIWINDOW_H_


