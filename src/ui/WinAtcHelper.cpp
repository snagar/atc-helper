#include "WinAtcHelper.h"

namespace atchelper
{
  int      WinAtcHelper::num_win;

  WinAtcHelper::atc_config_strct WinAtcHelper::layer_config;
  WinAtcHelper::atc_layer_main_strct WinAtcHelper::layer_main;

  bool WinAtcHelper::atc_config_strct::flag_first_time{ true };
  bool WinAtcHelper::atc_layer_main_strct::flag_first_time{ true };

  int WinAtcHelper::MAX_WIDTH = 700;
  int WinAtcHelper::MAX_HEIGHT = 800;
  int WinAtcHelper::LINE_HEIGHT = 25;
  int WinAtcHelper::OPTION_BOTTOM_PADDING = 10;
}



//////////////////////// CLASS Memebers  //////////////////////////

atchelper::WinAtcHelper::WinAtcHelper(int left, int top, int right, int bot, XPLMWindowDecoration decoration, XPLMWindowLayer layer)
  : ImgWindow(left, top, right, bot, decoration, layer),
  myWinNum(++num_win)             // assign a unique window number
{
  // Disable reading/writing of "imgui.ini"
  ImGuiIO& io = ImGui::GetIO();
  io.IniFilename = nullptr;

  // We take the parameter combination "SelfDecorateResizeable" + "LayerFlightOverlay"
  // to mean: simulate HUD
  if (decoration == xplm_WindowDecorationSelfDecoratedResizable && (layer == xplm_WindowLayerFlightOverlay || layer == xplm_WindowLayerFloatingWindows))
  {
    // let's set a fairly transparent, barely visible background
    ImGuiStyle& style = ImGui::GetStyle();
    style.Colors[ImGuiCol_WindowBg] = ImColor(0, 0, 0, 150);
    // There's no window decoration, so to move the window we need to
    // activate a "drag area", here a small strip (roughly double text height)
    // at the top of the window, ie. the window can be moved by
    // dragging a spot near the window's top
    SetWindowDragArea(0, 5, INT_MAX, 5 + 2 * int(atchelper::DEFAULT_FONT_SIZE));
  }

  // Define our own window title
  SetWindowTitle(WIN_TITLE);
  SetWindowResizingLimits(300, 150, WinAtcHelper::MAX_WIDTH, 400); // minW. minH. maxW, maxH
  SetVisible(true); // set visibility of window: yes/no

  this->mWindow = this->GetWindowId();


  // Markdown Config
#ifdef USE_IMGUI_MARKDOWN
  mdConfig.linkCallback = NULL;
  mdConfig.tooltipCallback = NULL;
  mdConfig.imageCallback = NULL;
  mdConfig.linkIcon = ICON_FA_LINK;
  mdConfig.headingFormats[0] = { NULL, true };
  mdConfig.headingFormats[1] = { NULL, true };
  mdConfig.headingFormats[2] = { NULL, false };
  mdConfig.userData = NULL;
#endif
}


// -------------------------------------------------

atchelper::WinAtcHelper::~WinAtcHelper()
{
}


// -------------------------------------------------

void atchelper::WinAtcHelper::reset_atc_window()
{
  setLayer(atchelper::uiLayer_enum::setup_layer);

  WinAtcHelper::layer_main.reset();
  WinAtcHelper::layer_config.reset();
  this->mapAtcCategories.clear();
}

// -------------------------------------------------

std::string atchelper::WinAtcHelper::replaceSpecialCharsWithUserDefinedOnes(const std::string inText)
{
  std::string text = inText;


  std::map <std::string, std::string> mapReplaceKeywords = { 
    {atchelper::ATC_CALLSIGN, atchelper::ATC_CALLSIGN}, {atchelper::ATC_PLANE_TYPE, atchelper::ATC_PLANE_TYPE},{atchelper::ATC_MANUFACTURER, atchelper::ATC_MANUFACTURER}
    ,{atchelper::ATC_DEP_AIRPORT, atchelper::ATC_DEP_AIRPORT},{atchelper::ATC_DEST_AIRPORT, atchelper::ATC_DEST_AIRPORT},{atchelper::ATC_STAND, atchelper::ATC_STAND}
    ,{atchelper::ATC_ATIS_CODE, atchelper::ATC_ATIS_CODE}
  };
#ifdef DEFINE_MARKDOWN_TAGS_IN_PLUGIN
  if (!WinAtcHelper::layer_config.callsign.empty())
    mapReplaceKeywords[atchelper::ATC_CALLSIGN] = "[" + mxUtils::rtrim(WinAtcHelper::layer_config.callsign_phonetic) + "]()(" + " *" + WinAtcHelper::layer_config.callsign + "* )";
  if (!WinAtcHelper::layer_config.plane_type.empty())
    mapReplaceKeywords[atchelper::ATC_PLANE_TYPE] = "*" + WinAtcHelper::layer_config.plane_type + "*";
  if (!WinAtcHelper::layer_config.dep_airport.empty())
    mapReplaceKeywords[atchelper::ATC_DEP_AIRPORT] = "*" + WinAtcHelper::layer_config.dep_airport + "*";
  if (!WinAtcHelper::layer_config.dest_airport.empty())
    mapReplaceKeywords[atchelper::ATC_DEST_AIRPORT] = "*" + WinAtcHelper::layer_config.dest_airport + "*";
  if (!WinAtcHelper::layer_config.ramp_start.empty())
    mapReplaceKeywords[atchelper::ATC_STAND] = "**" + mxUtils::rtrim(WinAtcHelper::layer_config.ramp_start_translated) + "** (*" + WinAtcHelper::layer_config.ramp_start + "*)";
  if (!WinAtcHelper::layer_config.atis_code.empty())
    mapReplaceKeywords[atchelper::ATC_ATIS_CODE] = "*" + WinAtcHelper::layer_config.atis_code + "*";
#else
  if (!WinAtcHelper::layer_config.callsign.empty())
  {
    if (WinAtcHelper::layer_config.b_useCallsignPhonetic)
    {
      mapReplaceKeywords[atchelper::ATC_CALLSIGN] = "[" + mxUtils::rtrim(WinAtcHelper::layer_config.callsign_phonetic) + "]() (" + mxUtils::trim(WinAtcHelper::layer_config.callsign) + ")"; // a hyperlink needs [label] and (http://) format. Hence: [callsign]() with empty url.

    }
    else
    {
      mapReplaceKeywords[atchelper::ATC_CALLSIGN] = "[" + mxUtils::rtrim(WinAtcHelper::layer_config.callsign) + "]()"; // a hyperlink needs [label] and (http://) format. Hence: [callsign]() with empty url.

    }
  }
  if (!WinAtcHelper::layer_config.plane_type.empty())
  {
    if (WinAtcHelper::layer_config.b_usePlaneTypePhonetic) // v0.4.0 fixed plane_type always shows phonetic option
      mapReplaceKeywords[atchelper::ATC_PLANE_TYPE] = "*" + mxUtils::rtrim(WinAtcHelper::layer_config.plane_type_phonetic) + "* (" + WinAtcHelper::layer_config.plane_type + ")";
    else 
      mapReplaceKeywords[atchelper::ATC_PLANE_TYPE] = "*" + mxUtils::rtrim(WinAtcHelper::layer_config.plane_type) + "*";
  }

  if (!WinAtcHelper::layer_config.plane_manufacturer.empty()) // v0.3.0
    mapReplaceKeywords[atchelper::ATC_MANUFACTURER] = WinAtcHelper::layer_config.plane_manufacturer;

  if (!WinAtcHelper::layer_config.dep_airport.empty())
    mapReplaceKeywords[atchelper::ATC_DEP_AIRPORT] = WinAtcHelper::layer_config.dep_airport;
  if (!WinAtcHelper::layer_config.dest_airport.empty())
    mapReplaceKeywords[atchelper::ATC_DEST_AIRPORT] = WinAtcHelper::layer_config.dest_airport;

  if (!WinAtcHelper::layer_config.ramp_start_translated.empty())
    mapReplaceKeywords[atchelper::ATC_STAND] = "*" + mxUtils::rtrim(WinAtcHelper::layer_config.ramp_start_translated) + "* (" + WinAtcHelper::layer_config.ramp_start + ")";
  if (!WinAtcHelper::layer_config.atis_code.empty())
    mapReplaceKeywords[atchelper::ATC_ATIS_CODE] = WinAtcHelper::layer_config.atis_code;

  // Add user options
  for (auto& option_s : WinAtcHelper::layer_main.mapCategoryOptionsValues)
  {
    if (option_s.second.option_val_s.empty())
      mapReplaceKeywords[option_s.first] = option_s.first;
    else
      mapReplaceKeywords[option_s.first] = ((option_s.second.b_apply_as_value) ? option_s.second.option_val_s : option_s.second.option_val_phonetic_s);

  }

#endif // USE_TAGS_IN_PLUGIN


  if (!text.empty())
  {


    for (auto& r : mapReplaceKeywords) // replace each keyword with the value from mapReplaceKeywords
    {
      if (r.first.compare(r.second) != 0)
        text = Utils::replaceStringWithOtherString(text, r.first, r.second, true); // task text will be replacing r.first string with r.second string.
    }
#ifndef RELEASE
    Log::logMsg(">>>>  Replaceing Text Result: <<<<\n" + text);
#endif // !RELEASE
  }

  return text;
}

// -------------------------------------------------

void atchelper::WinAtcHelper::buildInterface()
{

  if (!this->GetVisible())
    return;

  //ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_yellow); //  yellow
  //ImGui::TextUnformatted(WIN_TITLE.c_str());
  //ImGui::PopStyleColor(1);

  // If we are a transparent HUD-like window then we draw 3 lines that look
  // a bit like a head...so people know where to drag the window to move it
  if (HasWindowDragArea()) {
    ImGui::SameLine();
    ImDrawList* draw_list = ImGui::GetWindowDrawList();
    ImVec2 pos_start = ImGui::GetCursorPos();
    float x_end = ImGui::GetWindowContentRegionWidth() - 75;
    for (int i = 0; i < 3; i++) {
      draw_list->AddLine(pos_start, { x_end, pos_start.y }, IM_COL32(0xa0, 0xa0, 0xa0, 255), 1.0f);
      pos_start.y += 5;
    }
  }

  // Button with fixed width 30 and standard height
  // to pop out the window in an OS window
  static float btnWidth = ImGui::CalcTextSize(ICON_FA_WINDOW_MAXIMIZE).x + 5;
  const bool bBtnPopOut = !this->IsPoppedOut();
  const bool bBtnPopIn = IsPoppedOut() || IsInVR();
  const bool bBtnVR = atchelper::atc_data::flag_in_vr && !IsInVR();
  int numBtn = bBtnPopOut + bBtnVR + bBtnPopIn;


  if (numBtn > 0) {
    // Setup colors for window sizing buttons
    ImGui::PushStyleColor(ImGuiCol_Text, ImGui::GetColorU32(ImGuiCol_ScrollbarGrabActive)); // dark gray
    ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32_BLACK_TRANS);                           // transparent
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImGui::GetColorU32(ImGuiCol_ScrollbarGrab)); // lighter gray

    if (bBtnVR) {
      // Same line, but right-alinged
      ImGui::SameLine(ImGui::GetWindowContentRegionWidth() - (numBtn * btnWidth));
      if (this->ButtonTooltip(ICON_FA_EXTERNAL_LINK_SQUARE_ALT, "Move into VR"))
        nextWinPosMode = xplm_WindowVR;
      --numBtn;
    }
    if (bBtnPopIn) {
      // Same line, but right-alinged
      ImGui::SameLine(ImGui::GetWindowContentRegionWidth() - (numBtn * btnWidth));
      if (this->ButtonTooltip(ICON_FA_WINDOW_RESTORE, "Move back into X-Plane"))
        nextWinPosMode = xplm_WindowPositionFree;
      --numBtn;
    }
    if (bBtnPopOut) {
      // Same line, but right-alinged
      ImGui::SameLine(ImGui::GetWindowContentRegionWidth() - (numBtn * btnWidth));
      if (this->ButtonTooltip(ICON_FA_WINDOW_MAXIMIZE, "Pop out into separate window"))
        nextWinPosMode = xplm_WindowPopOut;
      --numBtn;
    }

    // Restore colors
    ImGui::PopStyleColor(3);

    // Window mode should be set outside drawing calls to avoid crashes
    if (nextWinPosMode >= 0 && flId)
      XPLMScheduleFlightLoop(flId, -1.0, 1);
    /// End drawing popout buttons

  } // end drawing popout/in buttons

  //  ///// Setup button - saar
  // SETUP button on all layers
  {
    ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_whitesmoke); // 
    ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32_BLACK_TRANS);                           // transparent
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, atchelper::color::color_vec4_yellowgreen); // 
    ImGui::SameLine(10.0, 0.0);
    if (ImgWindow::ButtonTooltip(ICON_FA_COG"##SetupToolbarBtn", "Setup"))
      WinAtcHelper::setLayer(atchelper::uiLayer_enum::setup_layer);

    ImGui::PopStyleColor(3);
  }
  // -- End setup button

  // Save configuration button
  if (this->currentLayer == atchelper::uiLayer_enum::setup_layer)
  {
    ImGui::SameLine(0.0f, 5.0f);
    ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_skyblue); // 
    ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32_BLACK_TRANS);                           // transparent
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, atchelper::color::color_vec4_yellowgreen); // 

    if (ImgWindow::ButtonTooltip(ICON_FA_SAVE"##SetupToolbarBtn", "Save Config"))
      this->execAction(atchelper::mx_window_actions::ACTION_SAVE_CONFIGURATION);

    ImGui::PopStyleColor(3);
    ImGui::SameLine();
  }
  else // draw in main layer (the ATC helper layer)
  {
    ImGui::SameLine(0.0f, 5.0f);
    ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_skyblue); // 
    ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32_BLACK_TRANS);                           // transparent
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, atchelper::color::color_vec4_yellowgreen); // 

    if (ImgWindow::ButtonTooltip(ICON_FA_FILE_CODE"##SetupToolbarBtn", "Alphabet"))
    {
      this->layer_main.b_display_helper_tab = !this->layer_main.b_display_helper_tab;
    }

    ImGui::PopStyleColor(3);
    ImGui::SameLine();
  }


  // Scale font buttons
  {
    const auto currentFontScale = ImGui::GetCurrentContext()->CurrentWindow->FontWindowScale;
    ImVec2 const contentRegionSize = ImGui::GetContentRegionAvail();
    //ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.14f, 0.6f, 0.6f));
    //ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.14f, 0.7f, 0.7f));
    //ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.14f, 0.8f, 0.8f));

    ImGui::SetCursorPosX(contentRegionSize.x * 0.5f - 35.0f);


    //if (ImGui::Button("+", ImVec2(30.0f, 20.0f) ) && currentFontScale <= atchelper::DEFAULT_MAX_FONT_SCALE)
    if (ImgWindow::ButtonTooltip(ICON_FA_PLUS"##SclaeFontUp", "Upscale font size", IM_COL32(1, 1, 1, 0), IM_COL32(1, 1, 1, 0), ImVec2(30.0f, 20.0f)) && currentFontScale <= atchelper::DEFAULT_MAX_FONT_SCALE)
    {
      ImGui::SetWindowFontScale(currentFontScale + 0.1f);
    }
    ImGui::SameLine(0.0f, 10.0f);
    //if (ImGui::Button("-", ImVec2(30.0f, 20.0f)) && currentFontScale >= atchelper::DEFAULT_MIN_FONT_SCALE)
    if (ImgWindow::ButtonTooltip(ICON_FA_MINUS"##SclaeFontDown", "Downscale font size", IM_COL32(1, 1, 1, 0), IM_COL32(1, 1, 1, 0), ImVec2(30.0f, 20.0f)) && currentFontScale >= atchelper::DEFAULT_MIN_FONT_SCALE)

    {
      ImGui::SetWindowFontScale(currentFontScale - 0.1f);
    }
    //ImGui::PopStyleColor(3);


  }




  // Background color using button and texture. After adding this button we shoud reset the focus location using setCursorPosX/Y
  //ImVec2 window_size_vec2(ImGui::GetCurrentContext());
  //ImGui::ImageButton((void*)(intptr_t)atc_data::mapPluginTextures[atchelper::BITMAP_BACKGROUND].gTexture, window_size_vec2, uv0, uv1, this->BTN_PADDING, ImVec4(0.0f, 0.0f, 0.0f, 1.0f));


  // Call function to draw:
  //if (this->drawFuncPtr != nullptr)
    //drawFuncPtr;
  switch (this->currentLayer)
  {
  case atchelper::uiLayer_enum::atc_layer:
  {
    draw_atc_layer();
  }
  break;
  case atchelper::uiLayer_enum::setup_layer:
  {
    draw_setup_layer();
  }
  break;
  default:
    this->drawFuncPtr = nullptr;
    break;
  }
}

// -------------------------------------------------

void atchelper::WinAtcHelper::flc()
{

  if (this->nextWinPosMode >= 0)
  {
    this->SetWindowPositioningMode(this->nextWinPosMode);
    // If we pop in, then we need to explicitely set a position for the window to appear
    if (this->nextWinPosMode == xplm_WindowPositionFree) {
      int left, top, right, bottom;
      this->GetCurrentWindowGeometry(left, top, right, bottom);
      // Normalize to our starting position (WIN_PAD|WIN_PAD), but keep size unchanged
      const int width = right - left;
      const int height = top - bottom;
      Utils::CalcWinCoords(width, height, this->win_pad, this->win_coll_pad, left, top, right, bottom);
      right = left + width;
      bottom = top - height;
      this->SetWindowGeometry(left, top, right, bottom);
    }
    this->nextWinPosMode = -1;
  }

  // decide pattern image size
  if (currentLayer == atchelper::uiLayer_enum::atc_layer && layer_main.b_display_helper_tab)
  {
    if (layer_main.b_display_pattern_full)
      layer_main.vec2Pattern = ImVec2(atc_data::mapPluginTextures[atchelper::BITMAP_PATTERN].sImageData.getW_f(), atc_data::mapPluginTextures[atchelper::BITMAP_PATTERN].sImageData.getH_f());
    else
      layer_main.vec2Pattern = ImVec2(330.0f, 132.0f);
  }
}

// -------------------------------------------------

void atchelper::WinAtcHelper::setNewCategories(std::map <int, atchelper::atc_category_strct>& inMapAtcCategories, bool b_inResetOptionValues)
{
  if (!inMapAtcCategories.empty())
  {
    this->mapAtcCategories.clear();
    this->mapAtcCategories = inMapAtcCategories;

    if (this->layer_main.initCategories(this->mapAtcCategories))
    {    
      assert(this->layer_main.vecCategoryChoice.size() && "Category vector was not initialized correctly.");

      this->layer_main.initSubCategories(this->mapAtcCategories, this->layer_main.vecCategoryChoice.front());
    }

    // store all dynamic options localy, independent of the category map. This is for not being dependent outside of the class

    layer_main.vecCategoryOptions.clear();
    layer_main.vecCategoryOptions_chars.clear();
    if (b_inResetOptionValues)
      this->layer_main.mapCategoryOptionsValues.clear();

    // prepare a std:set. It will also order the values
    std::set <std::string> setOptions;
    for (auto& category : inMapAtcCategories)
    {
      for (auto& option : category.second.vecDynamicOptions)
      {
        setOptions.emplace(Utils::trim(option));
      }
    }

    for (auto& o : setOptions ) // prepare the option layer with the ordered set values
      layer_main.vecCategoryOptions.emplace_back(o);

    // build the pointers to the options for the choice element
    // add options to mapCategoryOptionsValues.
    for (auto &option : layer_main.vecCategoryOptions)
    {
      option_values_strct empty;

      layer_main.vecCategoryOptions_chars.emplace_back(option.c_str());

      if (b_inResetOptionValues || !mxUtils::isElementExists(layer_main.mapCategoryOptionsValues, option) ) // if we need to clear options or there is a new option that does not eists in the map
        this->layer_main.mapCategoryOptionsValues[option] = empty; // used operator= in the struct
    }

  }

  // reset "clear options" checkbox to false
  this->layer_config.b_clear_option_values = false;
}

// -------------------------------------------------

std::string atchelper::WinAtcHelper::getSearchXMLPath()
{

  return "/ATC/" + WinAtcHelper::layer_config.flight_type + "/" + WinAtcHelper::layer_config.comm_standard; // return the XML search path. Must be in template format
}

// -------------------------------------------------

void atchelper::WinAtcHelper::setLayer(atchelper::uiLayer_enum inLayer)
{
  this->prevLayer = this->currentLayer;
  this->currentLayer = inLayer;
  switch (inLayer)
  {
    case atchelper::uiLayer_enum::atc_layer:
    {
      drawFuncPtr = &atchelper::WinAtcHelper::draw_atc_layer;
     
    }
    break;
    case atchelper::uiLayer_enum::setup_layer:
    {
      drawFuncPtr = &atchelper::WinAtcHelper::draw_setup_layer;
    }
    break;
    default:
      this->drawFuncPtr = nullptr;
      break;
  } // end switch


}

std::string atchelper::WinAtcHelper::get_phonetic_or_string_by_user_preference(bool& inUserPick_b, const std::string& inText, const bool inTranslateNumerics_b)
{
  return (inUserPick_b) ? atchelper::atc_data::get_translated_string_to_phonetic_alphabet(inText, inTranslateNumerics_b) : inText;
}

// -------------------------------------------------

void atchelper::WinAtcHelper::get_and_set_plane_info_based_on_selected_string(const std::string inSelected)
{
  const std::vector<std::string> vecValues = Utils::split(inSelected, ':');
  if (vecValues.size() > (size_t)1)
  {
    std::memcpy(this->layer_config.buff_plane_type, mxUtils::trim(vecValues.at(0)).c_str(), sizeof(this->layer_config.buff_plane_type));
    std::memcpy(this->layer_config.buff_plane_manufacturer, mxUtils::trim(vecValues.at(1)).c_str(), sizeof(this->layer_config.buff_plane_manufacturer));
    this->layer_config.plane_type = this->layer_config.buff_plane_type;
    this->layer_config.plane_manufacturer = this->layer_config.buff_plane_manufacturer;
  }
}

// -------------------------------------------------

// -------------------------------------------------

void atchelper::WinAtcHelper::draw_setup_layer()
{
  
  ImVec2 window_size_vec2(ImGui::GetWindowSize());
  ImVec2 current_context(ImGui::GetContentRegionAvail());
  auto storedCursorPos = ImGui::GetCursorPos();

  // ---- ROW 1 Flight Type
  if (WinAtcHelper::layer_config.flag_first_time)
  {
    WinAtcHelper::layer_config.user_picked_flight_type_i = layer_config.getFlightTypeSeq(layer_config.flight_type);

    // if our INI config value is not same as the one in the list, pick the first one from the list - if there is any
    // todo: update config file after this action
    if (WinAtcHelper::layer_config.user_picked_flight_type_i == layer_config.NOT_FOUND_I && !layer_config.flightTypesAndStandard_ui_strct.mapFlightTypes_seq_key_ui.empty())
    {
      WinAtcHelper::layer_config.user_picked_flight_type_i = layer_config.getFlightTypeSeq(layer_config.flightTypesAndStandard_ui_strct.mapFlightTypes_seq_key_ui.cbegin()->second);
    }

    WinAtcHelper::layer_config.flightTypesAndStandard_ui_strct.vecCommStandards_char = layer_config.getCommStandards_basedOnFlightType_asConstChar(layer_config.user_picked_flight_type_i);

    layer_config.flag_first_time = false;
  }


  ImGui::BeginGroup();
  ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_yellow); // yellow
  //std::string ui_debug = "winY: " + Utils::formatNumber<float>(window_size_vec2.y) + ", ctxY: " + Utils::formatNumber<float>(current_context.y);
  //ImGui::TextUnformatted(std::string("Enter as much information as you can: " + ui_debug).c_str() ) ;
  ImGui::TextUnformatted("Enter as much information as you can:");
  ImGui::PopStyleColor();

  if (WinAtcHelper::layer_config.user_picked_flight_type_i == layer_config.NOT_FOUND_I)
  {
    ImGui::TextWrapped("There might be an issue with the ATC XML Template.\nPlease check the template file, or clear the config file in the plugin's folder.");
    ImGui::TextWrapped(atchelper::atc_data::load_xml_err_s.c_str()); // v0.3.0
  }
  else
  {
    // draw Flight Types  IFR/VFR
    for (auto &ft : WinAtcHelper::layer_config.flightTypesAndStandard_ui_strct.mapFlightTypes_seq_key_ui)
    {
      if (ImGui::RadioButton(ft.second.c_str(), ft.first == layer_config.user_picked_flight_type_i))
      {
        layer_config.user_picked_flight_type_i = ft.first;
        WinAtcHelper::layer_config.flightTypesAndStandard_ui_strct.vecCommStandards_char = WinAtcHelper::layer_config.getCommStandards_basedOnFlightType_asConstChar(ft.first);
        WinAtcHelper::layer_config.user_picked_comm_standard_i = 0; // set to first element
      }

      ImGui::SameLine(0.0f, 10.0f);
    }


    // ---- ROW 2: Communication Standard
    // Draw Communication Standard Options - which are drill down from Flight Types pick
    ImGui::NewLine();
    ImGui::PushItemWidth(100.0f); // 
    ImGui::Combo("Communication Standard", &WinAtcHelper::layer_config.user_picked_comm_standard_i, WinAtcHelper::layer_config.flightTypesAndStandard_ui_strct.vecCommStandards_char.data(), (int)WinAtcHelper::layer_config.flightTypesAndStandard_ui_strct.vecCommStandards_char.size());


    // ---- ROW 3: Callsign
    // Checkbox - disable enable phonetic for callsign - for airliners
    if (ImGui::Checkbox("##CallsignNoPhonetico", &this->layer_config.b_useCallsignPhonetic))
    {
      //layer_config.callsign_phonetic = this->layer_config.get_phonetic_callsign_by_user_preference();
      layer_config.callsign_phonetic = this->get_phonetic_or_string_by_user_preference(this->layer_config.b_useCallsignPhonetic, layer_config.callsign, false);
    }
    this->add_tooltip(atchelper::color::color_vec4_whitesmoke, "Enable/Disable Phonetic.");
    // Draw Communication Standard Options - which are drill down from Flight Types pick
    ImGui::SameLine();
    if (ImGui::InputTextWithHint("Callsign", "NG13", WinAtcHelper::layer_config.buff_callsign, sizeof(layer_config.buff_callsign), ImGuiInputTextFlags_CharsUppercase))
    {
      layer_config.callsign = WinAtcHelper::layer_config.buff_callsign;
      layer_config.callsign_phonetic = this->get_phonetic_or_string_by_user_preference(this->layer_config.b_useCallsignPhonetic, layer_config.callsign, false);
    }

    // Display Phonetic only if user does not want to ignore it
    if (!layer_config.callsign_phonetic.empty() && layer_config.b_useCallsignPhonetic)
    {
      ImGui::SameLine(0.0f, 5.0f);
      ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_yellow); // yellow

      ImGui::PushItemWidth(current_context.x - 40.0f); // 
      ImGui::TextWrapped( ("[" + layer_config.callsign_phonetic + "]").c_str() );
      ImGui::PopItemWidth();

      ImGui::PopStyleColor();
    }

    //ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
    //ImGui::TextUnformatted(layer_config.callsign_phonetic.c_str());
    //ImGui::PopTextWrapPos();

    // ---- ROW 4: Plane ICAO and manufacturer
    if (ImGui::Checkbox("##PlaneTypeNoPhonetico", &this->layer_config.b_usePlaneTypePhonetic))
    {
      layer_config.plane_type_phonetic = this->get_phonetic_or_string_by_user_preference (this->layer_config.b_usePlaneTypePhonetic, layer_config.plane_type, true);
    }
    this->add_tooltip(atchelper::color::color_vec4_whitesmoke, "Enable/Disable Phonetic.");


    // Draw Communication Standard Options - which are drill down from Flight Types pick
    ImGui::SameLine();
    if (ImGui::InputTextWithHint("Plane ICAO", "A310, C172", WinAtcHelper::layer_config.buff_plane_type, sizeof(layer_config.buff_plane_type), ImGuiInputTextFlags_CharsUppercase))
    {
      layer_config.plane_type = WinAtcHelper::layer_config.buff_plane_type;
      layer_config.plane_type_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_alphabet(layer_config.plane_type, true);
    }
    ImGui::SameLine(0.0f, 50.0f);
    ImGui::SetNextItemWidth(200.0f);
    if (ImGui::InputTextWithHint("Manufacturer", "Airbus, Cessna", WinAtcHelper::layer_config.buff_plane_manufacturer, sizeof(layer_config.buff_plane_manufacturer), ImGuiInputTextFlags_CharsUppercase))
    {
      layer_config.plane_manufacturer = WinAtcHelper::layer_config.buff_plane_manufacturer;
    }

    ////// Plane Type Filter helper
    ImGui::PushStyleColor(ImGuiCol_Header, atchelper::color::color_vec4_steelblue);
    ImGui::PushStyleColor(ImGuiCol_HeaderActive, atchelper::color::color_vec4_blue);
    ImGui::PushStyleColor(ImGuiCol_HeaderHovered, atchelper::color::color_vec4_blue);
    static bool b_autoCollapseHeader = false;
    if (b_autoCollapseHeader)
    {
      ImGui::SetNextItemOpen (false);
      b_autoCollapseHeader = false;
    }

    if (ImGui::CollapsingHeader("Plane Type Helper"))
    {
      static ImGuiTextFilter filter = this->layer_config.buff_plane_type; // (!this->layer_config.plane_type.empty()) ? "A310" : ImGuiTextFilter("");
      filter = this->layer_config.buff_plane_type;
      //HelpMarker( "Filter usage:\n"
      //  "  \"\"         display all lines\n"
      //  "  \"xxx\"      display lines containing \"xxx\"\n"
      //  "  \"xxx,yyy\"  display lines containing \"xxx\" or \"yyy\"\n"
      //  "  \"-xxx\"     hide lines containing \"xxx\"");

      ImGui::SameLine(); ImGui::TextColored(atchelper::color::color_vec4_yellow, "Filter usage: start typeing to display text contains...\n");

      static int selected = -1;   

      //filter.Draw("Filter (inc,-exc)");

      ImGui::TextColored(atchelper::color::color_vec4_yellow, "ICAO : Manufacturer :Airplanes");

      const auto currentFontScale = ImGui::GetCurrentContext()->CurrentWindow->FontWindowScale;
      ImGui::SetWindowFontScale(  (currentFontScale > 1.0f)? 1.0f: currentFontScale ) ;

      ImGui::PushItemWidth(-1);
      //ImGui::ListBoxHeader("##ICAO:Man:Airplanes", current_context);
      ImGui::ListBoxHeader("##ICAO:Man:Airplanes", (int)atc_data::vecDoc8643_char.size(), 4);
      for (size_t i = (size_t)0; i < atc_data::vecDoc8643_char.size(); i++)
        if (filter.PassFilter(atc_data::vecDoc8643_char[i]))
        {
          //sprintf(buf, "Object %d", );
          if (ImGui::Selectable(atc_data::vecDoc8643_char[i], selected == (int)i))
          {
            selected = (int)i;
            get_and_set_plane_info_based_on_selected_string(atc_data::vecDoc8643_char[i]);    
            b_autoCollapseHeader = true;
          }

         }
      ImGui::ListBoxFooter();
      ImGui::PopItemWidth();
      ImGui::SetWindowFontScale(currentFontScale);
    }
    ImGui::PopStyleColor(3);



    ImGui::NewLine();
    ImGui::Separator();

    // ---- ROW 5: Atis Code
    if (ImGui::Combo("Atis Code (if Any)", &WinAtcHelper::layer_config.user_picked_atis_code_i, atchelper::vecAtisCodes.data(), (int)atchelper::vecAtisCodes.size()))
    {
      layer_config.atis_code = atchelper::vecAtisCodes.at(WinAtcHelper::layer_config.user_picked_atis_code_i);
    }

    // ---- ROW 6: Ramp start number or stand
      if (ImGui::InputTextWithHint("Ramp number / Stand number", "", WinAtcHelper::layer_config.buff_ramp_start, sizeof(layer_config.buff_ramp_start), ImGuiInputTextFlags_CharsUppercase))
      {
        layer_config.ramp_start = WinAtcHelper::layer_config.buff_ramp_start;
        layer_config.ramp_start_translated = atchelper::atc_data::get_translated_string_to_phonetic_alphabet(layer_config.ramp_start, true);
      }
      // translation for ramp
      if (!layer_config.ramp_start_translated.empty())
      {
        ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_yellow); // yellow

        ImGui::SameLine(0.0f, 5.0f);
        ImGui::PushItemWidth(current_context.x * 0.5f); // 
        ImGui::TextWrapped(("[" + layer_config.ramp_start_translated + "]").c_str());
        ImGui::PopItemWidth();

        ImGui::PopStyleColor();
      }

      // ---- ROW 7: Departure and Destination
      ImGui::PushItemWidth(current_context.x * 0.33f); // 

      if (ImGui::InputTextWithHint("Departure", "", WinAtcHelper::layer_config.buff_dep_airport, sizeof(layer_config.buff_dep_airport), ImGuiInputTextFlags_CharsUppercase))
      {
        layer_config.dep_airport = WinAtcHelper::layer_config.buff_dep_airport;
      }
      ImGui::SameLine(0.0f, 5.0f);
      if (ImGui::InputTextWithHint("Destination", "", WinAtcHelper::layer_config.buff_destination_airport, sizeof(layer_config.buff_destination_airport), ImGuiInputTextFlags_CharsUppercase))
      {
        layer_config.dest_airport = WinAtcHelper::layer_config.buff_destination_airport;
      }
    ImGui::PopItemWidth();


    // ---- ROW 8: BUTTON - Load information using XPATH and display the ATC helper text
    ImGui::PushStyleColor(ImGuiCol_Button, atchelper::color::color_vec4_green); // 
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, atchelper::color::color_vec4_goldenrod); // 
    if (ImGui::Button("Show ATC Text", ImVec2(current_context.x * 0.50f, 30.0f)))
    {
      this->execAction(atchelper::mx_window_actions::ACTION_READ_ATC_AND_DISPLAY);
    }
    ImGui::PopStyleColor(2);

  }
  // ---- Last row: Internal use: reload XML file
  storedCursorPos = ImGui::GetCursorPos();


  if (storedCursorPos.y >= current_context.y)
    ImGui::SetCursorPosY(storedCursorPos.y + 20.0f);
  else 
    ImGui::SetCursorPosY(current_context.y - 10.0f);

  ImGui::Separator();
  // checkbox
  ImGui::Checkbox("Clear Options", &this->layer_config.b_clear_option_values);
  ImGui::SameLine();

  ImGui::SetCursorPosX(current_context.x - (current_context.x * 0.25f));

    if (ImGui::Button("Reload XML"/*, ImVec2(current_context.x * 0.25f, 30.0f)*/))
    {
      this->execAction(atchelper::mx_window_actions::ACTION_RELOAD_XML_TEMPLATE);
    }

  ImGui::EndGroup();

}

// -------------------------------------------------

void atchelper::WinAtcHelper::draw_atc_layer()
{
  ImVec2 const contentSize = ImGui::GetContentRegionAvail();
  //ImVec2 AlphabetImageSize_vec2(window_size_vec2.x - this->CHILD_BORDER_PADDING, 30.0f);
  const ImVec2 AlphabetChildArea_vec2(contentSize.x, 158.0f);

  if (WinAtcHelper::layer_main.flag_first_time)
  {
    // init user options
    WinAtcHelper::layer_main.item_option_current_i = 0;
    WinAtcHelper::layer_main.buff_option_value[0] = '\0';

    WinAtcHelper::layer_main.flag_first_time = false;
  }


  // Display the Helper TAB or Categories. The idea is to have more space for the "helper tab" and the Markdown
  if (this->layer_main.b_display_helper_tab)
  {

    ImGui::PushStyleColor(ImGuiCol_Button, atchelper::color::color_vec4_steelblue);
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, atchelper::color::color_vec4_steelblue);
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, atchelper::color::color_vec4_steelblue);

    ImGui::BeginChild("alphabet_helper", AlphabetChildArea_vec2);
    ImGui::BeginGroup();
    ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
    if (ImGui::BeginTabBar("HelpresTabs", tab_bar_flags))
    {
      if (ImGui::BeginTabItem("Alphabet"))
      {
        if (ImGui::ImageButton((void*)(intptr_t)atc_data::mapPluginTextures[atchelper::BITMAP_ALPHABET].gTexture, ImVec2(atc_data::mapPluginTextures[atchelper::BITMAP_ALPHABET].sImageData.getW_f(), atc_data::mapPluginTextures[atchelper::BITMAP_ALPHABET].sImageData.getH_f())))
        {
          this->layer_main.b_display_helper_tab = !this->layer_main.b_display_helper_tab;
        }
        ImGui::EndTabItem();
      }
      if (ImGui::BeginTabItem("Pattern"))
      {
        if (ImGui::ImageButton((void*)(intptr_t)atc_data::mapPluginTextures[atchelper::BITMAP_PATTERN].gTexture, layer_main.vec2Pattern))
        {
          layer_main.b_display_pattern_full = !layer_main.b_display_pattern_full;
        }
        ImGui::EndTabItem();
      }
      ImGui::EndTabBar();
    }

    ImGui::EndGroup();
    ImGui::EndChild();

    //ImGui::Separator();

    ImGui::PopStyleColor(3);

  }
  else 
  {
    //ImGui::BeginChild("categoriesAndSubs", ImVec2(contentSize.x, 25.0f));
    {
      ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_yellow); // yellow
      {
        ImGui::BeginGroup();
        ImGui::PushItemWidth(contentSize.x * 0.33f); // alwayz third of screen width
        if (ImGui::Combo("##Categories", &this->layer_main.item_category_current, this->layer_main.vecCategoryChoice_char.data(), (int)this->layer_main.vecCategoryChoice_char.size()))
        {
          assert(this->layer_main.vecCategoryChoice.size() >= (size_t)this->layer_main.item_category_current && "Category boundary is incorrect. Notify programmer.");

          this->layer_main.item_sub_category_current = 0;
          this->layer_main.initSubCategories(this->mapAtcCategories, this->layer_main.vecCategoryChoice[this->layer_main.item_category_current]);
        }

        ImGui::SameLine(0.0f, 5.0f);
        ImGui::PushItemWidth(contentSize.x * 0.33f); // alwayz thirs of screen width
        if (ImGui::Combo("##subCategories", &this->layer_main.item_sub_category_current, this->layer_main.vecSubCategoryChoice_char.data(), (int)this->layer_main.vecSubCategoryChoice_char.size()))
        {
          if (this->layer_main.vecSubCategoryText.size() > this->layer_main.item_sub_category_current)
            this->layer_main.markup_text_ptr = this->layer_main.vecSubCategoryText[this->layer_main.item_sub_category_current];
        }

        ImGui::SameLine(0.0f, 10.0f);
        ImGui::TextColored(atchelper::color::color_vec4_orangered, layer_config.flight_type.c_str() );

        ImGui::EndGroup();
      }
      ImGui::PopStyleColor(1);
    }
    //ImGui::EndChild();
  }


  // -------------------------
  //// Markdown with TEXT ////
  // -------------------------

  const auto currentFontScale = ImGui::GetCurrentContext()->CurrentWindow->FontWindowScale;
  ImVec2 markdownSize_vec2;
  if (layer_main.b_display_helper_tab)
    markdownSize_vec2 = ImVec2(contentSize.x, contentSize.y - 138.0f - 52.0f); // keep 52px free from bottom and remove the size of the helper tab
  else
    markdownSize_vec2 = ImVec2(contentSize.x, contentSize.y - 52.0f); // keep 52px free from bottom

  //ImGuiWindowFlags child_flags_02 = ImGuiWindowFlags_AlwaysVerticalScrollbar;
  //ImGui::BeginChild("draw_main_layer_02", markdownSize_vec2, true, child_flags_02);

  ImGui::BeginChild("draw_main_layer_02", markdownSize_vec2);
  {
    ImGui::PushStyleColor(ImGuiCol_Text, atchelper::color::color_vec4_white); // yellow
    ImGui::BeginGroup();

    atchelper::plugin_font_scale = currentFontScale;

#ifdef USE_IMGUI_MARKDOWN
    ImGui::Markdown(this->layer_main.markup_text_ptr.data(), this->layer_main.markup_text_ptr.size(), this->mdConfig);
#endif
    ImGui::EndGroup();
    ImGui::PopStyleColor(1);
  }
  ImGui::EndChild();
  ImGui::Separator();


  // -------------------------
  // lower toolbar - dynamic options
  // -------------------------
  if (!this->layer_main.vecCategoryOptions_chars.empty()) // if not empty
  {
    // Display Option COMBO
    ImGui::PushItemWidth(contentSize.x * 0.20f); // alwayz 1/5 of screen width
    if (ImGui::Combo("##categoryOptions", &this->layer_main.item_option_current_i, this->layer_main.vecCategoryOptions_chars.data(), (int)this->layer_main.vecCategoryOptions_chars.size()))
    {
      assert(this->layer_main.vecCategoryOptions.size() >= this->layer_main.item_option_current_i && "vecCategoryOptions might not be in syncg with vecCategoryOptions_chars.");

      if (this->layer_main.vecCategoryOptions.size() >= this->layer_main.item_option_current_i)
      {
        const std::string key = this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i);

        assert(Utils::isElementExists(this->layer_main.mapCategoryOptionsValues, key) && "layer_main.mapCategoryOptionsValues is not sync with vecCategoryOptions values ");

        // get phonetic
        layer_main.option_string_value_phonetic = this->layer_main.mapCategoryOptionsValues[key].option_val_phonetic_s;
        // store value into option value BUFF
        std::memcpy(this->layer_main.buff_option_value, this->layer_main.mapCategoryOptionsValues[key].option_val_s.c_str(), sizeof(this->layer_main.buff_option_value));
      }
    }

    // User Input Text Field value
    ImGui::SameLine(0.0f, 5.0f);
    if (ImGui::InputTextWithHint("##UserEnterOptionValue", "Some Text...", this->layer_main.buff_option_value, sizeof(this->layer_main.buff_option_value)))
    {
      const std::string key = this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i);

      this->layer_main.option_string_value = this->layer_main.buff_option_value; // store buffer in string
      if (this->layer_main.mapCategoryOptionsValues[key].getOptionType() == atchelper::enm_option_type::option_by_phonetic_elev)
        layer_main.option_string_value_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_elevation(this->layer_main.option_string_value);
      else
        layer_main.option_string_value_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_alphabet(this->layer_main.option_string_value, this->layer_main.b_translate_numbers_as_phonetic);
    }

    // trash button
    ImGui::SameLine();
    if (ImgWindow::ButtonTooltip(ICON_FA_TRASH_ALT"##ClearFromTextValue", "Clear"))
    {
      memcpy(this->layer_main.buff_option_value, "", sizeof(this->layer_main.buff_option_value));
      this->layer_main.option_string_value.clear();
      layer_main.option_string_value_phonetic.clear();
    }

    // Button Apply as is
    ImGui::SameLine(0.0f, 10.0f);
    if (ImGui::Button("Apply##ApplyCategoryOption"))
    {
      const std::string key = this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i);
      this->layer_main.mapCategoryOptionsValues[key].b_apply_as_value = true;
      this->layer_main.mapCategoryOptionsValues[key].b_apply_as_elv_phonetic = false;


      this->execAction(atchelper::mx_window_actions::ACTION_CALL_APPLY_USER_ATC_OPTION_FROM_MAIN_THREAD);
    }
    this->add_tooltip(atchelper::color::color_vec4_yellow, "Apply value.");

    // Button Apply as Phonetic
    ImGui::SameLine(0.0f, 5.0f);
    if (ImGui::Button("Ph##ApplyCategoryOption"))
    {
      // translate again to phonetic alphabet
      layer_main.option_string_value_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_alphabet(this->layer_main.option_string_value, this->layer_main.b_translate_numbers_as_phonetic);

      const std::string key = this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i);
      this->layer_main.mapCategoryOptionsValues[key].b_apply_as_value = false;
      this->layer_main.mapCategoryOptionsValues[key].b_apply_as_elv_phonetic = false;

      this->execAction(atchelper::mx_window_actions::ACTION_CALL_APPLY_USER_ATC_OPTION_FROM_MAIN_THREAD);
    }
    this->add_tooltip(atchelper::color::color_vec4_yellow, "Phonetic: Apply value as the phonetic value of each character.");

    // Elevation phonetic
    ImGui::SameLine(0.0f, 5.0f);
    if (ImGui::Button("Ph.Elev##ApplyCategoryOption"))
    {
      // translate to elevation phonetic
      layer_main.option_string_value_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_elevation(this->layer_main.option_string_value);

      // set the elev phonetic flags and apply to ATC Text
      const std::string key = this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i);
      this->layer_main.mapCategoryOptionsValues[key].b_apply_as_value = false;
      this->layer_main.mapCategoryOptionsValues[key].b_apply_as_elv_phonetic = true;

      this->execAction(atchelper::mx_window_actions::ACTION_CALL_APPLY_USER_ATC_OPTION_FROM_MAIN_THREAD);
    }
    this->add_tooltip(atchelper::color::color_vec4_yellow, "Elev Phonetic: Apply value as the Elevation phonetic value.");

    // Checkbox - disable enable phonetic numbers
    ImGui::SameLine();
    ImGui::PushID("##PhoneticNumberYesNo");
    if (ImGui::Checkbox("", &this->layer_main.b_translate_numbers_as_phonetic))
    {
      layer_main.option_string_value_phonetic = atchelper::atc_data::get_translated_string_to_phonetic_alphabet(this->layer_main.option_string_value, this->layer_main.b_translate_numbers_as_phonetic);
    }
    ImGui::PopID();
    this->add_tooltip(atchelper::color::color_vec4_whitesmoke, "Enable/Disable Phonetic Numbers.");

    // Informated text: Display the phonetic output
    ImGui::SameLine(); // (0.0f, 0.0f);
    ImGui::PushID("##displayPhoneticOptionValue");
    ImGui::TextUnformatted(std::string(layer_main.option_string_value_phonetic).c_str());
    ImGui::PopID();
  }
}

// -------------------------------------------------

void atchelper::WinAtcHelper::execAction(atchelper::mx_window_actions actionCommand)
{

  switch (actionCommand)
  {
  case atchelper::mx_window_actions::ACTION_HIDE_WINDOW:
  {
    if (this->GetVisible())
      this->toggleWindowState();

  }
  break;
  case atchelper::mx_window_actions::ACTION_TOGGLE_WINDOW:
  {

    if (!this->IsPoppedOut())
      this->toggleWindowState();

  }
  break;
  case atchelper::mx_window_actions::ACTION_READ_ATC_AND_DISPLAY:
  {
    if (layer_config.user_picked_flight_type_i == layer_config.NOT_FOUND_I)
    {
      this->layer_config.flight_type.clear();
      this->layer_config.comm_standard.clear();
    }
    else
    {
      layer_config.flight_type = WinAtcHelper::layer_config.flightTypesAndStandard_ui_strct.mapFlightTypes_seq_key_ui[layer_config.user_picked_flight_type_i];
      this->layer_config.comm_standard = this->layer_config.flightTypesAndStandard_ui_strct.vecCommStandards_char.at(layer_config.user_picked_comm_standard_i);
    }


    this->setLayer(atchelper::uiLayer_enum::atc_layer);
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_load_xpath_data); // upload new XML information to uiImGuiAtcWindow Window


  }
  break;
  case atchelper::mx_window_actions::ACTION_RELOAD_XML_TEMPLATE:
  {
    WinAtcHelper::layer_config.flag_first_time = true;
    this->reset_atc_window();
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_reload_xml_atc_template_file); // upload new XML information to uiImGuiAtcWindow Window
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_read_atc_config_data_from_loaded_xml_and_set_atc_conf_layer); // 

  }
  break;
  case atchelper::mx_window_actions::ACTION_CALL_APPLY_USER_ATC_OPTION_FROM_MAIN_THREAD:
  {
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::task_reapply_user_option_on_text_from_main_thread); // 
  }
  break;
  case atchelper::mx_window_actions::ACTION_APPLY_USER_ATC_OPTION:
  {
    assert(this->layer_main.vecCategoryOptions.size() >= this->layer_main.item_option_current_i && "[action]vecCategoryOptions and vecCategoryOptions_chars might not be in sync");
    assert(this->mapAtcCategories.size() > (size_t)0 && "mapAtcCategories in ATC is empty");

    // store data
    this->layer_main.mapCategoryOptionsValues[this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i)].option_val_s = this->layer_main.option_string_value;
    this->layer_main.mapCategoryOptionsValues[this->layer_main.vecCategoryOptions.at(this->layer_main.item_option_current_i)].option_val_phonetic_s = this->layer_main.option_string_value_phonetic;

    this->layer_main.initSubCategories(this->mapAtcCategories, this->layer_main.vecCategoryChoice[this->layer_main.item_category_current], false);

  }
  break;
  case atchelper::mx_window_actions::ACTION_SAVE_CONFIGURATION:
  {
    atchelper::atc_data::queFlcActions.emplace(atc_data::atc_flc_pre_command::save_atc_setup); // 
  }
  break;
  default:
    break;

  } // end switch actions

}
