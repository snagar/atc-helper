#ifndef ATC_H_
#define ATC_H_
#pragma once

#include "core/base_c_includes.h"
#include "core/base_xp_include.h"
#include "core/xx_atc_helper_const.hpp"
#include "core/atc_data.h"

#include "ui/WinAtcHelper.h"

namespace atchelper
{
  typedef std::shared_ptr<WinAtcHelper> ImgAtcWindowSPtrTy;

  class ATC
  {
  private:
    atc_data ad;

  public:
    ATC();
    ~ATC();

    void init();


    ///// Plugin Menu Information /////
    // Menu Commands/sub menus
    struct _atc_menu
    {
      int toggle_draw_state_menu;
    }; // holds menu/submenu ids

    static _atc_menu atc_menu;

    typedef enum _menuIdRefs : uint8_t
    {
      ATC_HELPER_MENU_ENTRY, // Menu seed under plug-in menu
      TOGGLE_ATC_HELPER_WINDOW
    } plugin_menuIdRefs;

    static XPLMMenuID atcMenuEntry; // plugin menu id








    /// <summary>
    /// Main Flight Callback function, it calls the rest of the "flc_xxx()" functions
    /// </summary>
    void flc();
    /// <summary>
    /// The function was built to handle action one by one. It won't progress actions if the state of the "current_executing" parameter is not idle.
    /// This is good for long running tasks that we want to run in serialisation and not parallel.
    /// </summary>
    void flc_serial_dispatcher();
    /// <summary>
    /// The function will execute all actions in its deque list and won't wait for long task to finish.
    /// For serialized execution use flc_serial_dispatcher
    /// </summary>
    void flc_TASKS();


    void read_config_file();
    void save_config_file();
    void toggleMainWindow();


    void stop_plugin();
    
    static ImgAtcWindowSPtrTy uiImGuiAtcWindow; // ImGUI: Main ATC Helper Window 2D window

  };

}

#endif // !ATC_H_


