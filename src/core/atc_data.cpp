#include "atc_data.h"

#include <array>
//#include "coordinate/NavAidInfo.hpp"

#include <cstdio>
#include <iostream>
#include <fstream>



#ifdef IBM
#include "../io/dirent.vs.h" // directory header
#else
#include "dirent.h" // directory header
#endif

//#include "../core/coordinate/UtilsGraph.hpp"

namespace atchelper
{
  // Dispatcher
  std::deque <atc_data::atc_dispatch_task> atchelper::atc_data::dqDispatcher;
  atc_data::atc_dispatch_task atc_data::current_executing;

  // VR Related
  //XPLMDataRef atchelper::atc_data::g_vr_dref;
  int atchelper::atc_data::flag_in_vr{ false };
  int atchelper::atc_data::flag_prev_in_vr{ false };

  // INI 
  std::map <std::string, std::string>  atchelper::atc_data::mapConfig = { {"maxWidth","800"}, {"maxHeight", "500" } };

  // Template XML
  std::string  atchelper::atc_data::load_xml_err_s;

  //// Queue
  std::queue <std::string> atchelper::atc_data::queThreadMessage; // holds messages from threads to display in briefer. use: "rw_data::queFlcActions.push(atchelper::mx_flc_pre_command::set_briefer_text_message);" but set the que before hand
  std::queue <atc_data::atc_flc_pre_command> atchelper::atc_data::queFlcActions;
  std::queue <atc_data::atc_flc_pre_command> atchelper::atc_data::postFlcActions; //v3.0.146 // we will place actions in this Queue if we want them to happen after flc() so xplane updates its state. Example pause after position plane

  // Draw GL
  bool atc_data::flag_can_draw;
  bool atc_data::flag_drawcallback_is_active;

  // Plane Position
  atc_data::xx_plane_pos_strct atc_data::plane_pos;
  bool atc_data::flag_plane_in_rw_area; // use this only when we try to evaluate plane touchdown/takeoff // use in rwm::flc()
  const std::string atc_data::PLUGIN_PATH = Utils::getPluginDirectoryWithSep(atchelper::PLUGIN_NAME, true);
  std::map <std::string, atchelper::mxTextureFile> atc_data::mapPluginTextures;

  std::map <int, atchelper::atc_category_strct> atc_data::mapAtcCategories;
  pugi::xml_document atc_data::xml_main_doc_template_ptr; // holds the inmemory DOM of the XML file

  //std::string atc_data::mapDoc8643_filepath_s = Utils::getPluginDirectoryWithSep(atchelper::PLUGIN_NAME, true) + atchelper::DOC8643_FILENAME;
  //std::filesystem::path atc_data::filepath_to_Doc8643 = mapDoc8643_filepath_s;
  std::filesystem::path atc_data::filepath_to_Doc8643 = (Utils::getPluginDirectoryWithSep(atchelper::PLUGIN_NAME, true) + atchelper::DOC8643_FILENAME);
  const std::vector <std::string> atc_data::vecDoc8643 = atc_data::get_doc8643(atc_data::filepath_to_Doc8643);
  std::vector <const char*> atc_data::vecDoc8643_char;
  int atc_data::vecDoc8643_size_i{ 0 };
}


// ---------------------------------------------------------
// ---------------------------------------------------------

atchelper::atc_data::atc_data()
{
  atchelper::atc_data::init();

  readPluginTextures();

}


void atchelper::atc_data::init() {
  //rw_data::plugin_path = mxUtils::getPluginDirectoryWithSep(atchelper::PLUGIN_NAME::);
  
  atc_data::dqDispatcher.clear();
  atc_data::current_executing = atc_dispatch_task::idle;
  atc_data::flag_can_draw = false;
  atc_data::flag_drawcallback_is_active = false;
  atc_data::flag_plane_in_rw_area = false;

  // initialize vecDoc8643_char
  for (const auto& c : atc_data::vecDoc8643)
    atc_data::vecDoc8643_char.emplace_back(c.c_str());

  atc_data::vecDoc8643_size_i = (int)atc_data::vecDoc8643_char.size();
}
// ---------------------------------------------------------

atchelper::atc_data::~atc_data()
{
}


// ---------------------------------------------------------

void atchelper::atc_data::read_config_file(std::string_view inDefaultConfigText)
{
  const std::string config_file_path = atc_data::PLUGIN_PATH + atchelper::CONFIG_FILENAME;

  std::ifstream infs;
  std::ofstream outfsConfFile;

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);


  // prepare reading config file: "atc-helper.ini" file
  infs.open(config_file_path.c_str(), std::ios::in); // can we read the file
  if (!infs.is_open())
  {
    Log::logMsg("Fail to open config file: " + config_file_path + ". Will try to create a default file.\n");

    outfsConfFile.open(config_file_path.c_str(), std::ios::out);
    if (outfsConfFile.is_open())
    {
      outfsConfFile << inDefaultConfigText << "\n";
      outfsConfFile.flush();
      Log::logMsg("Created default configuration file.\n");
    }
    else
      Log::logMsg("ERR: Failed creating config file.\n");

    outfsConfFile.close();

  }
  else
  {
    std::string line;
    while (getline(infs, line))
    {
      // skip if has comment string or is empty
      line = mxUtils::trim(line);
      if ((line.length() > 0 && (line.front() == '#' || line.front() == ';')) || line.empty())
        continue;

      std::vector<std::string> vecValues = Utils::split(line, '=');
      if (vecValues.size() == 2)
      {
        Utils::addElementToMap(atc_data::mapConfig, vecValues[0], vecValues[1]);
      }

    } // end loop over config file lines

  }

  if (infs.is_open())
    infs.close();

#ifndef RELEASE
  Log::logMsg("[config] There are: " + Utils::formatNumber<size_t>(atc_data::mapConfig.size()) + " in the configuration map.\n"); // debug
#endif // !RELEASE


}

// ---------------------------------------------------------


const std::map<std::string, std::vector<std::string>> atchelper::atc_data::get_flightType_and_commStandards_from_xml_template(pugi::xml_document& doc)
{
  std::map <std::string, std::vector <std::string>> mapFlightTypes_and_CommStandards_defined_in_xml_template; // key = IFR, value vector of sub node sibling names.
  std::vector <std::string> vecTags; // will hold all of the node names

  auto atc_node = doc.child("ATC");
  if (!atc_node.empty())
  {
    auto node_range_sub1 = atc_node.children(); // IFR / VFR 
    for (auto node = node_range_sub1.begin(); node != node_range_sub1.end(); ++node)
    {
      std::string key = node->name(); // name of ATC children, example: IFR/VFR

      auto node_range_sub2 = node->children(); // sub 2 childrens
      for (auto node_comm = node_range_sub2.begin(); node_comm != node_range_sub2.end(); ++node_comm)
      {
        vecTags.emplace_back(node_comm->name());
      }

      // decide if to store
      if (!vecTags.empty())
        mapFlightTypes_and_CommStandards_defined_in_xml_template[key] = vecTags;

      vecTags.clear();

    }
  }

  return mapFlightTypes_and_CommStandards_defined_in_xml_template;
}


// ---------------------------------------------------------


void atchelper::atc_data::xml_search_xpath_and_load_data_to_mapAtcCategories(const pugi::xml_document& doc, const std::string inPathQuery)
{
  
  if (atc_data::xml_main_doc_template_ptr.children().begin() != atc_data::xml_main_doc_template_ptr.children().end())
  {
    ///// Read XML using XPATH
    pugi::xpath_node xpath_node = doc.select_node( inPathQuery.data() );
    auto nodes = xpath_node.node().children();

    std::string prevKey{ "" };

    mapAtcCategories.clear();

    // we could have a recursive function but since we only read all text of the second level of sub nodes we could write a simple loop inside a loop
// <category node>
//   <internal task> => clearence, taxi, tower
    int category_node_seq_i = 0;
    for (auto nodeCategory : nodes)
    {
      if (nodeCategory.empty() || nodeCategory.name() == "")
        continue;

      atc_category_strct category; // category struct like 
      // intitialize category struct
      category.seq_category_i = category_node_seq_i; // store sequence, will use it also in the category map.
      category.category_name = nodeCategory.name(); // store name of category node

      //auto subTaskNodes = nodeCategory.children();
      for (auto taskNode : nodeCategory.children())
      {

        auto pcnodes = taskNode.children(); // loop over all PCDATA and CDATA nodes
        std::string nodeText_s{ "" };

        for (auto pcn : pcnodes)
        {
          std::string name = pcn.name();
          std::string text = Utils::ltrim_text_on_all_lines( pcn.text().get() ); // trim leading white space for each line
          if (name.empty() && !text.empty())
            nodeText_s.append(pcn.text().as_string());
        }

        // store the task information in the category struct
        category.category_tasks.mapName[category.category_tasks.seq] = taskNode.name(); // store task node name
        category.category_tasks.mapText[category.category_tasks.seq] = Utils::ltrim_text_on_all_lines(nodeText_s); // store the task cumulative text // ltrim leading spaces for all lines in text and not just the first line
        category.category_tasks.seq++; // increment internal task sequence. Start from 0..N-1

      } // end loop over all category sub tasks

      //// READ category options
      const auto lmbda_read_category_dynamic_options = [](pugi::xml_node nodeCategory) {
        auto options_attrib = nodeCategory.attribute("options");
        if (options_attrib)
        {
          return Utils::splitString_to_vec(options_attrib.value(), ',', true); // return vector
        }

        //std::vector <std::string> empty_vecOptions;
        return std::vector <std::string>();
      };

      category.vecDynamicOptions = lmbda_read_category_dynamic_options(nodeCategory); // add dynamic options to the category struct

      // Store the category with all subtasks data
      atc_data::mapAtcCategories[category_node_seq_i] = category;
      category_node_seq_i++; // increment category node
    } // end loop over all categories
  }

}

// ---------------------------------------------------------

std::string atchelper::atc_data::get_translated_string_to_phonetic_alphabet(std::string_view inVal, bool translateNumerics_b )
{
  //bool last_char_was_translated_b = false;
  std::string result;
  result.clear();

  for (auto c : inVal)
  {
    const std::string c_s_u{ (char)std::toupper(c) };

    if ((!translateNumerics_b && ((int)c > 47) && (int)c < 58)) // handle if char is number and we dont need to translate
      result += c;
    else if (mxUtils::isElementExists(atchelper::atc_phonetics, c_s_u) )
    {
      result += atchelper::atc_phonetics.at(c_s_u) + " ";
    }
    else
    {
      result += c;
    }
  }

  return result;
}

// ---------------------------------------------------------

std::string atchelper::atc_data::get_translated_string_to_phonetic_elevation(std::string inVal)
{
  std::string val = mxUtils::stringToUpper(inVal);  // val should only hold the raw number

  const bool b_isFL = (val.size() > (size_t)3) && val.find("FL") == 0;
  bool b_isNumber = mxUtils::is_number(inVal);

  std::string num_to_convert;

  if (b_isFL) // extract number after FL if valid then we will convert to phonetic
  {
    num_to_convert = val.substr(2);
    b_isNumber = mxUtils::is_number(num_to_convert);
    if (b_isNumber)
    {
      int convert_fl_to_thousend = Utils::stringToNumber<int>(num_to_convert) * 100; // normalize the FL value to correct elevation FL200 = FL 20000 feet
      val = Utils::formatNumber<int>(convert_fl_to_thousend);
      if (convert_fl_to_thousend > 100000)
        return inVal; // invalid elevation for GA or IFR
    }
    else
      return inVal; // not valid hence returning same value
  }

  if (b_isNumber)
  {
#ifndef RELEASE
    const long val_l = mxUtils::stringToNumber<long>(val); // val should only hold the raw number
#endif 
    if (b_isFL)
      return "Flight Level " + Utils::nameForNumber(mxUtils::stringToNumber<long>(val));
    else 
      return Utils::nameForNumber(mxUtils::stringToNumber<long>(val));
  }

  return inVal;
}

// ---------------------------------------------------------

const std::map<std::string, std::string> atchelper::atc_data::get_parsed_doc8643(std::filesystem::path& filepath)
{

  char buff[256];
  std::map <std::string, std::string> mapDoc8643;
  std::ifstream ifs(filepath, std::ios::binary | std::ios::ate);

  if (!ifs)
  {
#ifdef IBM
    strerror_s(buff, errno);
#endif
    Log::logMsg( atchelper::PLUGIN_NAME + ": Failed to open file: " + filepath.string() + ". Err: File is empty");
    return mapDoc8643; // throw std::runtime_error(filepath.string() + ": " + buff);
  }


  auto end = ifs.tellg();
  ifs.seekg(0, std::ios::beg);

  auto size = std::size_t(end - ifs.tellg());

  if (size == 0) // avoid undefined behavior 
  {
    Log::logMsg(atchelper::PLUGIN_NAME + ": Skipping File : " + filepath.string() + ". Err: File is empty.");
    return mapDoc8643;
  }

  std::vector<std::byte> buffer(size);

  if (!ifs.read((char*)buffer.data(), buffer.size()))
  {
#ifdef IBM
    strerror_s(buff, errno);
#endif
    Log::logMsg(atchelper::PLUGIN_NAME + ": Caching of file: " + filepath.string() + " failed. Err: " + buff);

    return mapDoc8643; // throw std::runtime_error(filepath.string() + ": " + buff + "\n");

  }

  // split vector in "=" symbol
  std::string key{ "" }, value{ "" };
  bool isKey = true;
  for (const auto& c : buffer)
  {
    if ((char)c == '\n')
    {
      (!isKey) ? mapDoc8643[key] = value : "";
      isKey = true;
      key.clear();
      value.clear();

      continue;
    }
    else if ((char)c == '=')
    {
      isKey = false;
      continue;
    }
    else if ((int)c > 31)
      (isKey) ? key.push_back((char)c) : value.push_back((char)c);
    else
      continue;

  }

  Log::logMsg(atchelper::PLUGIN_NAME + ": Airplain read: " + mxUtils::formatNumber<size_t>(mapDoc8643.size()));

  return mapDoc8643;
}

// ---------------------------------------------------------

const std::vector <std::string> atchelper::atc_data::get_doc8643(std::filesystem::path& filepath)
{

  if (std::filesystem::exists(filepath))
  {
    std::ifstream p{ filepath };
    if (!p)
    {
      Log::logMsg(atchelper::PLUGIN_NAME + ": Failed to read from file: " + filepath.string());      
    }
    else
    {
      std::vector <std::string> vecLines;
      std::string s;
      while (getline(p, s))
        vecLines.emplace_back(s);

      return vecLines;
    }
  }

  return std::vector <std::string>();
}

// ---------------------------------------------------------

void atchelper::atc_data::readPluginTextures()
{
  std::string bitmap_path = std::string(atc_data::PLUGIN_PATH) + "bitmap";
  std::array <std::string, 2> textureName_arr = { atchelper::BITMAP_ALPHABET, atchelper::BITMAP_PATTERN };

  for (const auto& f : textureName_arr)
  {
    mxTextureFile tFile;
    tFile.fileName = f;
    tFile.filePath = bitmap_path;

    atchelper::BitmapReader::loadGLTexture(tFile, false); // load image

    if (tFile.gTexture != 0)
    {
      Utils::addElementToMap(atc_data::mapPluginTextures, tFile.fileName, tFile);
      Log::logMsg("Loaded bitmap: " + tFile.getAbsoluteFileLocation() + "\n"); // debug
    }
  }

}

// ---------------------------------------------------------


void atchelper::atc_data::clearPluginTextures()
{
  for (auto img : atc_data::mapPluginTextures)
  {
    //    glDeleteTextures(1, (const GLuint*)&img.second.handle.id);
    glDeleteTextures(1, (const GLuint*)&img.second.gTexture); // 
  }
}

// ---------------------------------------------------------

void atchelper::atc_data::flc()
{

  atc_data::plane_pos.prevFaxil = atc_data::plane_pos.curFaxil;
  atc_data::plane_pos.curFaxil = atc_data::get_plane_faxil();
  float grounSpead_f = XPLMGetDataf(Utils::drefs.dref_groundspeed_f);

  atc_data::plane_pos.flag_first_time = false;

  //atchelper::mxvr::vr_is_enabled = XPLMGetDatai(g_vr_dref);
  //if (atchelper::mxvr::vr_is_enabled)
  //  atchelper::mxvr::vr_ratio = atchelper::mxvr::VR_RATIO; // 0.75f; // 0.52f;
  //else
  //  atchelper::mxvr::vr_ratio = atchelper::mxvr::DEFAULT_RATIO; //  1.0f;
  
}

// ---------------------------------------------------------

void atchelper::atc_data::stop_plugin()
{

  mapAtcCategories.clear();

  atc_data::current_executing = atc_data::atc_dispatch_task::idle;

  atc_data::clearPluginTextures();
  atc_data::clearAllQueues();


  //atc_data::mapIndexNavAids.clear();
  //atc_data::cachedNavInfo_map.clear();

  atc_data::plane_pos.reset();

}

void atchelper::atc_data::draw()
{


} // draw


float atchelper::atc_data::get_plane_faxil()
{
  return XPLMGetDataf(Utils::drefs.dref_faxil_gear_f);
}

void atchelper::atc_data::clearAllQueues()
{
  atc_data::dqDispatcher.clear();

  // clear pre flc action queu
  while (!atc_data::queFlcActions.empty())
    atc_data::queFlcActions.pop();

  while (!atc_data::queThreadMessage.empty())
    atc_data::queThreadMessage.pop();

}






// ---------------------------------------------------------

// ---------------------------------------------------------

// ---------------------------------------------------------

// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------

