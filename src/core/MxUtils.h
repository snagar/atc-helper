#ifndef MXUTILS_H_
#define MXUTILS_H_
#pragma once

#include <list>
#include "base_c_includes.h"
#include "XPLMUtilities.h"
#include "xx_atc_helper_const.hpp"

#ifdef APL
#include <cstdlib>
#endif

using namespace atchelper;
using namespace atchelper;
//using namespace std;

namespace atchelper
{

  class mxUtils
  {
  private:
    //const atchelper::atchelper_const ahc;
  public:
    mxUtils();
    ~mxUtils();

    

    // Members
    static std::string stringToUpper(std::string strToConvert);
    static std::string stringToLower(std::string strToConvert);
    static bool is_alpha(const std::string &str);
    static bool is_digits(const std::string &str);
    static bool is_number(const std::string& s);
    static std::string ltrim(std::string str, const std::string chars = std::string("\t\n\v\f\r "));
    static std::string rtrim(std::string str, const std::string chars = std::string("\t\n\v\f\r "));
    static std::string trim(std::string str, const std::string chars = std::string("\t\n\v\f\r "));
    /// <summary>
    /// 
    /// evaluate if a string holds a boolean value, example:
    /// "true,false,yes,no.y,n"
    /// </summary>
    /// <param name="inTestValue"> holds the string to test as boolean value</param>
    /// <param name="outStringResult_asBool">returns the string boolean evaluation: true or false, dependent on the string itself.
    ///   Will return false, if function is evaluated to false (be careful).
    ///   You have to be careful with the returned value, since there is a third option which is not supported by boolean: not true and not false. This happens when the string is not a boolean value and therefore not supported. 
    ///   You have to check the "function" returned value if the string represnts a bool value and only then use the returned value by "outStringResult_asBool".
    ///   The outcome will be "false" for strings who are not bool values, and that could be awkward. 
    /// </param>
    /// <returns>
    ///   returns true if the string represents a bool value
    ///   returns false if the string do not represents a bool value.   
    ///   The parameter "outStringResult_asBool" will hold the parssed boolean value. This is the real boolean returned value you need to check if the function returns "true".
    /// </returns>
    static bool isStringBool(std::string inTestValue, bool & outStringResult_asBool);
    // -------------------------------------------
    // v3.0.255.1
    static std::string remove_char_from_string(const std::string& inVal, const char inCharToRemove); // remove specific char from a given string. Return a new strin without this specific char
    // -------------------------------------------
    static std::vector<std::string> split(const std::string& s, char delimiter = ' '); // v3.0.219.12 from https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/

    static std::string translateMxPadLabelPositionToValid(std::string inLabelPosition);

    //static std::string translateMessageChannelTypeToString(mx_message_channel_type_enum  mType);
    //static mx_message_channel_type_enum translateMessageTypeToEnum (std::string & inType);

    static std::string emptyReplace(std::string inValue, std::string inReplaceWith);
    
    static float convert_skewed_bearing_to_degrees(const float inBearing); // convert the bearing number to valid degree between 0-359
    static float convert_skewed_bearing_to_degrees(const std::string inBearing_s); // convert the bearing number to valid degree between 0-359
    static std::string convert_skewed_bearing_to_degrees_return_as_string(const std::string inBearing_s); // convert the bearing number to valid degree between 0-359

    /* ********************************************** */

    //// Templates //////// Templates //////// Templates ////

    /* ********************************************** */

    template <class Container>
    static bool isElementExists(Container& inMap, const typename Container::key_type &key)
    {

      //assert(inMap != nullptr);

      //if (inMap != nullptr && inMap.find(value) != inMap.end())
      if (inMap.find(key) != inMap.end())
        return true;

      return false;
    }
    
    /* ********************************************** */
    /// v3.0.255.3 This template search and return the "key" value only if exists. If not then it returns "default value) // should have been defined many version back.
    // http://www.cppblog.com/mzty/archive/2005/12/14/1728.html
    template <class Container>
    static typename Container::mapped_type getValueFromElement(Container& inMap, typename Container::key_type key, typename Container::mapped_type default_value)
    {
      if (inMap.find(key) != inMap.end())
        return inMap[key];

      return default_value;
    }

    
    /* ********************************************** */

    template <class Container, typename T>
    static T getElementValue(Container& inMap, const typename Container::key_type key, T optional_value)
    {

      if (inMap.find(key) != inMap.end())
        return inMap[key]; // return value

      return optional_value;
    }
    


    /* ********************************************** */
    template < typename T >
    static bool isElementExistsInList(std::list<T>& inContainer, T &key) // v3.0.205.2
    {
      auto it = std::find(inContainer.begin(), inContainer.end(), key);
      if (it != inContainer.end())
        return true;

      return false;
    }


    /* ********************************************** */
    template < typename T >
    static bool isElementExistsInVec(const std::vector<T>& inContainer, const T &value) // v3.0.205.2
    {
      auto it = std::find(inContainer.begin(), inContainer.end(), value);
      if (it != inContainer.end())
        return true;

      return false;
    }


    /* ********************************************** */
    /* ********************************************** */
    
    template <typename T>
    static bool  isStringNumber(T& t, const std::string& s, std::ios_base& (*f)(std::ios_base&))
    {
      std::istringstream iss(s);

      return !(iss >> f >> t).fail();
    } 
    // isStringNumber

    /* ********************************************** */

    // Convert String into Number
    template <typename N>
    static N stringToNumber(std::string s)
    {
      std::istringstream is(s);
      N n;
      is >> n;

      return n;
    }
    /* ********************************************** */

    // Convert String into Number with precision
    template <typename N>
    static N stringToNumber(std::string s, int precision)
    {
      std::istringstream is(s);
      N n;
      std::string num; num.clear();
      for (int i = 0; i < precision; i++)
      {
        char c = '\0';
        is.get(c);
        num += c;
      }
      std::istringstream isNum(num);
      isNum >> n;

      //is >> std::setprecision(precision) >> std::setiosflags(ios::fixed) >> n;


      return n;
    }


    // -------------------------------------------
    template <typename N>
    static int getPrecision(N n)
    {
      int count = 0;

      if (std::is_floating_point<N>::value)
      {
#ifdef IBM
        n = std::abs(n); 
#else
        n = fabs(n);
#endif
        n = n - int(n); // remove left of decimal.
        while (n >= 0.0000001 && count < 7) // limit precision to 8
        {
          n = n * 10;
          count++;
          n = n - int(n); // remove left of decimal
        }
      } // if floating point


      return count;
    }

    // -------------------------------------------

    // Number To String Template - NO formating
    template <typename N>
    static std::string  formatNumber(N inVal)
    {
      std::ostringstream o;
      if (!(o << inVal))
      {
        //std::snprintf ( LOG_BUFF, LOG_BUFF_SIZE, " The number %f, could not be converted.", (N)inVal );
        //log("The number could not be converted.");
        return EMPTY_STRING;
      }

      return o.str();
    }

    // -------------------------------------------

    // Number to String Template - WITH precision formating
    template <typename N>
    static std::string  formatNumber(N inVal, int precision)
    {
      std::ostringstream o;
      if (!(o << std::setiosflags(std::ios::fixed) << std::setprecision(precision) << inVal))
      {
        std::snprintf(LOG_BUFF, sizeof(LOG_BUFF), " The number %f, could not be converted.", (float)inVal);
        XPLMDebugString(LOG_BUFF);
        return EMPTY_STRING;
      }


      return o.str();
    }

    // -------------------------------------------

    static atchelper::mx_btn_colors translateStringToButtonColor(std::string inColor);

    // -------------------------------------------
    static std::string getFreqFormated( const int freq );
      
    // -------------------------------------------

    // based on https://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string
    static std::string replaceAll(std::string str, const std::string& from, const std::string& to);

//#include <regex>
//    ...
//      std::string string("hello $name");
//    string = std::regex_replace(string, std::regex("\\$name"), "Somename");

    
    // -------------------------------------------
    template < typename T>
    static void eval_min_max(const T &inVal, T &outMin, T &outMax)
    {
      if (inVal > outMin )
      {
        if (inVal > outMax)
          outMax = inVal;
      }
      else
        outMin = inVal;
    }
    // -------------------------------------------
    // -------------------------------------------

  };

}

#endif // !MXUTILS_H_
