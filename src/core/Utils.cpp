﻿#include <functional>
#include "Utils.h"

using namespace atchelper;


namespace atchelper
{  
  atc_drefs Utils::drefs;

}

/**************/
// initialize map
//const std::map<std::string, int> Utils::mapCloudTypes = { { atchelper::CLOUD_TYPE1,0 },{ atchelper::CLOUD_TYPE2, 1 }, { atchelper::CLOUD_TYPE3, 2}, { atchelper::CLOUD_TYPE4, 3}, { atchelper::CLOUD_TYPE5, 4}, { atchelper::CLOUD_TYPE6, 5},{ atchelper::CLOUD_TYPE7, 6 } };

atchelper::Utils::Utils()
{
  

}

atchelper::Utils::~Utils()
{
}

// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------

std::string atchelper::Utils::ltrim_text_on_all_lines(const std::string inText)
{
  int lineIndex = 0;

  std::string s; 
  s.clear();

  // loop over all characters
  bool b_found_none_space_char = false;
  for (auto c : inText)
  {
    if (c == ' ' && !b_found_none_space_char)
    {
      continue;
    }
    else if (c == '\n') // new line
    {
      if (lineIndex == 0 && !b_found_none_space_char) // skip all leading \n from first row until we reach the first line with characters.
        continue;

      ++lineIndex;
      b_found_none_space_char = false; // reset bool on new line
      s += c;
      continue;
    }
    else if (c == '.' && !b_found_none_space_char) // represent the begining of logical line
    {
      // first dot (".") in line represent "read from here all chars until "\n", include leading spaces.
      b_found_none_space_char = true;

      continue;
    }

    s += c;
    b_found_none_space_char = true;
  }

  return s;
}

// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------
int atchelper::Utils::getPrecisionFromString(std::string &inValue)
{
  int precision = 0;
  size_t dotLocation;
  std::stoi(inValue, &dotLocation);
  if (dotLocation)
    precision = (int)std::string(inValue).substr(dotLocation).size();

  return precision;
}

// ---------------------------------------------------------
// ---------------------------------------------------------

std::vector<std::string> atchelper::Utils::splitStringToLines(std::string source, size_t width)
{
  int sourceLen = (int)source.length();
  int strLastPos = 0;
  int strLeftLength = sourceLen;
  int lastNewLinePos = 0; // source.npos;
  int widthToCut = 0;
  int spacePos = 0;
  int debugCounter = 0;

  std::string cutString = atchelper::EMPTY_STRING; // v3.0.148
  std::vector<std::string> vecSentences; // v3.0.148
  std::vector<std::string> vecReturnSentences; // v3.0.150
  vecSentences.clear();
  vecReturnSentences.clear();


  Log::logDebugBO(source);


  try
  {
    // Loop as long the remain string is longer than "width" restriction
    while (strLeftLength > 1 && debugCounter < 50)
    {
      debugCounter++;
      // find first "\n" (UNIX_EOL) in this widget for new line
      lastNewLinePos = (int)source.find(atchelper::UNIX_EOL, ((lastNewLinePos == (int)source.npos) ? 0 : strLastPos + 1));


      if (lastNewLinePos != (int)std::string::npos && ((lastNewLinePos - strLastPos) <= (int)width)) // if found ';' before "width"
      {
        widthToCut = lastNewLinePos - strLastPos;
        cutString = ltrim(source.substr(strLastPos, widthToCut));
        //target += cutString;
        vecSentences.push_back(cutString); // v3.0.148 add line

        strLeftLength -= widthToCut + 1; // decrease left length
        strLastPos += widthToCut + 1;
       
      }
 
      else if ((lastNewLinePos != (int)std::string::npos && lastNewLinePos > (int)width)
        || lastNewLinePos == (int)source.npos)
      {
        // make sure lastNewLinePos > -1 ( Avoid Out of range error )
        lastNewLinePos = (lastNewLinePos == (int)std::string::npos) ? 0 : lastNewLinePos;

        widthToCut = (int)width;
        if (source.at(lastNewLinePos) != ' ')
        {
          // calculate new lastNewLinePos to point to SPACE if possible
          spacePos = (int)source.find_last_of(' ', (strLastPos + width));

          if (spacePos != (int)source.npos && spacePos > strLastPos)
            widthToCut = spacePos - strLastPos; // change the width to cut
        }

        cutString = ltrim(source.substr(strLastPos, widthToCut));
        vecSentences.push_back(cutString); // v3.0.148 add line
        //target += cutString;

        strLastPos += widthToCut;
        strLeftLength -= widthToCut;

      } // if lastNLPos > width


    } // end while


  }
  catch (out_of_range &oor) {
    std::string what = oor.what();    
    //Utils::logMsg(what);
    
  }

  //     add last characters to Target string
  if (strLeftLength > 0)
  {
    cutString = source.substr(strLastPos, strLeftLength);
    vecSentences.push_back(cutString); // v3.0.148 add line
    //target += cutString;
  }
    

  // remove special characters from sentences. Replace with space
  for (auto s : vecSentences)
  {
    std::string outResult = s;
    replaceCharsWithString(outResult, "\r\t\n", atchelper::SPACE);
    vecReturnSentences.push_back(outResult);
  }


  return vecReturnSentences;
}

// ---------------------------------------------------------


/**
replaceChar1WithChar2
Replaces a character with another character or string of characters
Uses: remove the LF and UNIX_EOL from strings
*/
std::string atchelper::Utils::replaceChar1WithChar2(std::string inString, char charToReplace, std::string newChar)
{
  std::string outputStr;
  size_t lenString = 0;
  outputStr = inString;
  lenString = outputStr.length();
  bool no_more_to_search = false;

  size_t loc = 0;

  for (size_t i = 0; i < lenString && !no_more_to_search; i++)
  {
    loc = outputStr.find(charToReplace, i);
    if (loc != outputStr.npos && loc <= lenString)
      outputStr.replace(loc, 1, newChar);
    else
      no_more_to_search = true; // skip loop if nothing was found

  }

  return outputStr;
}

// ---------------------------------------------------------

std::string atchelper::Utils::replaceChar1WithChar2_v2(std::string inString, char charToReplace, std::string newChar)
{
  std::string result; result.clear();

  int length = (int)inString.length();

  for (auto &c : inString)
  {
    if (c == charToReplace)
    {
      if (newChar.empty())
        continue;

      result += newChar;
    }
    else
      result.push_back(c);
  }


  return result;
}

// replaceChar1WithChar2_v2

  // ---------------------------------------------------------
  /**
  replaceCharsWithString
  Replaces set of characters, one by one, with another string of characters
  Uses: remove the LF and UNIX_EOL from strings
  */
void atchelper::Utils::replaceCharsWithString(std::string &outString, std::string charsToReplace, std::string newChar)
{
  std::string result = outString;

  //outputStr = inString;
//  size_t lenString = 0;
//  lenString = outString.length();
  bool no_more_to_search = false;

  size_t loc = 0;
  for (auto c = charsToReplace.begin(); c != charsToReplace.end(); c++)
  {
    no_more_to_search = false;
    for (size_t i = 0; i < result.length() && !no_more_to_search; i++)
    {
      loc = result.find((*c), i);
      if (loc != result.npos)
        result.replace(loc, 1, newChar);
      else
        no_more_to_search = true; // skip loop if nothing was found

    } // end internal loop
  } // end external loop

  outString = result;

}

// ---------------------------------------------------------

std::string atchelper::Utils::replaceStringWithOtherString(std::string inStringToModify, std::string inStringToReplace, std::string inNewString, bool flag_forAllOccurances)
  {
    auto pos = inStringToModify.find(inStringToReplace);
    while ( pos != std::string::npos )
    {
      inStringToModify.replace(pos, inStringToReplace.length(), inNewString);
      
      if (flag_forAllOccurances)
      {
#ifndef RELEASE
        const auto offset = pos + inNewString.length(); // debug
        pos = inStringToModify.find(inStringToReplace, offset);
#else 
        pos = inStringToModify.find(inStringToReplace, pos + inNewString.length());
#endif // !RELEASE
      }
      else
      {
        pos = std::string::npos;
        break;
      }
    }

    return inStringToModify;
  }

// ---------------------------------------------------------

std::vector<std::string> atchelper::Utils::buildSentenceFromWords(std::vector<std::string> inTokens, size_t allowedSentenceLength, size_t maxLinesAllowed/*, size_t *outRealLinesCount*/)
{

  std::vector<std::string> vecSentence;
  std::vector<std::string>::iterator itToken;
  std::string sentence = "";
  size_t lineLen = 0;
  size_t lineCounter = 0;
  size_t realLineCount = 0;

  for (itToken = inTokens.begin(); itToken < inTokens.end() && lineCounter <= maxLinesAllowed; itToken++)
  {
    // check if new Token.length() + lineLen < sentenceLength ? sentance+=Token : start new line
    if (itToken->length() + lineLen <= allowedSentenceLength)
    {
      sentence = (sentence.length() == 0) ? *itToken : sentence.append(" ").append(*itToken);
      lineLen = sentence.length();
    }
    else 
    {

      // add sentence to vector
      vecSentence.push_back(sentence);
      sentence.clear();
      lineCounter++;
      realLineCount++;
      lineLen = 0;

      sentence = *itToken;
      lineLen = sentence.length();

    } // end sentence length check
  } // end loop

  // handle remainig string
  if (sentence.length() > 0 && sentence.length() <= allowedSentenceLength && lineCounter < maxLinesAllowed)
  {
    vecSentence.push_back(sentence);
    realLineCount++;
  }

  return vecSentence;

}

// ---------------------------------------------------------

std::vector<std::string> atchelper::Utils::splitString_to_vec(const std::string inString, char delimeter, bool b_remove_spaces, bool b_skipEmptyString)
{
  std::vector <std::string> tokens;
  std::string token{ "" };
  for (auto& c : inString)
  {
    if (c == delimeter)
    {
      if (token.empty() && b_skipEmptyString)
        continue;
      else
      {
        tokens.emplace_back(token);
        token.clear();
      }
    }
    else if (c == ' ' && b_remove_spaces)
      continue;
    else
    {
      token.push_back(c);
    }

  } // end loop over all characters
  // add the last token since it does not have delimeter after it
  if (!token.empty())
    tokens.emplace_back(token);

  return tokens;

}

// ---------------------------------------------------------

std::map<std::string, std::string> atchelper::Utils::splitString_to_map(const std::string inString, char delimeter, bool b_remove_spaces, bool b_skipEmptyString)
{
  std::map<std::string, std::string> mapTokens;
  const std::vector <std::string> vecTokens = Utils::splitString_to_vec(inString, delimeter, b_remove_spaces, b_skipEmptyString);

  for (const auto& t : vecTokens)
  {
    mapTokens[t] = t; 
  }

  return mapTokens; // the map token will be sorted by string but key nad value are the same
}

// ---------------------------------------------------------

std::vector<std::string> atchelper::Utils::splitString(std::string inString, std::string delimateChars)
{
  std::vector<std::string> vecSplitValues;
  vecSplitValues.clear();

  Tokenizer tkn(inString);
  std::string delimeter = std::string(delimateChars);
  tkn.setDelimiter(delimeter);

  return tkn.split();;

}

// ---------------------------------------------------------

std::map<int, std::string> atchelper::Utils::splitStringToMap(std::string inString, std::string delimateChars)
{
  int seq = 0;
  std::map<int, std::string> mapStrings;
  mapStrings.clear();

  std::vector<std::string> vecSplitValues = Utils::splitString(inString, delimateChars);
  for (auto val : vecSplitValues)
  {
    Utils::addElementToMap(mapStrings, seq, val);
    ++seq;
  }


  return mapStrings;
}

// ---------------------------------------------------------
std::list<std::string> atchelper::Utils::splitStringUsingStringDelimiter(std::string & inString, std::string delimateChars)
{

  Tokenizer tkn(inString);
  std::string delimeter = std::string(delimateChars);
  tkn.setDelimiter(delimeter);

  return tkn.splitByStringDelimeter();
}


// ---------------------------------------------------------
// Extract base string from a string. Good to extract file name without the extension "[file].exe", for example.
std::string atchelper::Utils::extractBaseFromString(std::string inFullFileName, std::string delimiter, std::string *outRestOfString)
{
  std::string result = "";
  size_t lengthString = inFullFileName.length();

  if (delimiter.empty() || lengthString == 1)
    return inFullFileName;

  char d = delimiter.front();
  size_t loc = inFullFileName.find_first_of(d);

  if (loc == string::npos) // did not find
  {
    if (outRestOfString != nullptr)
      outRestOfString->clear();

    return inFullFileName;
  }
  
  result = inFullFileName.substr(0, loc);
  if (outRestOfString != nullptr)
    (*outRestOfString) = inFullFileName.substr((loc + 1));



  return result;
}

// ---------------------------------------------------------
// Extract Last string from a string. Good to extract file name without the PATH, for example.
std::string atchelper::Utils::extractLastFromString(std::string inFullFileName, std::string delimiter, std::string *outRestOfString)
{
  std::string result; result.clear();
  size_t lengthString = inFullFileName.length();

  if (delimiter.empty() || lengthString == 1)
    return inFullFileName;

  char d = delimiter.front();
  size_t loc = inFullFileName.find_last_of(d);

  if (loc == string::npos) // did not find
  {
    if (outRestOfString != nullptr)
      outRestOfString->clear();

    return inFullFileName;
  }

  
  result = inFullFileName.substr(loc+1);
  if (outRestOfString != nullptr)
    (*outRestOfString) = inFullFileName.substr(0, loc);



  return result;
}



std::string atchelper::Utils::add_word_to_line(std::deque<std::string>& outList, std::string inCurrentLine_s, std::string inNewWord_s, int inMaxLineLength_i, bool flag_force_new_line)
{
  std::string newSentence = inCurrentLine_s;
  std::string newWord = inNewWord_s;
  auto lineLength = newSentence.length();
  size_t max_line_length = (size_t)inMaxLineLength_i;

  if (!newWord.empty() || flag_force_new_line)
  {
    // check length is valid
    size_t tmp_len = lineLength + newWord.length();

    if (tmp_len <= max_line_length)
    {
      newSentence += (newSentence.empty()) ? newWord : std::string(" ") + newWord;

      if (flag_force_new_line)
      {
        outList.push_back(newSentence); // add current good sentence length
        newSentence.clear();
      }

    } // end if sentence is in good length
    else if (tmp_len > max_line_length)
    {
      if (!newSentence.empty())
      {
        outList.push_back(newSentence); // add current good sentence length
        newSentence.clear();
      }


      tmp_len =newWord.length(); // check if new word is also too long
      auto devide_i = (size_t)(tmp_len / max_line_length);
      auto mod_i = (size_t)(tmp_len % max_line_length);

      for (size_t i1 = 0; i1 < devide_i; ++i1) // add parts of the work as a sentence
      {
        size_t from_st = i1 * max_line_length;
        newSentence = newWord.substr(from_st, max_line_length);
        outList.push_back(newSentence);
        newSentence.clear();
      }

      // std::cout << "mod: " << devide_i * max_line_length << std::endl;
      if (mod_i > 0)
      {
        newSentence = newWord.substr(devide_i * max_line_length); // start new sentence.
      }

    } // end if temp_len is too long or at correct length

    newWord.clear();
  } // end add newWord 

  return newSentence;
}



std::vector<std::string> atchelper::Utils::sentenceTokenizerWithBoundaries(std::string inString, std::string delimiterChar, size_t width, std::string in_special_LF_char)
{
  std::deque<std::string> dequeSentences;

  // if width = 0 then immediate return vecWord. For backword compatibility with old code, if any.
  if (width == 0)
  {
    std::vector<std::string> vecWords;
    Tokenizer str(inString);
    str.setDelimiter(delimiterChar);
    vecWords = str.split();
    return vecWords; // no need to create sentence
  }

  // main function to build the sentences, using deque instead vector, mainly for memory management benefits
  Utils::sentenceTokenizerWithBoundaries(dequeSentences, inString, delimiterChar, width, in_special_LF_char);
    
  std::vector<std::string> vecSentence({ dequeSentences.begin(), dequeSentences.end() }); // converts deque to vector
 
  return vecSentence;
}


void atchelper::Utils::sentenceTokenizerWithBoundaries(std::deque<std::string> &outDeque, std::string &inString, std::string delimiterChar, size_t width, std::string in_special_LF_char)
{

  char delimiter = (delimiterChar.empty()) ? ' ' : delimiterChar.front();

  std::string xWord = inString;


  std::string inSpecialChar = in_special_LF_char;
  auto lineLength = (size_t)0;
  auto max_line_length = width;

  std::string eol_found;

  std::string newSentence;
  std::string newWord;
  size_t charPos_i = -1;
  size_t skip_chars_i = 0;

  bool flag_skip = false;

  charPos_i = -1;
  auto pos_winEol = xWord.find(atchelper::WIN_EOL.c_str());
  auto pos_unixEol = xWord.find(atchelper::UNIX_EOL.c_str());
  auto pos_specialEol = xWord.find(inSpecialChar);
  auto lenWord_i = xWord.length();
  eol_found.clear();


  for (auto chr : xWord)
  {
    ++charPos_i; // current char position

    // check if we need to skip chars, in case of: "\n" or "\r\n"
    if (!flag_skip)
    {
      // handle Win EOL "\r\n"
      if (charPos_i == pos_winEol)
      {
        flag_skip = true;
        eol_found = atchelper::WIN_EOL;
        skip_chars_i = eol_found.length(); // We just need one more character to skip. calculate how many chars to skip

        pos_winEol = xWord.find(atchelper::WIN_EOL.c_str(), charPos_i + skip_chars_i);
        pos_unixEol = xWord.find(atchelper::UNIX_EOL.c_str(), charPos_i + skip_chars_i);

      }
      else
        // handle Unix EOL "\r"
        if (charPos_i == pos_unixEol)
        {
          eol_found = atchelper::UNIX_EOL;
          skip_chars_i = eol_found.length(); // - 1; // calculate how many chars to skip
          flag_skip = true;

          pos_winEol = xWord.find(atchelper::WIN_EOL.c_str(), charPos_i + skip_chars_i);
          pos_unixEol = xWord.find(atchelper::UNIX_EOL.c_str(), charPos_i + skip_chars_i);

        }
        else
          // handle Special EOL ";"
          if (charPos_i == pos_specialEol)
          {
            eol_found = inSpecialChar;
            skip_chars_i = eol_found.length(); // calculate how many chars to skip
            flag_skip = true;

            pos_specialEol = xWord.find(inSpecialChar, charPos_i + skip_chars_i);
          }

    }


    // check if we need to skip chars, in case of: "\n" or "\r\n"
    if (flag_skip)
    {
      --skip_chars_i;

      if (skip_chars_i <= 0)
      {
        flag_skip = false; // reset skip flag

        // add newWord to sentence and add sentence to vector. reset newWord and sentence
        newSentence = Utils::add_word_to_line(outDeque, newSentence, newWord, (int)max_line_length, true);

        newWord.clear();
        //newSentence.clear(); // bug: we should not delete the "sentence" that was returned from Utils::add_word_to_line(). Reason, long lines fail to display the "rest" of the line that was left out after substringing the line.
        lineLength = newSentence.length();

      }

    }
    else //if (!flag_skip /*&& char !='\r' && char != '\n'*/)
    {
      if (chr == delimiter)
      {
        newSentence = add_word_to_line(outDeque, newSentence, newWord, (int)max_line_length);
        newWord.clear();
        lineLength = newSentence.length();
      }
      else
        newWord += chr; // add characters to string

    }


  } // end word chars loop

  if (!newWord.empty())
    newSentence = Utils::add_word_to_line(outDeque, newSentence, newWord, (int)max_line_length);


  newWord.clear();
  lineLength = (int)newSentence.length();

  // Add last word if has value in it
  if (!newSentence.empty())
    outDeque.push_back(newSentence);


}

  // ---------------------------------------------------------

std::vector<std::string> atchelper::Utils::tokenizer(std::string inString, char delimateChar, size_t width)
{
  size_t sourceLen = 0;
  size_t strLastPos = 0;
  size_t lastNewLinePos = inString.npos;
  size_t strLeftLength = sourceLen;
  size_t widthToCut = 0;
  size_t spacePos = 0;
  int debugCounter = 0;
  std::string SPACE = " ";

  sourceLen = inString.length();
  //char chr = '\0';
  //int loc = 0;
  std::vector<std::string> vecWord;

  vecWord.clear();

  // *************
  while (strLastPos < sourceLen && debugCounter < 500)
  {
    debugCounter++;
    // find first ";" (UNIX_EOL) in this widget for new line
    lastNewLinePos = inString.find(delimateChar, ((lastNewLinePos == inString.npos) ? 0 : strLastPos + 1));

    if (lastNewLinePos != inString.npos && ((lastNewLinePos - strLastPos) <= width)) // if found ' ' before "width"
    {
      widthToCut = lastNewLinePos - strLastPos;
      vecWord.push_back(ltrim(inString.substr(strLastPos, widthToCut)));

      strLeftLength -= widthToCut; // decrease left length
      strLastPos += widthToCut;

      // if next "delimateChar" is greater then "width", add ' '
    }
    else if ((lastNewLinePos != inString.npos && lastNewLinePos > width)
      || lastNewLinePos == inString.npos)
    {
      // make sure lastNewLinePos > -1 ( Avoid Out of range error )
      lastNewLinePos = (lastNewLinePos == inString.npos) ? 0 : lastNewLinePos;

      widthToCut = width;
      if (inString.at(lastNewLinePos) != ' ')
      {
        // calculate new lastNewLinePos to point to SPACE if possible
        spacePos = inString.find_last_of(' ', (strLastPos + width));

        if (spacePos != inString.npos && spacePos > strLastPos)
          widthToCut = spacePos - strLastPos; // change the width to cut
      }

      vecWord.push_back(ltrim(inString.substr(strLastPos, widthToCut)));

      strLastPos += widthToCut;
      strLeftLength -= widthToCut;

    } // if lastNLPos > width


  } // end while


  return vecWord;

} // end tokenizer



// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------

void atchelper::Utils::CalcWinCoords(int inWinWidth, int inWinHeight, int inWinPad, int inColPad, int& left, int& top, int& right, int& bottom)
{
  // Screen coordinates
  int screenLeft, screenTop;
  XPLMGetScreenBoundsGlobal(&screenLeft, &screenTop, nullptr, nullptr);

  // Coordinates of our window
  left = screenLeft + inWinPad;
  right = left + inWinWidth;
  top = screenTop - inWinPad;
  bottom = top - inWinHeight;
}

void atchelper::Utils::getWinCoords(int& left, int& top, int& right, int& bottom)
{
  left = top = right = bottom = 0;
  XPLMGetScreenBoundsGlobal(&left, &top, &right, &bottom);
}

// ---------------------------------------------------------

std::string Utils::xml_readAttrib(pugi::xml_node& node, std::string attribName, std::string defaultValue)
{

  if (node.empty())
    return defaultValue;

  pugi::xml_attribute attr = node.attribute (attribName.c_str());
  if (attr.empty())
    return defaultValue;

  return attr.as_string();
}

// ---------------------------------------------------------

std::string Utils::nameForNumber(long number)
{
  if (number < 10) {
    return atchelper::ones[number];
  }
  else if (number < 20) {
    return atchelper::teens[number - 10];
  }
  else if (number < 100) {
    return atchelper::tens[number / 10] + ((number % 10 != 0) ? " " + nameForNumber(number % 10) : "");
  }
  else if (number < 1000) {
    return nameForNumber(number / 100) + " hundred" + ((number % 100 != 0) ? " " + nameForNumber(number % 100) : "");
  }
  else if (number < 1000000) {
    return nameForNumber(number / 1000) + " thousand" + ((number % 1000 != 0) ? " " + nameForNumber(number % 1000) : "");
  }
  else if (number < 1000000000) {
    return nameForNumber(number / 1000000) + " million" + ((number % 1000000 != 0) ? " " + nameForNumber(number % 1000000) : "");
  }
  else if (number < 1000000000000) {
    return nameForNumber(number / 1000000000) + " billion" + ((number % 1000000000 != 0) ? " " + nameForNumber(number % 1000000000) : "");
  }
  return "";
}

// ---------------------------------------------------------
// ---------------------------------------------------------
