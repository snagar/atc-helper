#ifndef BASETHREADH_
#define BASETHREADH_

#pragma once

/******
The class represents thread, and control information on it


**/

#include <iostream> 
#include <map>      
#include <thread>   
#include <chrono> 
#include <ctime>  

#include "../MxUtils.h"

namespace atchelper
{

class base_thread
{
public:
  base_thread() {};
  ~base_thread() {};

  typedef struct _thread_state
  {
    // state
    std::atomic<bool> is_active;// = false; // does thread() running/active
    std::atomic<bool> abort_thread; // = false; // does the plugin exit, thus aborting thread actions
    std::atomic<bool> thread_done_work; // = false; // true/false. Set to true only when Class::run_thread job finish execution
    std::atomic<mx_random_thread_wait_state_enum> thread_wait_state;

    // Chrono
    std::chrono::time_point<std::chrono::steady_clock> timer;
    std::string duration_s;

    std::string dataString;

    void init()
    {
      is_active = false;
      abort_thread = false;
      thread_done_work = false;

      startThreadStopper();
      dataString.clear();
    }

    void startThreadStopper()
    {
      timer = std::chrono::steady_clock::now();
    }

    std::string getDuration()
    {
      std::chrono::time_point<std::chrono::steady_clock> end = std::chrono::steady_clock::now();      
      double duration = (double)std::chrono::duration_cast<std::chrono::seconds>(end-timer).count();

      duration_s = mxUtils::formatNumber<double>((duration));
      return duration_s;
    }

  } thread_state;
 
  std::thread thread_ref; // will hold pointer to the current running thread

};

}
#endif // BASETHREADH_
