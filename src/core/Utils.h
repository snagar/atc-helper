#ifndef UTILS_H_
#define UTILS_H_
#pragma once

/**************


**************/
#include <random> // 
#include <unordered_set>
#include <deque>
#include <set>
#include <assert.h>

#include "XPLMScenery.h"
#include "xx_atc_helper_const.hpp"
#include "tokenizer/Tokenizer.h" // 

#include "../io/Log.hpp"
#include "MxUtils.h"

#include <pugixml.hpp>


using namespace std;
using namespace atchelper;

#ifdef __cplusplus
extern "C" {
#endif

//#define UNIX_EOL 13            /* Decimal code of Carriage Return char */
//#define LF 10            /* Decimal code of Line Feed char */
//#define EOF_MARKER 26    /* Decimal code of DOS end-of-file marker */
//#define MAX_REC_LEN 2048 /* Maximum size of input buffer */
// The shortest variant: Live On Coliru  //std::string str(std::istreambuf_iterator<char>{ifs}, {});
#ifdef __cplusplus
}
#endif

namespace atchelper
{

class Utils : public mxUtils
{
public:
  Utils();
  virtual
  ~Utils();
  static atc_drefs drefs;

  using mxUtils::formatNumber;
  using mxUtils::getPrecision;
  using mxUtils::isElementExists;
  using mxUtils::stringToNumber;

 
  
 // TEMPLATES

  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  template <class Container >
  static bool getElementAsDuplicateFromMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type &outMappedType)
  {
    bool found = false;
    typename Container::mapped_type dummy;

    //typename Container::const_iterator iter = inMap.find(key);
    typename Container::const_iterator iter = inMap.find(key);
    if (iter != inMap.end())
    {
      found = true;
      outMappedType = inMap[key];
    }

    return found;
  }

  // ---------------------------------------------------------


  template <class Container>
  static void addElementToMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type inElement)
  {
    inMap.insert(make_pair(key, inElement));
  }

  // ---------------------------------------------------------

  template <class Container>
  static bool addElementToMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type inElement, std::string& errText)
  {
    bool success = true;
    errText.clear();

    // check if exists
    typename Container::const_iterator iter = inMap.find(key);
    if (iter == inMap.end())
      inMap.insert(make_pair(key, inElement));
    else
    {
      success = false;
      errText = "Key: " + key + ", is already present. Change key name.";
    }

    return success;
  } // end template


  // ---------------------------------------------------------

    template <class Container>
    static void cloneMap(Container& inSource, Container& inTarget)
    {
    
        // loop over source and insert into target map
        inTarget.clear();
        
        for (auto a : inSource )
        {
            Utils::addElementToMap(inTarget, a.first, a.second);
        }
 
        
    } // end template
  // ---------------------------------------------------------
  // ---------------------------------------------------------

  // Convert String into Number with precision
  template <typename N>
  static N stringToNumber(std::string s, int precision)
  {
    std::istringstream is(s);
    N n;
    std::string num; num.clear();
    for (int i = 0; i < precision; i++)
    {
      char c = '\0';
      is.get(c);
      num += c;
    }
    std::istringstream isNum(num);
    isNum >> n;

    //is >> std::setprecision(precision) >> std::setiosflags(ios::fixed) >> n;
    

    return n;
  }


// ---------------------------------------------------------

  /// <summary>
  /// ltrim_text_on_all_lines() function will remove spaces from the begining of any l;ine in text string and not just from the first line.
  /// </summary>
  /// <param name="inText">Original test to be trimmed.</param>
  /// <returns>returns a new text string</returns>
  static std::string ltrim_text_on_all_lines(const std::string inText);
// ---------------------------------------------------------

  static int getPrecisionFromString(std::string &inValue);

// ---------------------------------------------------------
// splitStringToLines() function will create lines of strings based on limiting width sent to it.
// The new line will be determined by the ";" character or if reached "width" characters.
// The function concatenate one character by one and evaluate each time the new word.
static std::vector<std::string> splitStringToLines(std::string source, size_t width);

// ---------------------------------------------------------


  /**
    replaceChar1WithChar2
    Replaces a character with another character or string of characters
    Uses: remove the LF and UNIX_EOL from strings
  */
static std::string replaceChar1WithChar2(std::string inString, char charToReplace, std::string newChar);

static std::string replaceChar1WithChar2_v2(std::string inString, char charToReplace, std::string newChar);

// ---------------------------------------------------------
/**
replaceCharsWithString
Replaces set of characters, one by one, with another string of characters
Uses: remove the LF and UNIX_EOL from strings
*/
static void replaceCharsWithString(std::string &outString, std::string charsToReplace, std::string newChar);

// ---------------------------------------------------------
static std::string replaceStringWithOtherString(std::string inStringToModify, std::string inStringToReplace, std::string inNewString, bool flag_forAllOccurances = false);

// ---------------------------------------------------------
static std::string getXPlanePrefFileAndPath ( string fileName )
{
	char path[1024];
  std::string result; result.clear();

	XPLMGetPrefsPath(path);
	XPLMExtractFileAndPath(path);
  result = std::string(path);
  result += XPLMGetDirectorySeparator() + fileName;

  return result;
}

// ---------------------------------------------------------
// ---------------------------------------------------------

static std::string getXPlaneInstallFolder( )
{
  std::string dir; dir.clear();
  char sys_path[1024];

  XPLMGetSystemPath(sys_path);
  dir.assign(sys_path);

  return dir;
}

// ---------------------------------------------------------

static std::string getPluginDirectoryWithSep( string pluginDirName, bool isRelative = false)
{
  std::string result = std::string("Resources") + XPLMGetDirectorySeparator() + "plugins";

  if (!isRelative)
  {
    char path[1024];
    XPLMGetSystemPath(path);
    result.insert (0, path );
  }

  result += XPLMGetDirectorySeparator() + pluginDirName + XPLMGetDirectorySeparator();

  return result;
}


// ---------------------------------------------------------
static std::string getCustomSceneryRelativePath()
{
  static std::string str;
  str = "Custom Scenery";
  str.append(XPLMGetDirectorySeparator());

  return str;
}

// ---------------------------------------------------------

std::vector<std::string> static buildSentenceFromWords(std::vector<std::string> inTokens, size_t allowedSentenceLength, size_t maxLinesAllowed/*, size_t *outRealLinesCount*/);

// ---------------------------------------------------------
std::vector <std::string> static splitString_to_vec(const std::string inString, char delimeter, bool b_remove_spaces, bool b_skipEmptyString = false);
std::map <std::string, std::string> static splitString_to_map(const std::string inString, char delimeter, bool b_remove_spaces, bool b_skipEmptyString = false);

// ---------------------------------------------------------
std::vector<std::string> static splitString(std::string inString, std::string delimateChars);
// ---------------------------------------------------------

std::map<int, std::string> static splitStringToMap (std::string inString, std::string delimateChars); // returns set of key,values. Key is seq starting from 0... while value is string

// ---------------------------------------------------------

// Simple template to split string into numeric vector
// Do not use strings with this function. For strings use the simple: "splitStringToNumbers()
template <class T>
std::vector<T> static splitStringToNumbers(std::string inString, std::string delimateChar)
{
  //flag_isNumber = true;
  std::vector<T> vecResult;
  std::vector<std::string> vecSplitResult;

  vecSplitResult.clear();
  vecSplitResult = Utils::splitString(inString, delimateChar);
  for (auto s : vecSplitResult)
  {
    if (Utils::is_number(s))
    {
      T val = (T)Utils::stringToNumber<T>(s, 6);
      vecResult.push_back(val);
    }

  }

  return vecResult;
}
// ---------------------------------------------------------

std::list<std::string> static splitStringUsingStringDelimiter(std::string & inString, std::string delimateChars);

// ---------------------------------------------------------
// Extract base string from a string. Good to extract file name without the extension "[file].exe", for example.
static std::string extractBaseFromString(std::string inFullFileName, std::string delimiter = ".", std::string *outRestOfString = nullptr);

// ---------------------------------------------------------
// Extract base string from a string. Good to extract file name without the PATH, for example.
static std::string extractLastFromString(std::string inFullFileName, std::string delimiter = ".", std::string *outRestOfString = nullptr);

// ---------------------------------------------------------
// This function split words into sentences, while trying to keep the line width in boundaries.

// v3.0.223.1 main function to construct sentences with length in mind.
void static sentenceTokenizerWithBoundaries(std::deque<std::string> & outDeque, std::string &inString, std::string delimiterChar, size_t width = 0, std::string in_special_LF_char = ";"); 

// convert the std::deque sentence to vector, where vector is used. TODO: maybe change all calls to this function to use the deque only function.
std::vector<std::string> static sentenceTokenizerWithBoundaries(std::string inString, std::string delimiterChar, size_t width = 0, std::string in_special_LF_char = ";"); 

// v3.0.223.1 add the new string to the sentence string. Take into consideration limiting length and too long strings.
std::string static add_word_to_line(std::deque<std::string>& outDeque, std::string inCurrentLine_s, std::string inNewWord_s, int inMaxLineLength_i, bool flag_force_new_line = false); 

// ---------------------------------------------------------

std::vector<std::string> static tokenizer(std::string inString, char delimateChar, size_t width);

// ---------------------------------------------------------

static void CalcWinCoords(int inWinWidth, int inWinHeight, int inWinPad, int inColPad, int& left, int& top, int& right, int& bottom); // Calculate window's standard coordinates

// ---------------------------------------------------------

static void getWinCoords(int& left, int& top, int& right, int& bottom); // get window's standard coordinates

// ---------------------------------------------------------

static std::string xml_readAttrib(pugi::xml_node& node, std::string attribName, std::string defaultValue);

// ---------------------------------------------------------

template <class T>
static T xml_readNumericAttrib(pugi::xml_node& node, const std::string attribName, const T defaultValue)
{
  if (node.empty())
    return defaultValue;

  const std::string val = Utils::xml_readAttrib(node, attribName, mxUtils::formatNumber<T>(defaultValue));
  if (val.empty())
    return defaultValue;
  else if (mxUtils::is_number( val ))
  {
    return mxUtils::stringToNumber<T>(val);
  }

  return defaultValue;
}

// ---------------------------------------------------------

static std::string nameForNumber(long number); // based on https://stackoverflow.com/questions/40252753/c-converting-number-to-words

// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------
// ---------------------------------------------------------
};






} // namespace
#endif /*UTILS_H_*/
