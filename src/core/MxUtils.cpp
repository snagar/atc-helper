#include "MxUtils.h"

#ifdef MAC
#include <cmath> // solve std::remindar in XCode
#endif

atchelper::mxUtils::mxUtils()
{
}

atchelper::mxUtils::~mxUtils()
{
}



// --------------------------------------------------------------
std::string atchelper::mxUtils::stringToUpper(std::string strToConvert)
{//change each element of the string to upper case

  std::for_each(strToConvert.begin(), strToConvert.end(), [](char& c) { c = toupper(c); });
  return strToConvert;
}

// --------------------------------------------------------------

std::string atchelper::mxUtils::stringToLower(std::string strToConvert)
{//change each element of the string to lower case
  for (size_t i = 0; i<strToConvert.length(); i++)
  {
    strToConvert[i] = tolower(strToConvert[i]);
  }
  return strToConvert;//return the converted string
}


// --------------------------------------------------------------

bool atchelper::mxUtils::is_alpha(const std::string &str)
{
  return std::all_of(str.begin(), str.end(), ::isalpha); // C++11
}

// --------------------------------------------------------------

bool atchelper::mxUtils::is_digits(const std::string &str)
{
  //return str.find_first_not_of("0123456789.") == std::string::npos;
  return std::all_of(str.begin(), str.end(), ::isdigit); // C++11
}

// --------------------------------------------------------------

//Is string has only these characters - "0123456789.", and no more than 1 "." !!!
bool atchelper::mxUtils::is_number(const std::string& s)
{
  bool validNumber = true;

  bool minusPlusNotAllowed = false; // as long as we did not found digits, we can use "+/-" signs

  bool foundDecimalDot = false;

  std::string::const_iterator it = s.begin();
  while (it != s.end() && validNumber)
  {
    // number is legal if starts with "+/-" signs
    if (!minusPlusNotAllowed && (((*it) == '+') || ((*it) == '-')))
    {
      minusPlusNotAllowed = true; // only one sign is allowed at the begining
      ++it;
    }
    else if ((*it) == '.' && !foundDecimalDot)
    {
      foundDecimalDot = true;
      minusPlusNotAllowed = true;
      ++it;
    }
    else if (::isdigit(*it))
    {
      minusPlusNotAllowed = true;
      ++it;
    }
    else
      validNumber = false;
  } // end while

  return !s.empty() && it == s.end();
}

// --------------------------------------------------------------

std::string atchelper::mxUtils::ltrim(std::string str, const std::string chars )
{
  // trim leading spaces
  size_t startpos = str.find_first_not_of(chars);//(" \t");
  if (std::string::npos != startpos)
  {
    str = str.substr(startpos);
  }

  return str;
} // ltrim

  // --------------------------------------------------------------
std::string atchelper::mxUtils::rtrim( std::string str, const std::string chars )
{
  str.erase(str.find_last_not_of(chars) + 1);
  return str;
}

// --------------------------------------------------------------
std::string atchelper::mxUtils::trim( std::string str, const std::string chars )
{
  return ltrim(rtrim(str, chars), chars);
}

// --------------------------------------------------------------

bool atchelper::mxUtils::isStringBool(std::string inTestValue, bool &outStringResult_asBool)
{
  const std::string inValue = stringToLower(inTestValue);

  if (inValue.length() == 1)
  {
    char c = inValue[0];
    //flag_is_bool = (bool)c;

    switch (inValue[0])
    {
    case '1':
    case 'y':
    {
      outStringResult_asBool = true;
      return true;
    }
    break;
    case '0':
    case 'n':
    {
      outStringResult_asBool = false;
      return true;
    }
    break;
    default:
      break;

    }

    outStringResult_asBool = false;
    return false; // we can use this one char value as bool

  }
  else if ((atchelper::MX_TRUE.compare(inValue) == 0) || (atchelper::MX_YES.compare(inValue) == 0))
  {
    outStringResult_asBool = true; // true for boolean or numbers
    return true;
  }
  else if ((atchelper::MX_FALSE.compare(inValue) == 0) || (atchelper::MX_NO.compare(inValue) == 0))
  {
    outStringResult_asBool = false; // false for boolean or numbers
    return true; // special case where the value is boolean but its value is false, we need to return if the value was found from function
  }
  else if (is_number(inValue))
  {
    outStringResult_asBool = stringToNumber<bool>(inValue);
    return true; // special case // number can be 0 or minus
  }
  else
  {
    // fail to parse attrib string to number. Setting property to false;
    outStringResult_asBool = false;

  }

  return false;
}

std::string atchelper::mxUtils::remove_char_from_string(const std::string& inVal, const char inCharToRemove)
{
  // The following function will return a new string without the specific character. This character will be removed from all the string
  std::string retVal_s;
  for (auto& c : inVal)
  {
    if (c == inCharToRemove)
      continue;

    retVal_s.push_back(c);

  }
  
  return retVal_s;
}

std::vector<std::string> atchelper::mxUtils::split(const std::string & s, char delimiter)
{
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter))
  {
    if (token.empty())
      continue;

    tokens.push_back(token);
  }
  return tokens;
}

std::string atchelper::mxUtils::translateMxPadLabelPositionToValid(std::string inLabelPosition)
{
  std::string mLabelPosition;
  // v3.0.197 - translate position string to valid one
  mLabelPosition = mxUtils::stringToUpper(inLabelPosition);
  if (mLabelPosition.compare("L") == 0 || mLabelPosition.compare("LEFT") == 0)
    mLabelPosition = "L";
  else if (mLabelPosition.compare("R") == 0 || mLabelPosition.compare("RIGHT") == 0)
    mLabelPosition = "R";
  else if (!mLabelPosition.empty())
    mLabelPosition = "L";


  return mLabelPosition;
}

// --------------------------------------------------------------


std::string atchelper::mxUtils::emptyReplace(std::string inValue, std::string inReplaceWith)
{
  if (inValue.empty())
    return inReplaceWith;

  return inValue;
}

float atchelper::mxUtils::convert_skewed_bearing_to_degrees(const float inBearing)
{
  if (inBearing >= 0.0f && inBearing <= 360.0f)
    return inBearing;

  //const auto reminder = std::remainder(inBearing, atchelper::DEGREESE_IN_CIRCLE); // 360.0f represent 360 degrees in a circle.
  //const float correct_bearing_f = (reminder < 0)? reminder + atchelper::DEGREESE_IN_CIRCLE : reminder;

  //const float correct_bearing_f_v2 = fmod(inBearing, 360.0f);

  //return correct_bearing_f;
  return  fmod(inBearing, 360.0f);

}

float atchelper::mxUtils::convert_skewed_bearing_to_degrees(const std::string inBearing_s)
{
  //const float bearing_f = mxUtils::stringToNumber<float>(inBearing_s, 3);
  //return atchelper::mxUtils::convert_skewed_bearing_to_degrees(bearing_f);

  return fmod(mxUtils::stringToNumber<float>(inBearing_s, 3), 360.0f);
}

std::string atchelper::mxUtils::convert_skewed_bearing_to_degrees_return_as_string(const std::string inBearing_s)
{
  //auto bearing_f = mxUtils::convert_skewed_bearing_to_degrees(inBearing_s);
  return mxUtils::formatNumber<float>(mxUtils::convert_skewed_bearing_to_degrees(inBearing_s), 3 );
}

atchelper::mx_btn_colors atchelper::mxUtils::translateStringToButtonColor(std::string inColor)
{
  //mx_btn_colors color_val;

  if (atchelper::YELLOW.compare(inColor) == 0)
    return atchelper::mx_btn_colors::yellow;
  else if (atchelper::RED.compare(inColor) == 0)
    return atchelper::mx_btn_colors::red;
  else if (atchelper::GREEN.compare(inColor) == 0)
    return atchelper::mx_btn_colors::green;
  else if (atchelper::BLUE.compare(inColor) == 0)
    return atchelper::mx_btn_colors::blue;
  else if (atchelper::BLACK.compare(inColor) == 0)
    return atchelper::mx_btn_colors::black;

  return atchelper::mx_btn_colors::white;
}

std::string atchelper::mxUtils::getFreqFormated(const int freq)
{
  const std::string freq_s = mxUtils::formatNumber<int>(freq);
  switch (freq_s.length())
  {
    case 5:
    case 6:
    {
      return (freq_s.substr(0, 3) + "." + freq_s.substr(3));
    }
    break;
    default:
      break;

  } // end switch

  return freq_s;
}

// --------------------------------------------------------------

std::string atchelper::mxUtils::replaceAll(std::string str, const std::string& from, const std::string& to)
{
  if (!from.empty())
  {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
      str.replace(start_pos, from.length(), to);
      start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
  }// end if "from" has value

  return str;
}



// --------------------------------------------------------------
// --------------------------------------------------------------


