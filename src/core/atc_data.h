#ifndef RW_DATA_MANAGER_H_
#define RW_DATA_MANAGER_H_
#pragma once

#include <queue>

#include <filesystem>
#include "../io/Log.hpp"
#include "Timer.hpp"
//#include "coordinate/NavAidInfo.hpp"

#include "xx_atc_helper_const.hpp"
#include "../../libs/imgui4xp/ImgWindow/ImgWindow.h" 
#include "../io/BitmapReader.h"

using namespace atchelper;

namespace atchelper
{


/**
rw_data holds ALL STATIC information that we want to share with the plugin, example: tasks, objective, folder information, global settings, seed ext parameters etc.
It sored the information in static maps, and has key functions to add tasks/objectives and goals to maps.
*/


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////


class atc_data
{
private:
  
public:
  atc_data();
  virtual ~atc_data();
  static void init();
  
 
  // plugin folder
  const static std::string PLUGIN_PATH;


  // draw callback flag
  //static bool needDrawCallBack; // Ben Supnik asked for this https://developer.x-plane.com/2018/01/plugins-do-not-register-drawing-callbacks-that-do-not-draw/

  // VR Related
  //static XPLMDataRef g_vr_dref; // holds if in VR mode
  static void flc();
  static int flag_in_vr; // are we in VR state
  static int flag_prev_in_vr; //


  // APT DAT OPTIMIZATION FLAGS
  //static bool flag_rw_optimization_is_running;
  //static bool flag_generate_engine_is_running;
  //static std::map<std::string, int> mapIndexNavAids;
  //static std::map< std::string, std::list<std::string> > cachedNavInfo_map;
  

  // GL Draw
  static bool flag_can_draw;
  static bool flag_drawcallback_is_active;

  // Clear data
  static void stop_plugin();

  //////// STRUCTS and ENUM //////////

  // Plane position
  typedef struct _plane_pos_strct
  {
    bool flag_first_time;
    float prevFaxil, curFaxil;

    _plane_pos_strct()
    {
      reset();
    }
    void reset()
    {
      flag_first_time = true;
      prevFaxil = curFaxil = 0.0f;
    }
  } xx_plane_pos_strct;

  static xx_plane_pos_strct plane_pos;
  static void draw();
  static bool flag_plane_in_rw_area;
  static float get_plane_faxil();


  // FLC Pre FLC commands
  // set of commands that need to be execute from "plugin" or "flc" phase since we don't want to interrupt flow of code
  typedef enum class _flc_commands : uint8_t
  {
    toggle_atc_helper_window,
    push_loaded_xpath_data_to_window,
    task_read_config_file,
    task_congifImgWindow_fontAtlas,
    task_reload_xml_atc_template_file,
    task_read_atc_config_data_from_loaded_xml_and_set_atc_conf_layer,
    task_load_xpath_data, // will be used only if doc was already loaded = task_reload_xml_atc_template_file
    task_reapply_user_option_on_text_from_main_thread,
    save_atc_setup
  } atc_flc_pre_command;

  // Queue actions in Flight Loop Callback
  static std::queue <std::string> queThreadMessage; // holds messages from threads to display in briefer. use: "atchelper::data_manager::queFlcActions.push(atchelper::mx_flc_pre_command::set_briefer_text_message);" but set the que before hand
  static std::queue <atc_data::atc_flc_pre_command> queFlcActions;
  static std::queue <atc_data::atc_flc_pre_command> postFlcActions; //v3.0.146 // we will place actions in this Queue if we want them to happen after flc() so xplane updates its state. Example pause after position plane
  static void clearAllQueues();
  // END FLC Queue



   ///// Dispatcher
  typedef enum class _atc_dispatch : uint8_t
  {
    idle    
  } atc_dispatch_task;

  static std::deque <atc_dispatch_task> dqDispatcher;
  static atc_dispatch_task current_executing;
  static void reset_dispatcher(atc_dispatch_task inValue = atc_data::atc_dispatch_task::idle) { atc_data::current_executing = inValue; }
  ///// END Dispatcher



  // Textures
  static std::map <std::string, atchelper::mxTextureFile> mapPluginTextures; // plugin specific textures



  // INI File
  static std::map <std::string, std::string> mapConfig;
  static void read_config_file(std::string_view inDefaultConfigText);



  // XML Template
  static std::string load_xml_err_s;
  static pugi::xml_document xml_main_doc_template_ptr; // holds the inmemory DOM of the XML file
// read all comm_standard elements. Mainly: <ATC><IFR><US>. Will store the first and second sub siblings. The first will be the key, and the second will be the vector.
  static const std::map <std::string, std::vector <std::string>> get_flightType_and_commStandards_from_xml_template(pugi::xml_document& doc);

  static std::map <int, atc_category_strct> mapAtcCategories; // will hold only the data from specific flight type and region
  static void xml_search_xpath_and_load_data_to_mapAtcCategories(const pugi::xml_document& doc, const std::string inPathQuery);


  // ATC Phonetics
  static std::string get_translated_string_to_phonetic_alphabet(std::string_view inVal, bool translateNumerics_b = false);
  static std::string get_translated_string_to_phonetic_elevation(std::string inVal);

  // doc8643: Read airplains information file
  //static std::string mapDoc8643_filepath_s;
  //static std::map <std::string, std::string> mapDoc8643;

  static std::filesystem::path filepath_to_Doc8643;
  //const static std::map <std::string, std::string> mapDoc8643;
  const static std::vector <std::string> vecDoc8643;
  static std::vector <const char*> vecDoc8643_char; // will holds the pointers to all strings in vecDoc8643 for ImGui Filter widget
  static int vecDoc8643_size_i; //
  static const std::vector <std::string> get_doc8643(std::filesystem::path& filepath);
  static const std::map <std::string, std::string> get_parsed_doc8643(std::filesystem::path& filepath);

private:

  static void readPluginTextures();
  static void clearPluginTextures();


}; // end class

}
#endif
