#ifndef BASE_C_INCLUDE_H_
#define BASE_C_INCLUDE_H_
#pragma once

// **************
#include <sstream>
#include <iostream>
#include <iomanip>  
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <stdio.h>
#include <errno.h>
#include <algorithm>
#include <stdexcept>


#include <math.h>
#include <cstring>
#include <cstdlib>


//v2.1.27 a3
#include <chrono>
#include <mutex>
#include <thread>
#include <future>

//// OpenGL Support
//#if defined(LIN) || defined(IBM)
//  #include <GL/glew.h>
//  #include <GL/glext.h>
//#else 
//  #define TRUE 1
//  #define FALSE 0
//
//
//
//  //#define NULL 0
// //#include <GL/glew.h>
////#define __gl_h_
//#define GL_DO_NOT_WARN_IF_MULTI_GL_VERSION_HEADERS_INCLUDED
//
//
//
//  #include <OpenGL/gl.h>
//  #include <OpenGL/gl3.h>
//  #include <OpenGL/glu.h>
//
//#endif


#ifdef APL
  #include <Carbon/Carbon.h>
#endif

// using namespace std;



#endif  //BASE_C_INCLUDE_H_

