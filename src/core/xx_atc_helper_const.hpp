#ifndef XX_MY_CONSTANTS_HPP
#define XX_MY_CONSTANTS_HPP
#pragma once


#include "base_c_includes.h"
#include <deque>
#include "XPLMDisplay.h"
#include "XPLMDataAccess.h"
//#include "vr/mxvr.h"

namespace atchelper
{


#define PLUGIN_VER_MAJOR "0"
#define PLUGIN_VER_MINOR "4"
#ifndef RELEASE
#define PLUGIN_REVISION  "00"
#else
#define PLUGIN_REVISION  "00"
#endif


  //// GLOBAL CONSTANTS
  const static std::string PLUGIN_NAME = "atc-helper";
  constexpr auto FULL_VERSION = PLUGIN_VER_MAJOR  "." PLUGIN_VER_MINOR "." PLUGIN_REVISION " " __DATE__;
  
  // Config file related information
  const static std::string CONFIG_FILENAME = PLUGIN_NAME + ".ini"; // config file name
  const static std::string CONFIG_MAX_WIDTH = "maxWidth"; // represents max width of window
  const static std::string CONFIG_MAX_HEIGHT = "maxHeight";
  const static std::string CONFIG_WIDTH = "width"; // represent starting width
  const static std::string CONFIG_HEIGHT = "height";
  const static std::string CONFIG_CALLSIGN = "callsign";
  const static std::string CONFIG_CALLSIGN_AS_PHONETIC = "callsign_as_phonetic";
  const static std::string CONFIG_PLANE_TYPE = "plane_type";
  const static std::string CONFIG_PLANE_TYPE_AS_PHONETIC = "plane_type_as_phonetic";
  const static std::string CONFIG_PLANE_MANUFACTURER = "manufacturer"; // 0.3.0
  const static std::string CONFIG_FLIGHT_TYPE = "flight_type";
  const static std::string CONFIG_COMM_STANDARD = "comm_standard"; // UK/US

  const static std::string ATC_TEMPLATE_FILENAME = "atc-template.xml";
  const static std::string DOC8643_FILENAME = "Doc8643.txt";

  //// Global Char Buffer for Login
  const int LOG_BUFF_SIZE = 2048;
  static char LOG_BUFF[LOG_BUFF_SIZE];


  const static intptr_t DAYS_IN_YEAR_365 = 365;
  const static intptr_t SECONDS_IN_1HOUR_3600 = 3600;
  const static intptr_t SECONDS_IN_1MINUTE = 60;
  const static intptr_t SECONDS_IN_1DAY = 86400;
  const static intptr_t HOURS_IN_A_DAY_24 = 24;

  const static std::string EMPTY_STRING = "";
  const static std::string MX_TRUE = "true";
  const static std::string MX_YES = "yes";
  const static std::string MX_FALSE = "false";
  const static std::string MX_NO = "no";


  const static std::string UNIX_EOL = "\n"; // used in Unix like OSes for end of line
  const static std::string WIN_EOL = "\r\n"; // Windows end of line
  const static std::string QM = "\""; // Quotation Mark
  const static std::string SPACE = " "; // SPACE
  const static std::string FOLDER_SEPARATOR = "/"; // folder separator

  const static std::string WHITE = "white";
  const static std::string RED = "red";
  const static std::string YELLOW = "yellow";
  const static std::string GREEN = "green";
  const static std::string DARK_GREEN_01 = "dark_green_01";
  const static std::string DARK_GREEN_02 = "dark_green_02";
  const static std::string ORANGE = "orange";
  const static std::string PURPLE = "purple";
  const static std::string BLUE = "blue";
  const static std::string BLACK = "black";


  const static std::string DEFAULT_FONT_LOCATION = "./Resources/fonts/DejaVuSans.ttf";
  const static std::string FONT_PATH = "./Resources/fonts/";

  constexpr float DEFAULT_FONT_SIZE = 14.0f;
  constexpr float DEFAULT_MIN_FONT_PIXEL_SIZE = 12.0f;
  constexpr float DEFAULT_MAX_FONT_PIXEL_SIZE = 16.0f;

  constexpr float DEFAULT_MIN_FONT_SCALE = 0.8f;
  constexpr float DEFAULT_MAX_FONT_SCALE = 1.4f;
  constexpr float DEFAULT_BASE_FONT_SCALE = 1.0f;

  /////// BITMAP ///////
  const static std::string BITMAP_ALPHABET = "alphabet.png";
  const static std::string BITMAP_PATTERN = "pattern.png";
  //const static std::string BITMAP_ALPHABET_AND_PATTERN = "alphabet_and_pattern.png";
  const static std::string BITMAP_BACKGROUND = "light_blue01_16x16.png";
  const static std::string BITMAP_BACKGROUND_TRANS = "light_blue_trans01_16x16.png";


  ///// ATC Special keywards ///////
  const static std::string ATC_CALLSIGN = "%callsign%"; // display callsign in Phonetic string. Example NG13 will displayed: November Golf One Three
  const static std::string ATC_SIGN = "%sign%"; // will display non phonetic callsign value. Example NG13 will displayed: NG13 (the same)
  const static std::string ATC_PLANE_TYPE = "%plane_type%";
  const static std::string ATC_MANUFACTURER = "%manufacturer%";
  const static std::string ATC_DEP_AIRPORT = "%dep_airport%";
  const static std::string ATC_DEST_AIRPORT = "%dest_airport%";
  const static std::string ATC_STAND = "%stand%";
  const static std::string ATC_ATIS_CODE = "%atis_code%";



  /////// Phonetic Alphabet ///////
  const static std::map <std::string, std::string> atc_phonetics = 
                                                               { {"A","Alfa"},{"B","Bravo"},{"C","Charlie"},{"D","Delta"},{"E","Echo"},{"F","Foxtrot"},{"G","Golf"},{"H","Hotel"},{"I","India"},{"J","Juliett"},
                                                                 {"K","Kilo"},{"L","Lima"},{"M","Mike"},{"N","November"},{"O","Oscar"},{"P","Papa"},{"Q","Quebec"},{"R","Romeo"},{"S","Sierra"},{"T","Tango"},
                                                                 {"U","Uniform"},{"V","Victor"},{"W","Whiskey"},{"X","Xray"},{"Y","Yankee"},{"Z","Zulu"},{".","Decimal"},
                                                                 {"1","One"},{"2","Two"},{"3","Three"},{"4","Four"},{"5","Fife"},{"6","Six"},{"7","Seven"},{"8","Ait"},{"9","Niner"},{"0","Zero"}
                                                               }; 

  const static std::vector<const char*>vecAtisCodes = { {""},{"Alfa"},{"Bravo"},{"Charlie"},{"Delta"},{"Echo"},{"Foxtrot"},{"Golf"},{"Hotel"},{"India"}, 
                                                        {"Juliett"}, {"Kilo"}, {"Lima"}, {"Mike"}, {"November"}, {"Oscar"}, {"Papa"}, {"Quebec"}, {"Romeo"}, {"Sierra"}, 
                                                        {"Tango"}, {"Uniform"}, {"Victor"}, {"Whiskey"}, {"Xray"}, {"Yankee"},{"Zulu"} };
                                                               


  static const std::vector<std::string> ones{ "","one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
  static const std::vector<std::string> teens{ "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen","sixteen", "seventeen", "eighteen", "nineteen" };
  static const std::vector<std::string> tens{ "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

  ////// ENUMS //////
  typedef enum class _options_enums : uint8_t
  {
    option_byValue = 0,
    option_by_phonetic = 1,
    option_by_phonetic_elev = 2
  } enm_option_type;

  typedef enum class _mx_btn_colors : uint8_t
  {
    white,
    yellow,
    red,
    green,
    blue,
    purple,
    orange,
    black
  } mx_btn_colors;

  typedef enum class _random_thread_wait_state : uint8_t
  {
    not_waiting = 0, // we need this if designer used wrong string for channel
    waiting_for_plugin_callback_job,
    finished_plugin_callback_job
  } mx_random_thread_wait_state_enum;


  typedef enum class _mxWindowActions : uint8_t
  {
    ACTION_NONE, // initialize action
    ACTION_TOGGLE_WINDOW, // only change window state (show / hide ) should not modify layer.
    ACTION_HIDE_WINDOW, 
    ACTION_SHOW_WINDOW,
    ACTION_CANCEL, 
    ACTION_CLOSE_WINDOW,
    ACTION_READ_ATC_AND_DISPLAY,
    ACTION_RELOAD_XML_TEMPLATE,
    ACTION_CALL_APPLY_USER_ATC_OPTION_FROM_MAIN_THREAD,
    ACTION_APPLY_USER_ATC_OPTION,
    ACTION_SAVE_CONFIGURATION
  } mx_window_actions; // for all windows. some actions are specific and some shared

  typedef struct _atc_task
  {
    int seq{ 0 }; // we will use the sequence as inumerator
    std::map <int, std::string> mapName; // example: clearence, taxt, tower. Task name in Category element
    std::map <int, std::string> mapText;  // all the text in the task
  } atc_tasks_strct;

  typedef struct _atc_category
  {
    int seq_category_i{ 0 };
    std::string category_name{ "" };
    atc_tasks_strct category_tasks;

    // option strings
    std::vector <std::string> vecDynamicOptions; // split the attribute dynamic_options
    std::vector <const char*> vecDynamicOptions_char; // pointer to string for imgui::choice widget
  } atc_category_strct;

  typedef enum class _uiLayer : uint8_t
  {
    setup_layer=0,
    atc_layer,
    about_layer
  } uiLayer_enum;

#ifdef IBM
  const class atc_drefs
#else
	class atc_drefs
#endif
  {
  public:
    ~atc_drefs(void) {};

    //  std::map <std::string, XPLMDataRef> map_dref;

    atc_drefs(void)
    {

    }


    const XPLMDataRef dref_faxil_gear_f = XPLMFindDataRef("sim/flightmodel/forces/faxil_gear"); // faxil_gear
    const XPLMDataRef dref_groundspeed_f = XPLMFindDataRef("sim/flightmodel/position/groundspeed"); // Groundspeed meters/sec
    const XPLMDataRef dref_local_date_days_i = XPLMFindDataRef("sim/time/local_date_days"); // local_date_days - Day in year
    const XPLMDataRef dref_zulu_time_sec_f = XPLMFindDataRef("sim/time/zulu_time_sec"); // zulu_time_sec - how many millisiconds from ZULU midnight. When value between   greater 66600 or less 18000 it will mark as night


  }; // end class atc_drefs

}

#endif // XX_MY_CONSTANTS_HPP


