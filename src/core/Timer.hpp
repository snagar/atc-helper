#ifndef TIMER_H_
#define TIMER_H_

#pragma once

#include "TimeFragment.hpp"

using namespace atchelper;

namespace atchelper
{
  typedef enum class _timer_state : uint8_t
  {
    timer_not_set,
    timer_running,
    timer_paused,
    timer_stop,
    timer_end
  } mx_timer_state;

class Timer 
{
private:

  ///// v3 /////
  atchelper::TimeFragment tf_begin;
  atchelper::TimeFragment tf_now;
  atchelper::TimeFragment tf_last; // v3.0.207.1
  float cumulativeXplaneTime_sec; // v3.0.221.11 will hold the secondsPassed and the cumulative seconds between timer stop/start. If we pause the timer then on second start we add the cumulative to the "secondsPassed"
  float secondsPassed;
  float deltaSecondsBetweenFragments; // v3.0.207.1
  float osSecondsPassed; // store osClock delta
  float cumulativeOsTime_sec; // v3.0.221.11

  float secondsToRun; // how much timer should run before stopping

  mx_timer_state timer_state;

  bool runContinuasly; // if we send "secondsToRun=0" then continuasly run
  bool flag_isCumulative; // v3.0.221.11 stop won't reset the time and we need to test against the cumulativeXXX varables instead on the seconds
  bool flag_isBasedSysClock; // v3.0.223.1 

  std::string name; // for debug purposes

public:

  Timer()
  {
    reset();
  }

  /* ************************************************* */

  void reset()
  {
    secondsToRun = 0.0f;
    secondsPassed = osSecondsPassed = 0.0f;
    deltaSecondsBetweenFragments = 0.0f; // v3.0.207.1
    runContinuasly = false;
    timer_state = mx_timer_state::timer_not_set;
    name.clear(); 

    cumulativeOsTime_sec = cumulativeXplaneTime_sec = 0.0f; // v3.0.221.11
  }


  /* ********************************************** */
  
  void clone(Timer &inTimer)
  {
    reset();
    this->tf_begin = inTimer.tf_begin;
    this->tf_now = inTimer.tf_now;
    this->tf_last = inTimer.tf_last; // v3.0.207.1
    this->deltaSecondsBetweenFragments = this->tf_now - this->tf_last; // v3.0.207.1

    this->cumulativeXplaneTime_sec = inTimer.cumulativeXplaneTime_sec; // v3.0.221.11
    this->cumulativeOsTime_sec = inTimer.cumulativeOsTime_sec; // v3.0.221.11

    this->secondsPassed = inTimer.secondsPassed;
    this->osSecondsPassed = inTimer.osSecondsPassed;
    this->secondsToRun = inTimer.secondsToRun;

    this->timer_state = inTimer.timer_state;
    this->runContinuasly = inTimer.runContinuasly;
    this->name = inTimer.name;
  }

  /* ************************************************* */
  
  void operator= (Timer &inTimer) { this->clone(inTimer); }

  /* ************************************************* */

  static std::string get_current_time_and_date( std::string date_format = "%Y-%m-%d %H:%M:%S") //  "%Y-%m-%d %X"
  {
    

    auto now = std::chrono::system_clock::now();
    auto in_raw_time_t = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
#if defined LIN || defined MAC
    ss << std::put_time(std::localtime(&in_raw_time_t), date_format.c_str());
#else
    struct std::tm timeinfo;
    localtime_s(&timeinfo, &in_raw_time_t);
    ss << std::put_time(&timeinfo, date_format.c_str());
#endif

    return ss.str();
  }

  /* ************************************************* */

  void stop()
  {
    // TODO: need to store seconds run until now
    timer_state = mx_timer_state::timer_stop;
  }

  /* ************************************************* */

  void pause()
  {
    // TODO: need to store seconds run until now
    timer_state = mx_timer_state::timer_paused;
  }

  /* ************************************************* */
  float getCumulativeXplaneTimeInSec()
  {
    return this->cumulativeXplaneTime_sec;
  }

  /* ************************************************* */
  float getCumulativeOSTimeInSec()
  {
    return this->cumulativeOsTime_sec;
  }


  /* ************************************************* */


  bool getIsCumulative()
  {
    return this->flag_isCumulative;
  }

  /* ************************************************* */

  void setCumulative_flag(bool inVal)
  {
    this->flag_isCumulative = inVal;
  }

  /* ************************************************* */

  void setEnd()
  {
    timer_state = mx_timer_state::timer_end;

  }

  /* ************************************************* */
  void resume()
  {
    // TODO: need to find the delta timer was in pause. we should use cumulativeTime and add it to the "secondsPassed"
    timer_state = mx_timer_state::timer_running;
  }

  /* ************************************************* */
  float getSecondsToRun()
  {
    return secondsToRun;
  }

  float getSecondsPassed()
  {
    if (timer_state == mx_timer_state::timer_running)
    {
      Timer::wasEnded(*this); // check now - begin
      return this->secondsPassed;
    }
      
    return secondsPassed;
  }

  /* ************************************************* */
  
  float getOsSecondsPassed()
  {
    return osSecondsPassed;
  }

  /* ************************************************* */
  
  bool isRunning()
  {
    return (timer_state == mx_timer_state::timer_running) ? true : false;
  }

  /* ************************************************* */
  
  mx_timer_state getState()
  {
    return timer_state;
  }

  /* ************************************************* */

  static void start(Timer &inTimer, float inSecondsToRun = 0, std::string inName = "Timer", bool isCumulative = false)
  {
    inTimer.name = inName; // for debug
    inTimer.flag_isCumulative = isCumulative; // v3.0.221.11

    if (inSecondsToRun == 0.0f)
      inTimer.runContinuasly = true;
    else
      inTimer.secondsToRun = (float)inSecondsToRun;

    atchelper::TimeFragment::init(inTimer.tf_begin); // store now
    inTimer.tf_last = inTimer.tf_begin;
    inTimer.tf_now = inTimer.tf_begin;
    inTimer.deltaSecondsBetweenFragments = 0.0f; // v3.0.207.1 // should be ZERO
    inTimer.timer_state = mx_timer_state::timer_running;
  }

  /* ************************************************* */
  static void unpause(Timer &inTimer)
  {
    if (inTimer.timer_state == mx_timer_state::timer_paused)
    {
      inTimer.timer_state = mx_timer_state::timer_running;
      atchelper::TimeFragment::init(inTimer.tf_last);
      //inTimer.tf_last = inTimer.tf_now;
    }
  }

  /* ************************************************* */
  // Progress the clock and calculates delta.
  // return true if "secondsPass" > "secondsToRun"
  // return false if "secondsToRun" was not passed or "runContinuasly" is true.
  static bool wasEnded(atchelper::Timer &inTimer, bool checkOsTime = false /* v3.0.159*/)
  {
    if (inTimer.timer_state == mx_timer_state::timer_running)
    {
      atchelper::TimeFragment::init(inTimer.tf_now);
      // check seconds passed
      inTimer.secondsPassed = inTimer.tf_now - inTimer.tf_begin;
      inTimer.deltaSecondsBetweenFragments = inTimer.tf_now - inTimer.tf_last; // v3.0.207.1
      inTimer.cumulativeXplaneTime_sec += inTimer.deltaSecondsBetweenFragments; // v3.0.221.11
      inTimer.osSecondsPassed = Timer::getOsDurationPassed(inTimer);
      inTimer.cumulativeOsTime_sec += TimeFragment::getOsDurationBetween2TimeFragments(inTimer.tf_now, inTimer.tf_last); // v3.0.221.11
      inTimer.tf_last = inTimer.tf_now; // v3.0.221.11 store current time fragment for cumulative test

      if (inTimer.runContinuasly)
        return false;

      if (inTimer.flag_isCumulative) // v3.0.221.11
      {
        if ((checkOsTime) ? (inTimer.cumulativeOsTime_sec >= inTimer.secondsToRun) : (inTimer.cumulativeXplaneTime_sec >= inTimer.secondsToRun)) // check based OS time or xplane time (when pause won't progress)
        {
          inTimer.timer_state = atchelper::mx_timer_state::timer_end;
          return true; // time passed, stop timer
        }

      }
      else if ( (checkOsTime)? (inTimer.osSecondsPassed >= inTimer.secondsToRun)  : (inTimer.secondsPassed >= inTimer.secondsToRun)) // check based OS time or xplane time (when pause won't progress)
      {
        inTimer.timer_state = atchelper::mx_timer_state::timer_end;
        return true; // time passed, stop timer
      }
    }
    else if (inTimer.timer_state == mx_timer_state::timer_end)
      return true;

    return false; // if timer state is not "timer_end" should return false. "timer_stop" does proove that timer reached its target.
  }


  /* ************************************************* */
  static float getDeltaBetween2TimeFragments(Timer &inTimer) // v3.0.207.1
  {
    return inTimer.deltaSecondsBetweenFragments;
  }
  /* ************************************************* */

  static float getOsDurationPassed(Timer &inTimer)
  {
    //std::chrono::time_point<std::chrono::steady_clock> os_clock = std::chrono::steady_clock::now(); // fetch NOW
    auto duration = chrono::duration_cast<chrono::milliseconds>(inTimer.tf_now.os_clock - inTimer.tf_begin.os_clock).count();    
    return (float)duration / 1000; // milliseconds to seconds
  }

  /* ************************************************* */

  static std::string translateTimerState(mx_timer_state &inState)
  {
    switch (inState)
    {
    case mx_timer_state::timer_running:
      return "Running";
      break;
    case mx_timer_state::timer_paused:
      return "Paused";
      break;
    case mx_timer_state::timer_stop:
      return "Stopped";
      break;
    case mx_timer_state::timer_end:
      return "Ended";
      break;
    default:
      break;

    } // end switch

    return "Not Set";

    // end translate
  } 


 /* ************************************************* */

  static std::string to_string(Timer &inTimer)
  {
    std::string format = "Timer Name: \"" + inTimer.name + "\". State: " + Timer::translateTimerState(inTimer.timer_state) + ". Run Continuasly: " + ((inTimer.runContinuasly) ? "Yes" : "No") + ". ";
    format += ((inTimer.runContinuasly) ? "Seconds to run: " + Utils::formatNumber<float>(inTimer.secondsToRun) + ", " : atchelper::EMPTY_STRING) + "Seconds Passed: " + Utils::formatNumber<float>(inTimer.secondsPassed) + ". Milliseconds Passed: " + Utils::formatNumber<float>(inTimer.osSecondsPassed) + ", Fragment delta: " + Utils::formatNumber<float>(atchelper::Timer::getDeltaBetween2TimeFragments(inTimer) )+ "\n";

    return format;
  }

  /* ************************************************* */

};

} // namespace atchelper

#endif //TIMER_H_
