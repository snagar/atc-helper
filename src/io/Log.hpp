/*
 * Log.h
 *
 *  Created on: Feb 19, 2012
 *      Author: snagar
 */


#ifndef LOG_H_
#define LOG_H_

#include <string>
#include <forward_list>
#include "../core/base_xp_include.h"
//#include "../core/xx_atc_helper_const.hpp"

#include "writeLogThread.h"

namespace atchelper {
  
  typedef enum class _format_type : uint8_t {
    none, // no formatting
    none_cr,
    header,
    footer,
    sub_element_lvl_1,
    sub_element_lvl_2,
    warning,
    attention,
    error
  } format_type;

class Log
{
private:
  static std::string logFilePath;
public:
  Log();
  virtual ~Log();

  static writeLogThread writeThread; // v3.0.217.8


  enum class LOGMODE : uint8_t
  {
    LOG_NO_CR, LOG_INFO, LOG_DEBUG, LOG_ERROR
  };

  /*  Snagar */
  static void logDebugBO(std::string message, bool isThread = false); // v3.0.221.15 rc4 rares - Log in debug build only

  static void logXPLMDebugString(std::string message); // v3.0.221.9

  static void set_logFile(std::string inLogFilePath) //v3.0.217.8
  { 
    logFilePath = inLogFilePath; 
    atchelper::writeLogThread::set_logFilePath(inLogFilePath);
    
    // XPLMDebugString((inLogFilePath + "\n").c_str()); // debug
  } 


  // Simple formating of headers in Log
  static void printHeaderToLog(std::string s, bool isThread = false, format_type format = format_type::header);

  // Simple formating for data in Log
  static void printToLog(std::string s, bool isThread = false, format_type format = format_type::sub_element_lvl_1, bool isHeader = false);


  static void logToFile(std::string msg, LOGMODE mode, bool isThread = false);



  static void logThreadMsg(std::string message, LOGMODE log_mode = LOGMODE::LOG_INFO)
  {
    logToFile(message, log_mode, true);
  }

  static void logThreadMsgDBO(std::string message, LOGMODE log_mode = LOGMODE::LOG_INFO) // debug only
  {
#if defined DEBUG || defined _DEBUG
    logToFile(message, log_mode, true);
#endif
  }

  static void logMsg ( std::string message, LOGMODE log_mode = LOGMODE::LOG_INFO, bool isThread = false )
  {
    logToFile( message, log_mode, isThread );
  }

  static void flc()
  {
    Log::writeThread.flc(); // manage the thread state
  }


  static void stop_plugin();

};


} // end namespace
  
#endif /* LOG_H_ */

