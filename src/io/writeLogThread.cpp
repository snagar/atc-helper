#include "writeLogThread.h"
#include "XPLMUtilities.h"

namespace atchelper
{
  std::queue<std::string> writeLogThread::qLogMessages; // v3.0.217.8  
  std::queue<std::string> writeLogThread::qLogMessages_mainThread; // v3.0.221.4
  std::string writeLogThread::logFilePath;

  std::vector<std::future<bool>> writeLogThread::mWriteFuture;
  std::mutex writeLogThread::s_write_mutex;
}

atchelper::writeLogThread::writeLogThread()
{
  
}


atchelper::writeLogThread::~writeLogThread()
{
  //if (!qLogMessages.empty())
  //  qLogMessages.pop();
}

void atchelper::writeLogThread::flc()
{

  // If thread work finished, we can join the thread
  if (tState.thread_done_work)
  {
    if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      thread_ref.join();

    tState.init();
  }
  else
  // aborting the thread does not mean to clean the messages from queue.
  if (tState.abort_thread )
  {
    if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      thread_ref.join();

    tState.init();
  }
  else
  // start thread
  if (!this->qLogMessages.empty() && !this->tState.is_active)
  {
    tState.init();
    if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      thread_ref.join(); // joining also solved our issue with crashing xplane. error: abort() was called from "win.xpl"

    tState.dataString = atchelper::writeLogThread::logFilePath;
    thread_ref = std::thread(&writeLogThread::exec_thread, this, &tState);
  }
    

}

void atchelper::writeLogThread::stop_plugin()
{
  tState.abort_thread = true;
  if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
    thread_ref.join(); // joining also solved our issue with crashing xplane. error: abort() was called from "win.xpl"

  this->clear_all_messages();
}

void atchelper::writeLogThread::init()
{
  tState.abort_thread = false;
  tState.init();
}

bool atchelper::writeLogThread::exec_thread(thread_state *xxthread_state)
{
  std::lock_guard<std::mutex> lock(writeLogThread::s_write_mutex);

  static std::ofstream ofs;

  xxthread_state->is_active = true;
  xxthread_state->thread_done_work = false;
  xxthread_state->abort_thread = false;


  while (!tState.abort_thread && !atchelper::writeLogThread::qLogMessages.empty() )
  {
    auto msg = atchelper::writeLogThread::qLogMessages.front();

    // we write the messages to the qLogMessages_mainThread "deque" container so we will write them only durin main FLB
    if (!msg.empty())
      atchelper::writeLogThread::qLogMessages_mainThread.push(msg);    

    if (!qLogMessages.empty())
      qLogMessages.pop();


  } // end while loop

  if (ofs.is_open())
    ofs.close();


  xxthread_state->thread_done_work = true;

  return true;
}
