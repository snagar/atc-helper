#include "BitmapReader.h"


#if defined(LIN) || defined(IBM)
#include <GL/glew.h>
#include <GL/glext.h>
#else 
#define TRUE 1
#define FALSE 0

#define GL_DO_NOT_WARN_IF_MULTI_GL_VERSION_HEADERS_INCLUDED

#include <OpenGL/gl.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>

#endif


/**************
**************/

atchelper::BitmapReader::BitmapReader()
{

  RED = NULL, GREEN = NULL, BLUE = NULL;

  RED   = XPLMFindDataRef("sim/graphics/misc/cockpit_light_level_r");
  GREEN = XPLMFindDataRef("sim/graphics/misc/cockpit_light_level_g");
  BLUE  = XPLMFindDataRef("sim/graphics/misc/cockpit_light_level_b");
  COCKPIT_LIGHTS  = XPLMFindDataRef("sim/cockpit/electrical/cockpit_lights");
  LIGHTS_ON       = XPLMFindDataRef("sim/cockpit/electrical/cockpit_lights_on");

  draw_phase = -1;
  current_phase = draw_phase;
}



bool atchelper::BitmapReader::loadGLTexture(mxTextureFile & inTextureFile, bool flipImage_b)
{
  bool bTextureLoad = false;

  std::string TextureFileName; // saar
  TextureFileName.clear(); // saar

  TextureFileName = inTextureFile.getAbsoluteFileLocation();

  // STB Load Image
  if ( loadImageStb( TextureFileName, &inTextureFile.sImageData, flipImage_b))
  {
    bTextureLoad = true;

    
    XPLMGenerateTextureNumbers(&inTextureFile.gTexture, 1);
    XPLMBindTexture2d(inTextureFile.gTexture, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // removed v3.0.251.1 
    //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // removed v3.0.251.1
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0); // added from imgui
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, (GLint)inTextureFile.sImageData.Width, (GLint)inTextureFile.sImageData.Height, 0, ((inTextureFile.sImageData.Channels < 4) ? GL_RGB : GL_RGBA), GL_UNSIGNED_BYTE, img);
#ifdef FLIP_IMAGE
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLint)inTextureFile.sImageData.Width, (GLint)inTextureFile.sImageData.Height, 0, ((inTextureFile.sImageData.Channels < 4) ? GL_RGB : GL_RGBA), GL_UNSIGNED_BYTE, img);
#else 
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLint)inTextureFile.sImageData.Width, (GLint)inTextureFile.sImageData.Height, 0, ((inTextureFile.sImageData.Channels < 4) ? GL_RGB : GL_RGBA), GL_UNSIGNED_BYTE, inTextureFile.sImageData.pData);
#endif 


    stbi_image_free(inTextureFile.sImageData.pData);
#ifdef FLIP_IMAGE
    stbi_image_free(img);
#endif

    inTextureFile.sImageData.pData = nullptr;

  } 
  // end if

  return bTextureLoad;
} 
// end loadGLTexture


bool atchelper::BitmapReader::loadImageStb ( std::string fileName, mxTextureFile::IMAGEDATA *ImageData, bool inFlipImage_b)
{
  int x,y,channels;

  std::string outErr;
  outErr.clear();
  

  if (inFlipImage_b)
    stbi_set_flip_vertically_on_load(true);
  else 
    stbi_set_flip_vertically_on_load(false);

  ImageData->pData = stbi_load (fileName.c_str(), &x, &y, &channels, 0, &outErr ); // v3.0.243.1 newer version + compatibility with imgui3xp
  if (!outErr.empty())
    Log::logMsg(outErr);

  // convert to xplane struct
  if ( ImageData->pData )
  {

    ImageData->Width = x;
    ImageData->Height = y;
    ImageData->Channels = (short)channels;

    return true;

  }

  return false;
}

std::vector<uint8_t> atchelper::BitmapReader::readFile(const char * path, std::string &errMsg)// -> std::vector<uint8_t>
{
  errMsg.clear();

  std::ifstream file(path, std::ios::binary | std::ios::ate);
  if (!file.is_open())
  {
    errMsg = "Failed to open file " + std::string(path);    
    auto bytes = std::vector<uint8_t>(0);
    return bytes;
  }
    
  auto size = file.tellg();
  file.seekg(0, std::ios::beg);
  auto bytes = std::vector<uint8_t>(size);
  file.read(reinterpret_cast<char*>(&bytes[0]), size);
  file.close();
  return bytes;
}


