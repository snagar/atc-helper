#ifndef SYSTEMACTIONS_H_
#define SYSTEMACTIONS_H_
#pragma once

/***

Main purpose of this CLSAS is to do System actions on ALL supported platforms.

1. Copy/Paste (ascii/binary)
2. 

***/

#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <stdio.h>

#include <assert.h>

//#include "../core/MxOptions.h"
//#include "../core/Utils.h"
#include "Log.hpp"

//using namespace missionx;
using namespace std;

namespace missionx
{

  static const int MX_LOAD_KEY_POS_IN_VEC = 0;
  static const int MX_LOAD_VALUE_POS_IN_VEC = 3;

class system_actions
{
public:
  system_actions();
  virtual ~system_actions();

  // MX Options
  //static atchelper::MxOptions pluginOptions; // holds plugin options saved in preferences file

  /***
  Copy file:
  inSourceFilePath: absolute file location + file name.
  inTargetFilePath: absolute destination file location + file name.
  outError: Holds error message.
  https://gehrcke.de/2011/06/reading-files-in-c-using-ifstream-dealing-correctly-with-badbit-failbit-eofbit-and-perror/
  **/

  //static bool copy_file(std::string inSourceFilePath, std::string inTargetFilePath, std::string &outError); // implemented in v3.0.201
  //
  ///*init_missionx_log_file: Reset the log file so we will write into a blank file. */
  //static void init_missionx_log_file(std::string inFileAndPath);

  ///*write_missionx_log_file: Write message to X-Plane log and to Mission-X log.*/
  //static bool write_missionx_log_file(std::string inFileAndPath, std::string &inMsg, std::string &outError);

  //// read dataref file and filter by write
  //static bool save_datarefs_with_savepoint(std::string inFileAndPath, std::string inTargetFileAndPath, std::string & outError);

  //// read missions checkpoint dataref file and apply in x-plane
  //static bool read_saved_mission_dataref_file(std::string inFileAndPath, std::string & outError);

  //// get full path and options file
  //static std::string getOptionFileAndPath();

  //// load plugin options
  //static void load_plugin_options();

  //static void store_plugin_options();


};

}
#endif // SYSTEMACTIONS_H_

