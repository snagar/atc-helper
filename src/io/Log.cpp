/*
 * Log.cpp
 *
 *  Created on: Feb 19, 2012
 *      Author: snagar
 */

/**************

Updated: 24-nov-2012

Done: Nothing to change. New in NET. Not exists in 2.05


ToDo:


**************/
#include "Log.hpp"
using namespace atchelper;

namespace atchelper
{
  std::string Log::logFilePath;
  writeLogThread  Log::writeThread; // v3.0.217.8
}



atchelper::Log::Log()
{
}

atchelper::Log::~Log()
{
  // TODO Auto-generated destructor stub
}


void atchelper::Log::logDebugBO(std::string message, bool isThread) // log debug in Build Only mode
{
#if defined DEBUG || defined _DEBUG
  printToLog( "[#debug] " + message, isThread);
#endif
}


void atchelper::Log::logXPLMDebugString(std::string message)
{

  XPLMDebugString(message.c_str()); // not thread safe
}



// Simple formating of headers in Log
void atchelper::Log::printHeaderToLog(std::string s, bool isThread, atchelper::format_type format)
{
  printToLog(s, isThread, format, true);
}

// Simple formating for data in Log
void atchelper::Log::printToLog(std::string s, bool isThread, atchelper::format_type format, bool isHeader)
{
  Log::LOGMODE logMode = LOGMODE::LOG_INFO;

  if (isHeader)
  {

  } // end if header
  else
  {
    if (format == atchelper::format_type::error)
    {
    s = "[ERROR] " + s;
    }
    else if (format == atchelper::format_type::none_cr)
    {
    logMode = LOGMODE::LOG_NO_CR;
    }
    else if (format == atchelper::format_type::sub_element_lvl_1)
    {
      s = "\t" + s;
    }
    else if (format == atchelper::format_type::sub_element_lvl_2)
    {
      s = "\t\t" + s;
    }
    else if (format == atchelper::format_type::warning)
    {
      s = "[Warning] " + s;
    }
    else if (format == atchelper::format_type::attention)
    {
      s = "!!!!!! " + s + " !!!!!!";
    }

  }

  Log::logToFile(s, logMode, isThread); // none
}


void atchelper::Log::logToFile(std::string msg, LOGMODE mode, bool isThread)
{  
    std::string errStr;
    std::string out = "";

    switch (mode)
    {
    case LOGMODE::LOG_NO_CR:
    {
      out = msg;
    }
    break;
    case LOGMODE::LOG_INFO:
    {
      out = msg;
    }
    break;
    case LOGMODE::LOG_ERROR:
    {
      out = " - ERROR: " + msg;
    }
    break;
    case LOGMODE::LOG_DEBUG:
    {
      out = " - DEBUG: " + msg;
    }
    break;
    default:
      break;
    }

    const std::string outMsg = atchelper::PLUGIN_NAME + ": " + out; // add plugin name

    if (isThread)
      Log::writeThread.add_message(outMsg);
    else
      Log::logXPLMDebugString(outMsg); // v3.0.221.15rc4 direct write to Log.txt

}

void atchelper::Log::stop_plugin()
{
  Log::writeThread.stop_plugin();
}
