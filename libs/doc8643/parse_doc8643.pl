#!/usr/bin/perl
use 5.010;
use DBI;
use strict;
use warnings;

# use HTML::Parser ();
# use HTML::TreeBuilder 5 -weak; # Ensure weak references in use
# How to install "HTML::TreeBuilder::XPath" - https://metacpan.org/pod/HTML::TreeBuilder::XPath
# perl -MCPAN -e shell
# install HTML::TreeBuilder::XPath
# use HTML::TreeBuilder::XPath;


use XML::LibXML;
use XML::LibXML::Reader;

use Time::HiRes;
# use DBI qw(:sql_types);



# The source of our XML or HTML data is in: https://doc8643.com/aircrafts
# We will have to save as HTML and extract most of the HTML element and just keep the <li> ones.
#
# Then, we will have to fix the <img ...> element since it is not closing correctly.
# You can Notepadd++ and do a find|replace using regular expresion.
# Youtube reference: https://www.youtube.com/watch?v=0F2sSUyrpKM
#
# Search: (<img.+)(>$)
# Replace: ($1)/($2)
#
# We enclose the parts we want to keep and then use it as $1...$N

#///////////////////////////////////////////
sub ltrim { my $s = shift; $s =~ s/^\s+//;       return $s };
sub rtrim { my $s = shift; $s =~ s/\s+$//;       return $s };
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };



# print @ARGV; #debug

# exit 0; # debug


#### Timelapse ######
my $start_time = Time::HiRes::gettimeofday();

my %arg_map; ## holds external ARGS user sent
my $filename = ""; #$ARGV[0]; current script filename
my $output_filename = ""; #


## parse and store all ARGS
foreach my $ext_arg (@ARGV)
{
  my @words = split ('=', $ext_arg);
  if ( scalar  @words > 1 )
  {
    $arg_map{$words[0]} = $words[1];
  }
  else 
  {
    $arg_map{$words[0]} = $words[0];    
  }
print "\n$words[0]=$words[1]"; ### debug
}

# print join(', ', @arg_map);

# exit 0; # debug
### Assignments of flags

sub print_help
{
  my $help = qq (

This script was written to convert a doc8643.html file into a simpler txt or xml file.
The format of the txt file is key=value just like jason

Syntax:
=======
{script name} *file={filename} *outfile=[all|txt|xml] 

Example: 
	* = mandatory 
	{script name} file=doc8643.html outfile=txt		Will parse and create a {FILE}.txt file in the format key=value
    
);

print $help;


}

if ( exists ($arg_map{"help"}) || scalar %arg_map == 0 )
{

 print_help();

 exit 0;
} 

print "\n";

if ( exists ( $arg_map{"file"} ) )
{
  $filename = $arg_map{"file"};
} 
else{
  die "No input file was defined.";
} 

if ( exists ( $arg_map{"outfile"} ) )
{
  $output_filename = $arg_map{"outfile"};
} 
else{
  $output_filename = $filename.".txt";
  print "No output file was defined. Will use default name:$output_filename\n";
} 

#   my $tree = HTML::TreeBuilder->new; # empty tree
#   $tree->parse_file($filename);
#   $tree->dump; # debug to see what it parsed


open(FH, '>', $output_filename) or die $!;
my $reader = XML::LibXML::Reader->new(location => $filename)  # auto decoding on read
       or die "cannot read file:$filename\n"; 

  my $h3;
  my $small;

  while($reader->read) 
  {
      next unless $reader->nodeType == XML_READER_TYPE_ELEMENT; 
      next unless ($reader->name eq 'li') ; # we want the <li> element since its sub elements holds the <h3> and <small> subelements we need.

      my $dom = $reader->copyCurrentNode(1); # $dom = DOM this should return <li > element.
      $h3  = $dom->findnodes('a/h3'); # latitude
      $small = $dom->findnodes('a/small'); # latitude


      my @fields=split (/ - /, $small);
      my $length_arr = @fields;
      my $planes="";
      for (my $i=1;$i<$length_arr;$i++) {
        {
          if ($i == 1) {
            $planes=$fields[$i];
          }else {
            $planes=$planes." - ".$fields[$i];
          }
        }
      }

      # print "$fields[0]$planes\n"; ## debug
      # print "$manufacture, $small \n";
      # print "$h3=$small\n";
      # print FH "$h3=$small\n"; // removed and changed format due to ImGui filtering rules
      # print FH "$h3\t$small\n";

      ### Format: ICAO Plane : Manufacturer : plane names
      my $manufacturer = $fields[0];
      if ( trim($fields[0]) eq "AVIONES COLOMBIA") {
        $manufacturer = "CESSNA";
      }

      printf FH ("%-6s : %-10s :%s\n", $h3, $manufacturer, $planes);

  }

close(FH);

my $stop_time = Time::HiRes::gettimeofday();
printf("\n\nElapsed: %.2f\n===================\n", $stop_time - $start_time);

exit 0;